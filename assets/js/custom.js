$(document).foundation();

var Fn = {
	// Valida el rut con su cadena completa "XXXXXXXX-X"
	validaRut : function (rutCompleto) {
		if (!/^[0-9]+-[0-9kK]{1}$/.test( rutCompleto ))
			return false;
		var tmp 	= rutCompleto.split('-');
		var digv	= tmp[1];
		var rut 	= tmp[0];
		if ( digv == 'K' ) digv = 'k' ;
		return (Fn.dv(rut) == digv );
	},
	dv : function(T){
		var M=0,S=1;
		for(;T;T=Math.floor(T/10))
			S=(S+T%10*(9-M++%6))%11;
		return S?S-1:'k';
	}
};

$('td[title]').qtip();

$('#rut_reg').filter_input({regex:'[0-9-kK]'});
$('#id_temp').filter_input({regex:'[0-9-]'});
$('#telefono').filter_input({regex:'[0-9-+#*]'});
$('#celular').filter_input({regex:'[0-9-+#*]'});
$('#num_depto').filter_input({regex:'[0-9-+#*]'});
$('input[type=number]').filter_input({regex:'[0-9]'});

var url = $('#postular_btn').data('url');

var options = {

    twitter: {
        text: 'Estoy postulando a esta oferta de trabajo'
    },

    facebook : true,
    googlePlus : true
};

$('#fecha_cierre').fdatepicker({
        language: 'es'
});

$('.socialShare').shareButtons(url, options);

$(document).on('mouseenter','.jobpanel',function(e){
	$(this).find('.jobdata').hide();
	$(this).find('a').css({"display":"inline-block"});
});

$(document).on('mouseleave','.jobpanel',function(e){
	$(this).find('.jobdata').show();
	$(this).children('.showButton').hide();
});

$(document).on('click','#registrar,#link_registro',function(e){
	$('#form_registro').each(function(){
	  	this.reset();
	});
	$('#modalRegistro').foundation('reveal', 'open');
});

$(document).on('click','#cancel_btn',function(e){
	$('#form_registro').each(function(){
	  	this.reset();
	});
	$('#modalRegistro').foundation('reveal', 'close');
});

$(document).on('click','#login_btn',function(e){
	e.preventDefault();
	e.stopPropagation();
	if($('#rut_log').val()=="" || $('pass_log').val()==""){
		var n = noty({
			text: 'Debe introducir datos',
			type: 'error',
			layout: 'center',
			theme: 'relax',
			timeout: '800'
		});
	}
	else{
		$('#login_btn').html('<div id="loader"><div id="circleG"><div id="circleG_1" class="circleG"></div><div id="circleG_2" class="circleG"></div><div id="circleG_3" class="circleG"></div></div></div>');
		$.ajax({
			type: 'POST',
			url: $('#form_login').attr('action'),
			data: $('#form_login').serialize(),
			dataType: 'JSON',
			success: function(data) {
				console.log(data);
				if(data.login){
					var n = noty({
						text: 'Se ha iniciado sesión exitosamente',
						type: 'success',
						layout: 'bottomRight',
						theme: 'relax',
						timeout: '800',
						callback: {
							afterClose: function (){
								window.location = data.redirect;
							}
						}
					});
				}
				else{
					var n = noty({
								text: 'Usuario o contraseña incorrectos',
								type: 'error',
								layout: 'bottomRight',
								timeout: '800',
								theme: 'relax'
							});
				}
			},
			error: function(){
				var n = noty({
								text: 'Ocurrió un error',
								type: 'error',
								layout: 'bottomRight',
								timeout: '800',
								theme: 'relax'
							});
			},
			complete: function(){
				$('#login_btn').html('Entrar');
			}
		});
	}
});

$(document).on('click','#reg_btn',function(e){
	e.preventDefault();
	e.stopPropagation();
	var val_msg = '';
	var valido = true;
	if(Fn.validaRut($('#rut_reg').val())==false){
		val_msg+='Ingrese un rut válido';
		valido = false;
	}
	if($('#nombres_reg').val()==''){
		valido = false;
		val_msg+='<br>Ingrese sus nombres';
	}
	if($('#apellidos_reg').val()==''){
		valido = false;
		val_msg+='<br>Ingrese sus apellidos';
	}
	if($('#email_reg').val()==''){
		valido = false;
		val_msg+='<br>Ingrese su correo';
	}
	if($('#pass_reg').val()==''){
		valido = false;
		val_msg+='<br>Ingrese su contraseña';
	}
	if(valido){
		$('#reg_btn').html('<i class="fa fa-spinner fa-pulse"></i>');
		$.ajax({
			type: 'POST',
			url: $('#form_registro').attr('action'),
			data: $('#form_registro').serialize(),
			dataType: 'JSON',
			success: function(data) {
				console.log(data);
				if(data.status){
						$('#modalRegistro').foundation('reveal', 'close');
						var n = noty({
							text: data.message,
							type: 'success',
							layout: 'bottomRight',
							theme: 'relax',
							timeout: '800'
						});
				}
				else{
					var n = noty({
							text: data.message,
							type: 'error',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax'
					});
				}
			},
			error: function(data){
				console.error(data);
				var n = noty({
							text: 'Ocurrió un error en el servidor',
							type: 'error',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax'
					});
			},
			complete: function(){
				$('#reg_btn').html('Registrar');
			}
		});
	}
	else{
		var n = noty({
				text: val_msg,
				type: 'error',
				layout: 'bottomRight',
				timeout: '800',
				theme: 'relax'
		});
	}

});



$(document).on('click','#publicar_btn',function(e){
	e.preventDefault();
	e.stopPropagation();
	var valida = true;
	var msg = "";
	if($('#titulo').val().length>=200){
		msg += "Título demasiado largo <br>";
		valida = false;
	}
	if($('#titulo').val()==''){
		msg += "Ingrese un título <br>";
		valida = false;
	}
	if($('#seleccion_final').val().length>=200){
		msg += "Texto de selección final demasiado largo <br>";
		valida = false;
	}
	$('#fecha_publicacion').val(new moment(new Date()).format('YYYY-MM-DD'));
	if ($('#estado').is(':checked')){
		$('#estado').val(1);
	}
	else{
		$('#estado').val(0);
	}
	if($('#fecha_cierre').val()==''){
		msg += "Ingrese una fecha de cierre <br>";
		valida = false;
	}
	if($('#descripcion').val()==''){
		msg += "Ingrese descripción <br>";
		valida = false;
	}
	if($('#horas').val()==''){
		msg += "Ingrese la cantidad de horas <br>";
		valida = false;
	}
	if($('#horas').val()==''){
		msg += "Ingrese la cantidad de puestos <br>";
		valida = false;
	}
	console.log($('#descripcion').text());
	if(valida){
		var fecha_cierre = $('#fecha_cierre').val();
		$('#fecha_cierre').val(new moment(fecha_cierre,'DD/MM/YYYY').format('YYYY-MM-DD'));
		$('#form_publicar').submit();
	}
	else{
		var n = noty({
			text: msg,
			type: 'error',
			layout: 'bottomRight',
			timeout: '800',
			theme: 'relax'
		});
	}

});

$(".accordion").on("click", ">li", function (event) {
        if($(this).hasClass('active')){
            $("li.active").removeClass('active').find(".content").slideUp("fast");
        }
        else {
            $("li.active").removeClass('active').find(".content").slideUp("fast");
            $(this).addClass('active').find(".content").slideToggle("fast");
        }
});

$(document).on('click','#postular_btn',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#postular_btn').data('fecha',new moment(new Date()).format('YYYY-MM-DD'));
	$('#postular_btn').html('<i class="fa fa-spinner fa-pulse"></i>');
	$.ajax({
		type: 'POST',
		url: $('#panel_oferta').data('url'),
		data: {id_oferta: $('#panel_oferta').data('id-oferta'),fecha: $('#postular_btn').data('fecha')},
		dataType: 'JSON',
		success: function(data){
			if(data.status){
				var n = noty({
						text: data.message,
						type: 'success',
						layout: 'bottomRight',
						theme: 'relax',
						timeout: '800'
					});
			}
			else{
				var n = noty({
						text: data.message,
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
			}
		},
		error: function(){
			var n = noty({
						text: 'Error en el servidor',
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
		},
		complete: function(){
			$('#postular_btn').html('Aceptar');
			$('#modalBases').foundation('reveal', 'close');
		}
	});
});

$(document).on('click','#cancelar_bases_btn',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#modalBases').foundation('reveal', 'close');
});

$('.datatable').dataTable({
          "sDom": '<f><t><ip>',
					'aoColumnDefs': [{
			        'bSortable': false,
			        'aTargets': ['nosort']
			    }],
          "language": {
              "sProcessing":    "Procesando...",
              "sLengthMenu":    "Mostrar _MENU_ registros",
              "sZeroRecords":   "No se encontraron resultados",
              "sEmptyTable":    "Ningún dato disponible en esta tabla",
              "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix":   "",
              "sSearch":        "Buscar:",
              "sUrl":           "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":    "Último",
                  "sNext":    "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
          }
});

$('.counter').counterUp({
	delay: 10,
    time: 900
});

$(document).on('click','.editarpost.editar>i',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#form_editar_postulacion').each(function(){
		this.reset();
	});
	console.log('click');
	$('#modalEditar').foundation('reveal', 'open');
	$.ajax({
		url: $(this).parent().data('url'),
		method: 'GET',
		dataType: 'JSON',
		success: function(data){
			console.log(data);
			$('#id_postulacion').val(data.id);
			if(data.estado=="1"){
				$('#estado').prop('checked', true);
			}
			else{
				$('#estado').prop('checked', false);
			}
			$('#estado').val(data.estado);
			$('#etapa').val(data.etapa);
			$('#id_oferta').val(data.id_oferta);
			$('#etapa_actual').val(data.etapa);
			$('#nombres').val(data.nombres);
			$('#email').val(data.email);
		}
	});
});

$(document).on('click','#btn_buscar',function(e){
	$.ajax({
		url: $('#form_buscar').attr('action'),
		method: 'POST',
		data: $('#form_buscar').serialize(),
		dataType: 'JSON',
		success: function(data){
			if(data.status){
				console.log(data);
				$('#panelResultados').html('');
				$.each(data.ofertas, function( key, value ) {
				  var oferta_url = $('#panelResultados').data('oferta-url')+'/'+value.id;
				  var $oferta = '<h4><i class="fa fa-suitcase"></i> '+value.titulo+'</h4><p><strong>Descripción </strong>'+value.descripcion+'</p><ul class="inline-list"><li><a href="'+oferta_url+'">Ver más detalles</a></li> </ul><hr/>';
				  $('#panelResultados').append($oferta);
				});
				$('#form_buscar').each(function(){
					this.reset();
				});
			}
			else{
				var n = noty({
						text: data.message,
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
			}
		}
	});
});

$(document).on('click','#btn_subir',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#modalSubir').foundation('reveal', 'open');
});

$(document).on('click','#actualizar_info_btn',function(e){
	e.preventDefault();
	e.stopPropagation();
	var fecha_nacimiento = $('#fecha_nacimiento').val();
	$('#fecha_nacimiento').val(new moment(fecha_nacimiento,'DD/MM/YYYY').format('YYYY-MM-DD'));
	$('#actualizacion').val(new moment(new Date()).format('YYYY-MM-DD'));
	$.ajax({
		url: $('#form_info_personal').attr('action'),
		method: 'POST',
		data: $('#form_info_personal').serialize(),
		dataType: 'JSON',
		success: function(data){
			if(data.status){
				var n = noty({
						text: data.message,
						type: 'success',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax',
						callback: {
							afterClose: function (){
								window.location = data.redirect;
							}
						}
				});
			}
			else{
				var n = noty({
						text: data.message,
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
			}
		}
	});
	fecha_nacimiento = $('#fecha_nacimiento').val();
	$('#fecha_nacimiento').val(new moment(fecha_nacimiento,'YYYY-MM-DD').format('DD/MM/YYYY'));
});

$(document).on('click','#add_med',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#btn_act_med').hide();
	$('#btn_add_med').show();
	$('#medTitle').text('Agregar Establecimiento');
	$('#form_agregar_media').each(function(){
		this.reset();
	});
	$('#modalMedia').foundation('reveal', 'open');
});

$(document).on('click','#btn_add_med',function(e){
	e.preventDefault();
	e.stopPropagation();
	var valido = true;
	var error_msg = '';

	if($('#institucion_med').val()==''){
		valido = false;
		error_msg+='Ingrese nombre de establecimiento';
	}

	if(valido){
		$.ajax({
			url: $('#form_agregar_media').attr('action'),
			data: $('#form_agregar_media').serialize(),
			method: 'POST',
			dataType: 'JSON',
			success: function(data){
				if(data.status){
					var n = noty({
							text: data.message,
							type: 'success',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax',
							callback: {
								afterClose: function (){
									window.location = data.redirect;
								}
							}
					});
				}
				else{
					var n = noty({
							text: data.message,
							type: 'error',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax'
					});
				}
			}
		});
		$('#modalMedia').foundation('reveal', 'close');
	}
	else{
		var n = noty({
	      text: error_msg,
	      type: 'error',
	      layout: 'bottomRight',
	      timeout: '800',
	      theme: 'relax'
	  });
	}

});

$(document).on('click','#add_sup',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#btn_act_sup').hide();
	$('#btn_add_sup').show();
	$('#supTitle').text('Agregar Establecimiento');
	$('#form_agregar_superior').each(function(){
		this.reset();
	});
	$('#modalSuperior').foundation('reveal', 'open');
});

$(document).on('click','#btn_add_sup',function(e){
	e.preventDefault();
	e.stopPropagation();
	var valido = true;
	var error_msg = '';

	if($('#institucion_sup').val()==''){
	  valido = false;
	  error_msg+='Ingrese nombre de establecimiento';
	}

	if(valido){
		$.ajax({
			url: $('#form_agregar_superior').attr('action'),
			data: $('#form_agregar_superior').serialize(),
			method: 'POST',
			dataType: 'JSON',
			success: function(data){
				if(data.status){
					var n = noty({
							text: data.message,
							type: 'success',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax',
							callback: {
								afterClose: function (){
									window.location = data.redirect;
								}
							}
					});
				}
				else{
					var n = noty({
							text: data.message,
							type: 'error',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax'
					});
				}
			}
		});
		$('#modalSuperior').foundation('reveal', 'close');
	}
	else{
	  var n = noty({
	      text: error_msg,
	      type: 'error',
	      layout: 'bottomRight',
	      timeout: '800',
	      theme: 'relax'
	  });
	}
});

$(document).on('click','#add_cap',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#btn_act_cap').hide();
	$('#btn_add_cap').show();
	$('#capTitle').text('Agregar Capacitación');
	$('#form_agregar_capacitacion').each(function(){
		this.reset();
	});
	$('#modalCapacitacion').foundation('reveal', 'open');
});

$(document).on('click','#btn_add_cap',function(e){
	e.preventDefault();
	e.stopPropagation();
	var valido = true;
	var error_msg = '';

	if($('#institucion_cap').val()==''){
	  valido = false;
	  error_msg+='Ingrese nombre de Institución';
	}

	if(valido){
		$.ajax({
			url: $('#form_agregar_capacitacion').attr('action'),
			data: $('#form_agregar_capacitacion').serialize(),
			method: 'POST',
			dataType: 'JSON',
			success: function(data){
				if(data.status){
					var n = noty({
							text: data.message,
							type: 'success',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax',
							callback: {
								afterClose: function (){
									window.location = data.redirect;
								}
							}
					});
				}
				else{
					var n = noty({
							text: data.message,
							type: 'error',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax'
					});
				}
			}
		});
		$('#modalCapacitacion').foundation('reveal', 'close');
	}
	else{
	  var n = noty({
	      text: error_msg,
	      type: 'error',
	      layout: 'bottomRight',
	      timeout: '800',
	      theme: 'relax'
	  });
	}
});

$(document).on('click','#add_esp',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#btn_act_esp').hide();
	$('#btn_add_esp').show();
	$('#espTitle').text('Agregar Especialización');
	$('#form_agregar_especializacion').each(function(){
		this.reset();
	});
	$('#modalEspecializacion').foundation('reveal', 'open');
});

$(document).on('click','#btn_add_esp',function(e){
	e.preventDefault();
	e.stopPropagation();
	var valido = true;
	var error_msg = '';

	if($('#institucion_esp').val()==''){
	  valido = false;
	  error_msg+='Ingrese nombre de Institución';
	}

	if(valido){
		$.ajax({
			url: $('#form_agregar_especializacion').attr('action'),
			data: $('#form_agregar_especializacion').serialize(),
			method: 'POST',
			dataType: 'JSON',
			success: function(data){
				if(data.status){
					var n = noty({
							text: data.message,
							type: 'success',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax',
							callback: {
								afterClose: function (){
									window.location = data.redirect;
								}
							}
					});
				}
				else{
					var n = noty({
							text: data.message,
							type: 'error',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax'
					});
				}
			}
		});
		$('#modalEspecializacion').foundation('reveal', 'close');
	}
	else{
	  var n = noty({
	      text: error_msg,
	      type: 'error',
	      layout: 'bottomRight',
	      timeout: '800',
	      theme: 'relax'
	  });
	}
});

$(document).on('click','#add_exp',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#btn_act_exp').hide();
	$('#btn_add_exp').show();
	$('#expTitle').text('Agregar Empresa');
	$('#form_agregar_exp').each(function(){
		this.reset();
	});
	$('#modalLaboral').foundation('reveal', 'open');
});

$(document).on('click','#btn_add_exp',function(e){
	e.preventDefault();
	e.stopPropagation();
	var valido = true;
	var error_msg = '';

	if($('#nombre_empresa').val()==''){
	  valido = false;
	  error_msg+='Ingrese nombre de la Empresa';
	}

	if(valido){
		$.ajax({
			url: $('#form_agregar_exp').attr('action'),
			data: $('#form_agregar_exp').serialize(),
			method: 'POST',
			dataType: 'JSON',
			success: function(data){
				if(data.status){
					var n = noty({
							text: data.message,
							type: 'success',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax',
							callback: {
								afterClose: function (){
									window.location = data.redirect;
								}
							}
					});
				}
				else{
					var n = noty({
							text: data.message,
							type: 'error',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax'
					});
				}
			}
		});
		$('#modalLaboral').foundation('reveal', 'close');
	}
	else{
	  var n = noty({
	      text: error_msg,
	      type: 'error',
	      layout: 'bottomRight',
	      timeout: '800',
	      theme: 'relax'
	  });
	}
});

$(document).on('click','#add_ref',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#btn_act_ref').hide();
	$('#btn_add_ref').show();
	$('#refTitle').text('Agregar Referencia');
	$('#form_agregar_ref').each(function(){
		this.reset();
	});
	$('#modalReferencia').foundation('reveal', 'open');
});

$(document).on('click','#btn_add_ref',function(e){
	e.preventDefault();
	e.stopPropagation();
	var valido = true;
	var error_msg = '';

	if($('#nombre_ref').val()==''){
	  valido = false;
	  error_msg+='Ingrese nombre del Contacto';
	}

	if(valido){
		$.ajax({
			url: $('#form_agregar_ref').attr('action'),
			data: $('#form_agregar_ref').serialize(),
			method: 'POST',
			dataType: 'JSON',
			success: function(data){
				if(data.status){
					var n = noty({
							text: data.message,
							type: 'success',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax',
							callback: {
								afterClose: function (){
									window.location = data.redirect;
								}
							}
					});
				}
				else{
					var n = noty({
							text: data.message,
							type: 'error',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax'
					});
				}
			}
		});
		$('#modalReferencia').foundation('reveal', 'close');
	}
	else{
	  var n = noty({
	      text: error_msg,
	      type: 'error',
	      layout: 'bottomRight',
	      timeout: '800',
	      theme: 'relax'
	  });
	}
});

$(document).on('click','.editarmedia.editar>i',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#btn_act_med').show();
	$('#btn_add_med').hide();
	$('#medTitle').text('Editar Establecimiento');
	$('#form_agregar_media').each(function(){
		this.reset();
	});
	$('#modalMedia').foundation('reveal', 'open');
	$.ajax({
		url: $(this).parent().data('url'),
		method: 'GET',
		dataType: 'JSON',
		success: function(data){
			console.log(data);
			$('#id_med').val(data.id);
			$('#institucion_med').val(data.institucion);
			$('#tipo_med').val(data.tipo_institucion);
			$('#titulo_med').val(data.titulo_profesional);
			$('#ciudad_med').val(data.ciudad);
			$('#pais_med').val(data.pais);
			$('#inicio_med').val(data.inicio);
			$('#termino_med').val(data.termino);
		},
		error: function(){
			alert('Ocurrió un error');
		}
	});
});

$(document).on('click','.editarsup.editar>i',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#btn_act_sup').show();
	$('#btn_add_sup').hide();
	$('#supTitle').text('Editar Establecimiento');
	$('#form_agregar_superior').each(function(){
		this.reset();
	});
	console.log('click');
	$('#modalSuperior').foundation('reveal', 'open');
	$.ajax({
		url: $(this).parent().data('url'),
		method: 'GET',
		dataType: 'JSON',
		success: function(data){
			console.log(data);
			$('#id_sup').val(data.id);
			$('#institucion_sup').val(data.institucion);
			$('#tipo_sup').val(data.tipo_institucion);
			$('#pais_sup').val(data.pais);
			$('#carrera_sup').val(data.carrera);
			$('#inicio_sup').val(data.inicio);
			$('#ciudad_sup').val(data.ciudad);
			$('#termino_sup').val(data.termino);
			$('#situacion_sup').val(data.situacion);
			$('#grado_sup').val(data.grado);
		}
	});
});

$(document).on('click','.editarcap.editar>i',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#btn_act_cap').show();
	$('#btn_add_cap').hide();
	$('#capTitle').text('Editar Capacitación');
	$('#form_agregar_capacitacion').each(function(){
		this.reset();
	});
	console.log('click');
	$('#modalCapacitacion').foundation('reveal', 'open');
	$.ajax({
		url: $(this).parent().data('url'),
		method: 'GET',
		dataType: 'JSON',
		success: function(data){
			console.log(data);
			$('#id_cap').val(data.id);
			$('#institucion_cap').val(data.institucion);
			$('#nombre_cap').val(data.nombre_capacitacion);
			$('#inicio_cap').val(data.inicio);
			$('#termino_cap').val(data.termino);
			$('#ciudad_cap').val(data.ciudad);
			$('#mes_inicio_cap').val(data.mes_inicio);
			$('#mes_termino_cap').val(data.mes_termino);
			$('#horas_cap').val(data.horas);
		}
	});
});

$(document).on('click','.editaresp.editar>i',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#btn_act_esp').show();
	$('#btn_add_esp').hide();
	$('#espTitle').text('Editar Especialización');
	$('#form_agregar_especializacion').each(function(){
		this.reset();
	});
	console.log('click');
	$('#modalEspecializacion').foundation('reveal', 'open');
	$.ajax({
		url: $(this).parent().data('url'),
		method: 'GET',
		dataType: 'JSON',
		success: function(data){
			console.log(data);
			$('#id_esp').val(data.id);
			$('#institucion_esp').val(data.institucion);
			$('#nombre_esp').val(data.nombre_capacitacion);
			$('#inicio_esp').val(data.inicio);
			$('#termino_esp').val(data.termino);
			$('#mes_inicio_esp').val(data.mes_inicio);
			$('#mes_termino_esp').val(data.mes_termino);
			$('#tipo_esp').val(data.tipo_esp);
		}
	});
});

$(document).on('click','#btn_act_med',function(e){
	e.preventDefault();
	e.stopPropagation();
	$.ajax({
		url: $(this).data('url'),
		method: 'POST',
		data: $('#form_agregar_media').serialize(),
		dataType: 'JSON',
		success: function(data){
			if(data.status){
				var n = noty({
						text: data.message,
						type: 'success',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax',
						callback: {
							afterClose: function (){
								$('#modalMedia').foundation('reveal', 'close');
								location.reload();
							}
						}
				});
			}
			else{
				var n = noty({
						text: data.message,
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
			}
		}
	});
});

$(document).on('click','#btn_act_sup',function(e){
	e.preventDefault();
	e.stopPropagation();
	$.ajax({
		url: $(this).data('url'),
		method: 'POST',
		data: $('#form_agregar_superior').serialize(),
		dataType: 'JSON',
		success: function(data){
			if(data.status){
				var n = noty({
						text: data.message,
						type: 'success',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax',
						callback: {
							afterClose: function (){
								$('#modalSuperior').foundation('reveal', 'close');
								location.reload();
							}
						}
				});
			}
			else{
				var n = noty({
						text: data.message,
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
			}
		}
	});
});

$(document).on('click','#btn_act_cap',function(e){
	e.preventDefault();
	e.stopPropagation();
	$.ajax({
		url: $(this).data('url'),
		method: 'POST',
		data: $('#form_agregar_capacitacion').serialize(),
		dataType: 'JSON',
		success: function(data){
			if(data.status){
				var n = noty({
						text: data.message,
						type: 'success',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax',
						callback: {
							afterClose: function (){
								$('#modalCapacitacion').foundation('reveal', 'close');
								location.reload();
							}
						}
				});
			}
			else{
				var n = noty({
						text: data.message,
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
			}
		},
		error: function(data){
			console.log(data);
			var n = noty({
						text: 'Ocurrió un error',
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
		}
	});
});

$(document).on('click','#btn_act_esp',function(e){
	e.preventDefault();
	e.stopPropagation();
	$.ajax({
		url: $(this).data('url'),
		method: 'POST',
		data: $('#form_agregar_especializacion').serialize(),
		dataType: 'JSON',
		success: function(data){
			if(data.status){
				var n = noty({
						text: data.message,
						type: 'success',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax',
						callback: {
							afterClose: function (){
								$('#modalEspecializacion').foundation('reveal', 'close');
								location.reload();
							}
						}
				});
			}
			else{
				var n = noty({
						text: data.message,
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
			}
		},
		error: function(data){
			console.log(data);
			var n = noty({
						text: 'Ocurrió un error',
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
		}
	});
});

$(document).on('click','.editarexp.editar>i',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#btn_act_exp').show();
	$('#btn_add_exp').hide();
	$('#expTitle').text('Editar Empresa');
	$('#form_agregar_exp').each(function(){
		this.reset();
	});
	console.log($(this).parent().data('url'));
	$('#modalLaboral').foundation('reveal', 'open');
	$.ajax({
		url: $(this).parent().data('url'),
		method: 'GET',
		dataType: 'JSON',
		success: function(data){
			console.log(data);
			$('#id_exp').val(data.id);
			$('#nombre_empresa').val(data.empresa);
			$('#pais').val(data.pais);
			$('#cargo').val(data.cargo);
			$('#inicio').val(data.inicio);
			$('#termino').val(data.termino);
			$('#mes_inicio').val(data.mes_inicio);
			$('#mes_termino').val(data.mes_termino);
			$('#funciones').html(data.funciones);
			$('#logros').html(data.logros);
		},
		error: function(data){
			console.log(data);
			var n = noty({
						text: 'Ocurrió un error',
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
		}
	});
});

$(document).on('click','#btn_act_exp',function(e){
	e.preventDefault();
	e.stopPropagation();
	$.ajax({
		url: $(this).data('url'),
		method: 'POST',
		data: $('#form_agregar_exp').serialize(),
		dataType: 'JSON',
		success: function(data){
			if(data.status){
				var n = noty({
						text: data.message,
						type: 'success',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax',
						callback: {
							afterClose: function (){
								$('#modalLaboral').foundation('reveal', 'close');
								location.reload();
							}
						}
				});
			}
			else{
				var n = noty({
						text: data.message,
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
			}
		},
		error: function(data){
			var n = noty({
						text: 'Ocurrió un error',
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
		}
	});
});

$(document).on('click','.editarref.editar>i',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#btn_act_ref').show();
	$('#btn_add_ref').hide();
	$('#refTitle').text('Editar Referencia');
	$('#form_agregar_ref').each(function(){
		this.reset();
	});
	console.log($(this).parent().data('url'));
	$('#modalReferencia').foundation('reveal', 'open');
	$.ajax({
		url: $(this).parent().data('url'),
		method: 'GET',
		dataType: 'JSON',
		success: function(data){
			console.log(data);
			$('#id_ref').val(data.id);
			$('#nombre_ref').val(data.nombre_ref);
			$('#cargo_ref').val(data.cargo_ref);
			$('#relacion_ref').val(data.relacion_ref);
			$('#institucion_ref').val(data.institucion_ref);
			$('#telefono_ref').val(data.telefono_ref);
			$('#email_ref').val(data.email_ref);

		},
		error: function(data){
			console.log(data);
			var n = noty({
						text: 'Ocurrió un error',
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
		}
	});
});

$(document).on('click','#btn_act_ref',function(e){
	e.preventDefault();
	e.stopPropagation();
	$.ajax({
		url: $(this).data('url'),
		method: 'POST',
		data: $('#form_agregar_ref').serialize(),
		dataType: 'JSON',
		success: function(data){
			if(data.status){
				var n = noty({
						text: data.message,
						type: 'success',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax',
						callback: {
							afterClose: function (){
								$('#modalReferencia').foundation('reveal', 'close');
								location.reload();
							}
						}
				});
			}
			else{
				var n = noty({
						text: data.message,
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
			}
		},
		error: function(data){
			var n = noty({
						text: 'Ocurrió un error',
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
		}
	});
});

$(document).on('click','#btn_act_post',function(e){
	e.preventDefault();
	e.stopPropagation();
	if ($('#estado').is(':checked')){
		$('#estado').val(1);
	}
	else{
		$('#estado').val(0);
	}
	$('#btn_act_post').html('<i class="fa fa-spinner fa-pulse"></i>');
	console.log($('#form_editar_postulacion').serialize());
	$.ajax({
		url: $('#form_editar_postulacion').attr('action'),
		method: 'POST',
		data: $('#form_editar_postulacion').serialize(),
		dataType: 'JSON',
		success: function(data){
			if(data.status){
				var n = noty({
						text: data.message,
						type: 'success',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax',
						callback: {
							afterClose: function (){
								$('#modalEditar').foundation('reveal', 'close');
								location.reload();
							}
						}
				});
			}
			else{
				var n = noty({
						text: data.message,
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
			}
		},
		error: function(data){
			var n = noty({
						text: 'Ocurrió un error',
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
		},
		complete: function(){
			$('#btn_act_post').html('Actualizar');
		}
	});
});

$(document).on('click','#actualizar_btn',function(e){
	e.preventDefault();
	e.stopPropagation();
	if ($('#estado').is(':checked')){
		$('#estado').val(1);
	}
	else{
		$('#estado').val(0);
	}
	var fecha_cierre = $('#fecha_cierre').val();
	$('#fecha_cierre').val(new moment(fecha_cierre,'DD/MM/YYYY').format('YYYY-MM-DD'));
	$('#form_editar').submit();
});

$('textarea.msg').jqte({fsize: false,format: false});

$(document).on('click','#pass_btn',function(e){
	console.log($('#form_password').serialize());
	e.preventDefault();
	e.stopPropagation();
	var nueva = $('#pass_nueva').val();
	var confirmacion = $('#pass_conf').val();
	if(nueva===confirmacion){
		$.ajax({
			url: $('#form_password').attr('action'),
			method: 'POST',
			data: $('#form_password').serialize(),
			dataType: 'JSON',
			success: function(data){
				if(data.valido){
					var n = noty({
							text: 'Contraseña cambiada exitosamente',
							type: 'success',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax',
							callback: {
								afterClose: function (){
									window.location = data.redirect;
								}
							}
					});
				}
				else{
					var n = noty({
							text: 'Ocurrió un error al cambiar la contraseña',
							type: 'error',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax'
					});
				}
			},
			error: function(data){
				console.log(data);
				var n = noty({
							text: 'Ocurrió un error en el servidor',
							type: 'error',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax'
					});
			}
		});
	}
	else{
		alert('Las contraseñas no coinciden');
	}
});

$(document).on('click','#actualizar_config_btn',function(e){
	if ($('#recibir_info').is(':checked')){
		$('#recibir_info').val(1);
	}
	else{
		$('#recibir_info').val(0);
	}
	$.ajax({
		method: 'post',
		url: $('#form_config').attr('action'),
		data: $('#form_config').serialize(),
		dataType: 'JSON',
		success: function(data){
			console.log(data);
			if(data.status){
				var n = noty({
						text: data.message,
						type: 'success',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
			}
			else{
				var n = noty({
						text: data.message,
						type: 'error',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
			}
		},
		error: function(){
			console.log('error');
			var n = noty({
							text: 'Ocurrió un error en el servidor',
							type: 'error',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax'
					});
		}
	});
});

$(document).on('click','#btn_postulaciones',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#tabla_resultados').html('');
	if($('#rut').val()!=''){
		$('#btn_postulaciones').html('<i class="fa fa-spinner fa-pulse"></i>');
		$.ajax({
			url: $('#form_dashboard').attr('action')+'/'+$('#rut').val(),
			method: 'POST',
			data: $('#form_dashboard').serialize(),
			dataType: 'JSON',
			success: function(data){
				console.log(data.length);
				if(data.length!=0){
					var content = '';
					$.each(data,function(k,v){
						var estado = (v.estado == 0) ? 'Terminada' : 'Activa';
						var etapas = ['Recepción de Currículum','Entrevista Psicológica','Entrevista Personal','Seleccionado'];
						content+='<tr><td>'+v.rut+'</td><td>'+estado+'</td><td>'+etapas[(v.etapa)-1]+'</td></tr>';
					});
					$('#tabla_resultados').html('<thead><tr><th>RUT</th><th>Estado</th><th>Etapa</th></tr></thead><tbody>'+content+'</tbody>');
				}
				else{
					$('#tabla_resultados').html('<tr><td>No se encontraron resultados asociados al RUT ingresado</td></tr>');
				}

			},
			error: function(){

			},
			complete: function(){
				$('#btn_postulaciones').html('<i class="fa fa-search"></i>');
			}

		});

	}
	else{
		alert('Ingrese un RUT válido');
	}
});

$(document).on('click','#btn_ofertas', function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#tabla_resultados2').html('');
	if($('#termino').val()!=''){
		$('#btn_ofertas').html('<i class="fa fa-spinner fa-pulse"></i>');
		$.ajax({
			url: $('#form_dashboard2').attr('action'),
			method: 'POST',
			data: $('#form_dashboard2').serialize(),
			dataType: 'JSON',
			success: function(data){
				console.log(data);
				$('#tabla_resultados2').html(data);
				if(data.status){
					var content = '';
					$.each(data.ofertas,function(k,v){
						var estado = (v.estado == 0) ? 'Terminada' : 'Activa';
						var etapas = ['Recepción de Currículum','Entrevista Psicológica','Entrevista Personal','Seleccionado'];
						content+='<tr><td><a href="'+v.url+'">'+v.titulo+'</a></td></tr>';
					});
					$('#tabla_resultados2').html('<tbody>'+content+'</tbody>');
				}
				else{
					$('#tabla_resultados2').html('<tr><td>No se encontraron resultados</td></tr>');
				}
			},
			error: function(){

			},
			complete: function(){
				$('#btn_ofertas').html('<i class="fa fa-search"></i>');
			}

		});

	}
	else{
		alert('Ingrese una palabra a buscar');
	}
});

$(document).on('click','.btnavance',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#etapa1').html('');
	$('#etapa2').html('');
	$('#etapa3').html('');
	$('#etapa4').html('');
	$.ajax({
		url: $(this).attr('href'),
		method: 'GET',
		dataType: 'JSON',
		success: function(data){
			console.log(data);
			var etapas = ['Recepción de Currículum','Entrevista Psicológica','Entrevista Personal','Selección'];
            $('#modalAvance').find('h3').find('strong').text(etapas[(data.avance)-1]);
            $.each(data.etapa1,function(k,v){
	            $('#etapa1').append('<li>'+v+'</li>');
            });
            $.each(data.etapa2,function(k,v){
	            $('#etapa2').append('<li>'+v+'</li>');
            });
            $.each(data.etapa3,function(k,v){
	            $('#etapa3').append('<li>'+v+'</li>');
            });
            $.each(data.etapa4,function(k,v){
	            $('#etapa4').append('<li>'+v+'</li>');
            });
		},
		error: function(){

		}
	});
	$('#modalAvance').foundation('reveal', 'open');
});

$(document).on('click','#rec_btn',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#rec_btn').html('<i class="fa fa-spinner fa-pulse"></i>');
	$.ajax({
		url: $('#form_recuperar').attr('action'),
		method: 'POST',
		dataType: 'JSON',
		data: $('#form_recuperar').serialize(),
		success: function(data){
			var n = noty({
						text: 'Ticket enviado',
						type: 'success',
						layout: 'bottomRight',
						timeout: '800',
						theme: 'relax'
				});
		},
		error: function(){

		},
		complete: function(){
			$('#rec_btn').html('Enviar');
			$('#modalRecuperar').foundation('reveal', 'close');
			$('#form_recuperar').each(function(){
			  	this.reset();
			});
		}
	});
});

$(document).on('click','#rec_cancel_btn',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#modalRecuperar').foundation('reveal', 'close');
	$('#form_recuperar').each(function(){
	  	this.reset();
	});
});

$(document).on('click','#btn_activar',function(e){
	e.preventDefault();
	e.stopPropagation();
	$.ajax({
		method: 'POST',
		url: $(this).attr('href'),
		data: $('#form_reemplazo').serialize(),
		dataType: 'JSON',
		success: function(data){
			if(data.status){
				var n = noty({
								text: data.message,
								type: 'success',
								layout: 'bottomRight',
								timeout: '800',
								theme: 'relax'
							});
			}
			else{
				var n = noty({
								text: data.message,
								type: 'error',
								layout: 'bottomRight',
								timeout: '800',
								theme: 'relax'
							});
			}
		},
		error: function(){
			var n = noty({
							text: 'Ocurrió un error',
							type: 'error',
							layout: 'bottomRight',
							timeout: '800',
							theme: 'relax'
						});
		}
	});
});

$(document).on('click','input.css-checkbox',function(){
	console.log('click');
	if($('input.css-checkbox').is(':checked')){
		$('#modalReemplazo').foundation('reveal', 'open');
	}
});

$(document).on('click','.user_link',function(e){
	e.preventDefault();
	e.stopPropagation();
	$('#dTitle').html($(this).html());
	$('#tabla_datos tbody').html('');
	$('#cv_link').attr('href',$(this).data('cv'));
	$('#modalDatos').foundation('reveal','open');
	$.ajax({
		url: $(this).data('url'),
		dataType: 'JSON',
		success: function(data){
			var content = '';
			$.each(data,function(key,value){
				console.log(value);
				content += '<tr><td>'+value.tipo_documento+'</td><td><a target="_blank" href="'+value.ruta_documento+'"><i class="fa fa-eye"></i></a></td></tr>';
			});
			$('#tabla_datos tbody').html(content);
		}
	});
});

$(document).on('click','.additem',function(e){
	var fecha_nacimiento = $('#fecha_nacimiento').val();
	$('#fecha_nacimiento').val(new moment(fecha_nacimiento,'DD/MM/YYYY').format('YYYY-MM-DD'));
	$('#actualizacion').val(new moment(new Date()).format('YYYY-MM-DD'));
	$.ajax({
		url: $('#form_info_personal').attr('action'),
		method: 'POST',
		data: $('#form_info_personal').serialize(),
		dataType: 'JSON',
		success: function(data){
			console.log('OK');
		}
	});
	fecha_nacimiento = $('#fecha_nacimiento').val();
	$('#fecha_nacimiento').val(new moment(fecha_nacimiento,'YYYY-MM-DD').format('DD/MM/YYYY'));
});
