<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Postulacion extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->model(array('mod_postulacion','mod_mensajes','mod_usuario','mod_instructivo','mod_oferta'));
        $this->load->helper('correo');
        $this->load->library('email');
  }

  public function nueva(){
    $user = $this->session->userdata('user_data');
    if (!isset($user)) {
      echo json_encode(array('status'=>false,'message'=>'Debe iniciar sesión para postular a una oferta'));
    }
    else {
      if($this->session->userdata('rol')=='user'){
        $rut_postulante = $this->session->userdata('rut');
        $id_oferta = $this->input->post('id_oferta');
        $fecha = $this->input->post('fecha');
        $data['rut_postulante'] = $rut_postulante;
        $data['id_oferta'] = $id_oferta;
        $data['fecha'] = $fecha;
        $data['estado'] = 1;
        $data['etapa'] = 1;
        $query = $this->mod_postulacion->verificar_postulacion($rut_postulante,$id_oferta);
        if ($query->num_rows() > 0){
            echo json_encode(array('status'=>false,'message'=>'Usted ya postuló a esta oferta'));
        }
        else{
            $query_post = $this->mod_oferta->obtener($id_oferta);
            $fecha_cierre = '';
            foreach($query_post->result() as $row){
              $fecha_cierre = date("d/m/Y", strtotime($row->fecha_cierre));;
            }
            $hoy = date('d/m/Y');
            $d1 = DateTime::createFromFormat('d/m/Y',$hoy);
            $d2 = DateTime::createFromFormat('d/m/Y',$fecha_cierre);
            if($d1<=$d2){
              if($this->mod_postulacion->nuevo($data)){
                  echo json_encode(array('status'=>true,'message'=>'Postulación realizada exitosamente'));
                  $config = obtener_config();
                  $this->email->initialize($config);
                  $data = array('nombres'=>$this->session->userdata('nombre'));
                  $query = $this->mod_mensajes->obtener();
                  $data['mensaje'] = '';
                  if ($query->num_rows() > 0){
                    foreach($query->result() as $row){
                      $data['mensaje'] = $row->msg_inicio;
                    }
                  }
                  $html_email = $this->load->view('mail', $data, true);
                  $this->email->from(obtener_correo());
                  $this->email->to($this->session->userdata('email'));
                  $this->email->subject('Inicio de postulación');
                  $this->email->message($html_email);
                  $status = $this->email->send();
              }
              else{
                  echo json_encode(array('status'=>false,'message'=>'Ocurrió un error al realizar la postulación'));
              }
            }
            else{
              echo json_encode(array('status'=>false,'message'=>'Postulación fuera de plazo'));
            }
        }
      }
      else{
        echo json_encode(array('status'=>false,'message'=>'Sólo los usuarios registrados pueden postular a ofertas'));
      }
    }
  }

  public function obtener(){
    if($this->session->userdata('rol')=='admin'){
      $id = $this->uri->segment(3);
      $query = $this->mod_postulacion->obtener($id);
      $data = array();
      foreach($query->result() as $row){
        $data['id'] = $row->id;
        $data['id_oferta'] = $row->id_oferta;
        $data['estado'] = $row->estado;
        $data['etapa'] = $row->etapa;
        $data['nombres'] = $row->nombres;
        $data['email'] = $row->email;
      }
      echo json_encode($data);
    }
  }

  public function buscar(){
    $rut = $this->uri->segment(3);
    $query = $this->mod_postulacion->historial($rut);
    $postulaciones = array();
    $data = array();
    foreach($query->result() as $row){
      $data['id_oferta'] = $row->id_oferta;
      $data['rut'] = $row->rut_postulante;
      $data['estado'] = (int)$row->estado;
      $data['etapa'] = (int)$row->etapa;
      $postulaciones[] = $data;
    }
    echo json_encode($postulaciones);
  }

  public function editar(){
    if($this->session->userdata('rol')=='admin'){
        $data['id'] = $this->input->post('id_postulacion');
        $data['estado'] = $this->input->post('estado');
        $data['etapa'] = $this->input->post('etapa');
        $data['actualizacion'] = date('Y/m/d H:i:s');
        $etapa_nueva = (int)$this->input->post('etapa');
        $etapa_actual = (int)$this->input->post('etapa_actual');
        $estado = (int)$this->input->post('estado');
        $id_oferta = $this->input->post('id_oferta');
        $insert = $this->mod_postulacion->actualizar($data);
        if($insert){
          if($estado==0){ // Mensaje de término de postulación
            $config = obtener_config();
            $this->email->initialize($config);
            $data = array('nombres'=>$this->input->post('nombres'));
            $query = $this->mod_mensajes->obtener();
            $data['mensaje'] = '';
            if ($query->num_rows() > 0){
              foreach($query->result() as $row){
                $data['mensaje'] = $row->msg_termino;
              }
            }
            $html_email = $this->load->view('mail', $data, true);
            $this->email->from(obtener_correo());
            $this->email->to($this->input->post('email'));
            $this->email->subject('Postulación');
            $this->email->message($html_email);
            $status = $this->email->send();
          }
          else{
            if($etapa_nueva>$etapa_actual){
              if($etapa_nueva==4){ // Mensaje de selección
                $config = obtener_config();
                $this->email->initialize($config);
                $data = array('nombres'=>$this->input->post('nombres'));
                $query = $this->mod_mensajes->obtener();
                $mensaje = '';
                if ($query->num_rows() > 0){
                  foreach($query->result() as $row){
                    $mensaje = $row->msg_seleccion;
                  }
                }
                $query_instructivo = $this->mod_instructivo->obtener($id_oferta);
                if ($query_instructivo->num_rows() > 0){
                  $mensaje .= '<br> Puedes descargar el instructivo de ingreso acá: <br>';
                  foreach($query_instructivo->result() as $row){
                    $mensaje .= base_url().'instructivos/'.$row->ruta_documento.'<br>';
                  }
                }
                $data['mensaje'] = $mensaje;
                $html_email = $this->load->view('mail', $data, true);
                $this->email->from(obtener_correo());
                $this->email->to($this->input->post('email'));
                $this->email->subject('Postulación');
                $this->email->message($html_email);
                $status = $this->email->send();
              }
              else{ // Mensaje de avance
                $config = obtener_config();
                $this->email->initialize($config);
                $data = array('nombres'=>$this->input->post('nombres'));
                $query = $this->mod_mensajes->obtener();
                $data['mensaje'] = '';
                if ($query->num_rows() > 0){
                  foreach($query->result() as $row){
                    $data['mensaje'] = $row->msg_avance;
                  }
                }
                $html_email = $this->load->view('mail', $data, true);
                $this->email->from(obtener_correo());
                $this->email->to($this->input->post('email'));
                $this->email->subject('Postulación');
                $this->email->message($html_email);
                $status = $this->email->send();
              }
            }
          }

          echo json_encode(array('status'=>$insert,'message'=>'Postulación actualizada correctamente','redirect'=>site_url('admin/postulaciones')));
        }
        else{
          echo json_encode(array('status'=>$insert,'message'=>'Ocurrió un error al actualizar la postulación'));
        }
    }
  }

  public function eliminar(){
    if($this->session->userdata('rol')=='admin'){
      $id = $this->uri->segment(3);
      $this->mod_postulacion->eliminar($id);
      redirect('/admin/postulaciones','refresh');
    }
  }
}
