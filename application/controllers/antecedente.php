<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Antecedente extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->model('mod_antecedente');
  }

  public function agregar_media(){
    if($this->session->userdata('rol')=='user'){
      $data['rut_postulante'] = $this->session->userdata('rut');
      $data['tipo_antecedente'] = 'media';
      $data['institucion'] = $this->input->post('institucion_med');
      $data['tipo_institucion'] = $this->input->post('tipo_med');
      $data['titulo_profesional'] = $this->input->post('titulo_med');
      $data['pais'] = $this->input->post('pais_med');
      $data['ciudad'] = $this->input->post('ciudad_med');
      $data['inicio'] = $this->input->post('inicio_med');
      $data['termino'] = $this->input->post('termino_med');
      $insert = $this->mod_antecedente->nuevo($data);
      if($insert){
        echo json_encode(array('status'=>$insert,'message'=>'Antecedente agregado correctamente','redirect'=>site_url('dashboard/curriculum')));
      }
      else{
        echo json_encode(array('status'=>$insert,'message'=>'Ocurrió un error al insertar el antecedente'));
      }
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function agregar_superior(){
    if($this->session->userdata('rol')=='user'){
      $data['rut_postulante'] = $this->session->userdata('rut');
      $data['tipo_antecedente'] = 'superior';
      $data['institucion'] = $this->input->post('institucion_sup');
      $data['tipo_institucion'] = $this->input->post('tipo_sup');
      $data['pais'] = $this->input->post('pais_sup');
      $data['carrera'] = $this->input->post('carrera_sup');
      $data['inicio'] = $this->input->post('inicio_sup');
      $data['termino'] = $this->input->post('termino_sup');
      $data['situacion'] = $this->input->post('situacion_sup');
      $data['ciudad'] = $this->input->post('ciudad_sup');
      $data['grado'] = $this->input->post('grado_sup');
      $insert = $this->mod_antecedente->nuevo($data);
      if($insert){
        echo json_encode(array('status'=>$insert,'message'=>'Antecedente agregado correctamente','redirect'=>site_url('dashboard/curriculum')));
      }
      else{
        echo json_encode(array('status'=>$insert,'message'=>'Ocurrió un error al insertar el antecedente'));
      }
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function agregar_especializacion(){
    if($this->session->userdata('rol')=='user'){
      $data['rut_postulante'] = $this->session->userdata('rut');
      $data['tipo_antecedente'] = 'especializacion';
      $data['institucion'] = $this->input->post('institucion_esp');
      $data['nombre_capacitacion'] = $this->input->post('nombre_esp');
      $data['mes_inicio'] = $this->input->post('mes_inicio_esp');
      $data['inicio'] = $this->input->post('inicio_esp');
      $data['mes_termino'] = $this->input->post('mes_termino_esp');
      $data['termino'] = $this->input->post('termino_esp');
      $data['tipo_esp'] = $this->input->post('tipo_esp');
      $insert = $this->mod_antecedente->nuevo($data);
      if($insert){
        echo json_encode(array('status'=>$insert,'message'=>'Antecedente agregado correctamente','redirect'=>site_url('dashboard/curriculum')));
      }
      else{
        echo json_encode(array('status'=>$insert,'message'=>'Ocurrió un error al insertar el antecedente'));
      }
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function agregar_capacitacion(){
    if($this->session->userdata('rol')=='user'){
      $data['rut_postulante'] = $this->session->userdata('rut');
      $data['tipo_antecedente'] = 'capacitacion';
      $data['institucion'] = $this->input->post('institucion_cap');
      $data['nombre_capacitacion'] = $this->input->post('nombre_cap');
      $data['mes_inicio'] = $this->input->post('mes_inicio_cap');
      $data['inicio'] = $this->input->post('inicio_cap');
      $data['mes_termino'] = $this->input->post('mes_termino_cap');
      $data['termino'] = $this->input->post('termino_cap');
      $data['horas'] = $this->input->post('horas_cap');
      $data['ciudad'] = $this->input->post('ciudad_cap');
      $insert = $this->mod_antecedente->nuevo($data);
      if($insert){
        echo json_encode(array('status'=>$insert,'message'=>'Antecedente agregado correctamente','redirect'=>site_url('dashboard/curriculum')));
      }
      else{
        echo json_encode(array('status'=>$insert,'message'=>'Ocurrió un error al insertar el antecedente'));
      }
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function obtener(){
    if($this->session->userdata('rol')=='user'){
      $id = $this->uri->segment(3);
      $query = $this->mod_antecedente->obtener($id);
      $data = array();
      foreach($query->result() as $row){
        $data['id'] = $row->id;
        $data['institucion'] = $row->institucion;
        $data['tipo_institucion'] = $row->tipo_institucion;
        $data['titulo_profesional'] = $row->titulo_profesional;
        $data['ciudad'] = $row->ciudad;
        $data['pais'] = $row->pais;
        $data['inicio'] = $row->inicio;
        $data['termino'] = $row->termino;
        $data['carrera'] = $row->carrera;
        $data['situacion'] = $row->situacion;
        $data['grado'] = $row->grado;
        $data['nombre_capacitacion'] = $row->nombre_capacitacion;
        $data['mes_inicio'] = $row->mes_inicio;
        $data['mes_termino'] = $row->mes_termino;
        $data['horas'] = $row->horas;
        $data['rut_postulante'] = $row->rut_postulante;
        $data['tipo_antecedente'] = $row->tipo_antecedente;
        $data['tipo_esp'] = $row->tipo_esp;
      }
      echo json_encode($data);
    }
  }

  public function editar_media(){
    if($this->session->userdata('rol')=='user'){
      $data['id'] = $this->input->post('id_med');
      $data['institucion'] = $this->input->post('institucion_med');
      $data['tipo_institucion'] = $this->input->post('tipo_med');
      $data['pais'] = $this->input->post('pais_med');
      $data['inicio'] = $this->input->post('inicio_med');
      $data['titulo_profesional'] = $this->input->post('titulo_med');
      $data['ciudad'] = $this->input->post('ciudad_med');
      $data['termino'] = $this->input->post('termino_med');
      $insert = $this->mod_antecedente->actualizar($data);
      if($insert){
        echo json_encode(array('status'=>$insert,'message'=>'Antecedente actualizado correctamente','redirect'=>site_url('dashboard/curriculum')));
      }
      else{
        echo json_encode(array('status'=>$insert,'message'=>'Ocurrió un error al actualizar el antecedente'));
      }
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function editar_superior(){
    if($this->session->userdata('rol')=='user'){
      $data['id'] = $this->input->post('id_sup');
      $data['institucion'] = $this->input->post('institucion_sup');
      $data['tipo_institucion'] = $this->input->post('tipo_sup');
      $data['pais'] = $this->input->post('pais_sup');
      $data['carrera'] = $this->input->post('carrera_sup');
      $data['inicio'] = $this->input->post('inicio_sup');
      $data['termino'] = $this->input->post('termino_sup');
      $data['ciudad'] = $this->input->post('ciudad_sup');
      $data['situacion'] = $this->input->post('situacion_sup');
      $data['grado'] = $this->input->post('grado_sup');

      $insert = $this->mod_antecedente->actualizar($data);
      if($insert){
        echo json_encode(array('status'=>$insert,'message'=>'Antecedente actualizado correctamente','redirect'=>site_url('dashboard/curriculum')));
      }
      else{
        echo json_encode(array('status'=>$insert,'message'=>'Ocurrió un error al actualizar el antecedente'));
      }
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function editar_especializacion(){
    if($this->session->userdata('rol')=='user'){
      $data['id'] = $this->input->post('id_esp');      
      $data['institucion'] = $this->input->post('institucion_esp');
      $data['nombre_capacitacion'] = $this->input->post('nombre_esp');
      $data['mes_inicio'] = $this->input->post('mes_inicio_esp');
      $data['inicio'] = $this->input->post('inicio_esp');
      $data['mes_termino'] = $this->input->post('mes_termino_esp');
      $data['termino'] = $this->input->post('termino_esp');
      $data['tipo_esp'] = $this->input->post('tipo_esp');
      $insert = $this->mod_antecedente->actualizar($data);
      if($insert){
        echo json_encode(array('status'=>$insert,'message'=>'Antecedente actualizado correctamente','redirect'=>site_url('dashboard/curriculum')));
      }
      else{
        echo json_encode(array('status'=>$insert,'message'=>'Ocurrió un error al actualizar el antecedente'));
      }
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function editar_capacitacion(){
    if($this->session->userdata('rol')=='user'){
      $data['id'] = $this->input->post('id_cap');      
      $data['institucion'] = $this->input->post('institucion_cap');
      $data['nombre_capacitacion'] = $this->input->post('nombre_cap');
      $data['mes_inicio'] = $this->input->post('mes_inicio_cap');
      $data['inicio'] = $this->input->post('inicio_cap');
      $data['mes_termino'] = $this->input->post('mes_termino_cap');
      $data['termino'] = $this->input->post('termino_cap');
      $data['horas'] = $this->input->post('horas_cap');
      $data['ciudad'] = $this->input->post('ciudad_cap');
      $insert = $this->mod_antecedente->actualizar($data);
      if($insert){
        echo json_encode(array('status'=>$insert,'message'=>'Antecedente actualizado correctamente','redirect'=>site_url('dashboard/curriculum')));
      }
      else{
        echo json_encode(array('status'=>$insert,'message'=>'Ocurrió un error al actualizar el antecedente'));
      }
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function eliminar(){
    $id = $this->uri->segment(3);
    $this->mod_antecedente->eliminar($id);
    redirect('dashboard/curriculum','refresh');
  }

}
