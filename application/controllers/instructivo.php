<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Instructivo extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->model('mod_instructivo');
        $this->load->library('upload');
  }

  public function subir(){
    $config['upload_path'] = './instructivos';
    $config['allowed_types'] = 'pdf|jpg|jpeg|png|doc|docx|xls|xlsx';
    $config['max_size'] = '20000';
    $config['encrypt_name'] = true;
    $config['remove_spaces'] = true;
    $this->upload->initialize($config);
    if (!$this->upload->do_upload('file')){
      $data = array('error' => $this->upload->display_errors());
      redirect('admin/instructivos', 'refresh');
    }
    else{
      $info = array('upload_data' => $this->upload->data());
      $data['id_oferta'] = $this->input->post('id_oferta');
      $data['ruta_documento'] = $info['upload_data']['file_name'];
      $this->mod_instructivo->nuevo($data);
      redirect('admin/instructivos', 'refresh');
    }
  }

  public function eliminar(){
    if($this->session->userdata('rol')=='user'){
      $id = $this->uri->segment(3);
      $this->mod_instructivo->eliminar($id);
      redirect('/admin/usuarios','refresh');
    }
  }
}
