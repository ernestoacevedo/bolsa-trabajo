<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recuperar extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->model('mod_usuario');
        $this->load->model('mod_ticket');
        $this->load->library('email');
        $this->load->helper('correo');
  }

  public function generar_ticket(){
    $data = array();
    $codigo = sha1(rand());
    $data['rut_postulante'] = $this->input->post('rut_rec');
    $data['codigo'] = $codigo;
    $data['estado'] = 0;
    $status = true;
    $query = $this->mod_usuario->obtener($this->input->post('rut_rec'));
    if ($query->num_rows()>0){
      $nombres = '';
      $email = '';
      foreach($query->result() as $row){
        $nombres = $row->nombres;
        $email = $row->email;
      }
      $status = $this->mod_ticket->nuevo($data);
      // envio de correo
      $config = obtener_config();
      $this->email->initialize($config);
      $data['nombres'] = $nombres;
      $data['mensaje'] = 'Ingrese a este link para restablecer su contraseña <a href='.site_url('recuperar/verificar/'.$codigo).'>'.site_url('recuperar/verificar/'.$codigo).'</a>';
      $html_email = $this->load->view('mail', $data, true);
      $this->email->from(obtener_correo());
      $this->email->to($email);
      $this->email->subject('Recuperación de contraseña');
      $this->email->message($html_email);
      $this->email->send();
    }
    else{
      $status = false;
    }
    $respuesta = array('status'=>$status);
    echo json_encode($respuesta);
  }

  public function verificar(){
    $codigo = '';
    if ($this->uri->segment(3) === FALSE){
        redirect('/home','refresh');
    }
    else{
        $codigo = $this->uri->segment(3);
        $query = $this->mod_ticket->buscar_ticket($codigo);
        if ($query->num_rows()>0){
          $data['codigo'] = $codigo;
          $this->load->view('header');
          $this->load->view('restablecer_pass',$data);
          $this->load->view('footer');
        }
        else{
          redirect('/home','refresh');
        }


    }
  }

  public function restablecer(){
    $codigo = $this->input->post('codigo');
    $query = $this->mod_ticket->buscar_ticket($codigo);
    if ($query->num_rows()>0){
      foreach($query->result() as $row){
        $rut_postulante = $row->rut_postulante;
        $status = $this->mod_usuario->cambiar_password($rut_postulante,$this->input->post('pass_nueva'));
        echo json_encode(array('valido'=>true,'status'=>$status,'redirect'=>site_url('home')));
      }
    }
    else{
      echo json_encode(array('valido'=>false));
    }
  }

  public function ver_email(){
    $this->load->view(mail);
  }

}
