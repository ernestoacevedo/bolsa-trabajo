<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class CV extends CI_Controller
{

    public function __construct() {
        parent::__construct();
        $this->load->model(array('mod_usuario','mod_antecedente','mod_laboral','mod_postulacion'));
        $this->load->library('pdf');
        $this->load->library('zip');
    }

    public function micv() {
        $user = $this->session->userdata('user_data');
        if (!isset($user)) {
            redirect('/home', 'refresh');
        }
        else {
            if ($this->session->userdata('rol') == 'user') {
                $query = $this->mod_usuario->obtener($this->session->userdata('rut'));
                $usuario = array();
                foreach ($query->result() as $row) {
                    $usuario['rut'] = $row->rut;
                    $usuario['nombres'] = $row->nombres;
                    $usuario['apellidos'] = $row->apellidos;
                    $usuario['direccion'] = $row->direccion;
                    $usuario['telefono'] = $row->telefono;
                    $usuario['email'] = $row->email;
                }
                $query = $this->mod_antecedente->obtener_tipo($this->session->userdata('rut'), 'media');
                $info_media = array();
                $institucion = array();
                foreach ($query->result() as $row) {
                    $institucion['id'] = $row->id;
                    $institucion['nombre'] = $row->institucion;
                    $institucion['tipo'] = $row->tipo_institucion;
                    $institucion['pais'] = $row->pais;
                    $institucion['inicio'] = $row->inicio;
                    $institucion['termino'] = $row->termino;
                    $info_media[] = $institucion;
                }
                $query = $this->mod_antecedente->obtener_tipo($this->session->userdata('rut'), 'superior');
                $info_superior = array();
                $institucion = array();
                foreach ($query->result() as $row) {
                    $institucion['id'] = $row->id;
                    $institucion['nombre'] = $row->institucion;
                    $institucion['tipo'] = $row->tipo_institucion;
                    $institucion['pais'] = $row->pais;
                    $institucion['carrera'] = $row->carrera;
                    $institucion['inicio'] = $row->inicio;
                    $institucion['termino'] = $row->termino;
                    $institucion['situacion'] = $row->situacion;
                    $institucion['grado'] = $row->grado;
                    $info_superior[] = $institucion;
                }
                $query = $this->mod_antecedente->obtener_tipo($this->session->userdata('rut'), 'capacitacion');
                $info_capacitaciones = array();
                $capacitacion = array();
                foreach ($query->result() as $row) {
                    $capacitacion['id'] = $row->id;
                    $capacitacion['institucion'] = $row->institucion;
                    $capacitacion['nombre'] = $row->nombre_capacitacion;
                    $capacitacion['mes_inicio'] = $row->mes_inicio;
                    $capacitacion['mes_termino'] = $row->mes_termino;
                    $capacitacion['inicio'] = $row->inicio;
                    $capacitacion['termino'] = $row->termino;
                    $capacitacion['horas'] = $row->horas;
                    $info_capacitaciones[] = $capacitacion;
                }
                $query = $this->mod_laboral->obtener_tipo($this->session->userdata('rut'), 'laboral');
                $info_antecedentes = array();
                $antecedente = array();
                foreach ($query->result() as $row) {
                    $antecedente['id'] = $row->id;
                    $antecedente['nombre_empresa'] = $row->nombre_empresa;
                    $antecedente['pais'] = $row->pais;
                    $antecedente['mes_inicio'] = $row->mes_inicio;
                    $antecedente['mes_termino'] = $row->mes_termino;
                    $antecedente['inicio'] = $row->inicio;
                    $antecedente['termino'] = $row->termino;
                    $antecedente['cargo'] = $row->cargo;
                    $antecedente['funciones'] = $row->funciones;
                    $antecedente['logros'] = $row->logros;
                    $info_antecedentes[] = $antecedente;
                }
                $query = $this->mod_laboral->obtener_tipo($this->session->userdata('rut'), 'referencia');
                $info_referencias = array();
                $referencia = array();
                foreach ($query->result() as $row) {
                    $referencia['id'] = $row->id;
                    $referencia['nombre'] = $row->nombre_ref;
                    $referencia['cargo'] = $row->cargo_ref;
                    $referencia['telefono'] = $row->telefono_ref;
                    $referencia['email'] = $row->email_ref;
                    $info_referencias[] = $referencia;
                }
                $data = array('usuario' => $usuario, 'info_media' => $info_media, 'info_superior' => $info_superior, 'info_capacitaciones' => $info_capacitaciones, 'info_antecedentes' => $info_antecedentes, 'info_referencias' => $info_referencias);
                $this->load->view('curriculum', $data);
            }
            else {
                redirect('/home', 'refresh');
            }
        }
    }

    public function ver() {
        $user = $this->session->userdata('user_data');
        if (!isset($user)) {
            redirect('/home', 'refresh');
        }
        else {
            $rut = '';
            if ($this->uri->segment(3) === FALSE) {
                redirect('/home', 'refresh');
            }
            else {
                $rut = $this->uri->segment(3);
                 if ($this->session->userdata('rol') == 'admin') {
                    $query = $this->mod_usuario->obtener($rut);
                    $usuario = array();
                    if ($query->num_rows() > 0) {
                        foreach ($query->result() as $row) {
                            $usuario['nombres'] = $row->nombres;
                            $usuario['apellidos'] = $row->apellidos;
                            $usuario['rut'] = $row->rut;
                            $usuario['direccion'] = $row->direccion;
                            $usuario['telefono'] = $row->telefono;
                            $usuario['email'] = $row->email;
                        }
                        $query = $this->mod_antecedente->obtener_tipo($rut, 'media');
                        $info_media = array();
                        $institucion = array();
                        foreach ($query->result() as $row) {
                            $institucion['id'] = $row->id;
                            $institucion['nombre'] = $row->institucion;
                            $institucion['tipo'] = $row->tipo_institucion;
                            $institucion['pais'] = $row->pais;
                            $institucion['inicio'] = $row->inicio;
                            $institucion['termino'] = $row->termino;
                            $info_media[] = $institucion;
                        }
                        $query = $this->mod_antecedente->obtener_tipo($rut, 'superior');
                        $info_superior = array();
                        $institucion = array();
                        foreach ($query->result() as $row) {
                            $institucion['id'] = $row->id;
                            $institucion['nombre'] = $row->institucion;
                            $institucion['tipo'] = $row->tipo_institucion;
                            $institucion['pais'] = $row->pais;
                            $institucion['carrera'] = $row->carrera;
                            $institucion['inicio'] = $row->inicio;
                            $institucion['termino'] = $row->termino;
                            $institucion['situacion'] = $row->situacion;
                            $institucion['grado'] = $row->grado;
                            $info_superior[] = $institucion;
                        }
                        $query = $this->mod_antecedente->obtener_tipo($rut, 'capacitacion');
                        $info_capacitaciones = array();
                        $capacitacion = array();
                        foreach ($query->result() as $row) {
                            $capacitacion['id'] = $row->id;
                            $capacitacion['institucion'] = $row->institucion;
                            $capacitacion['nombre'] = $row->nombre_capacitacion;
                            $capacitacion['mes_inicio'] = $row->mes_inicio;
                            $capacitacion['mes_termino'] = $row->mes_termino;
                            $capacitacion['inicio'] = $row->inicio;
                            $capacitacion['termino'] = $row->termino;
                            $capacitacion['horas'] = $row->horas;
                            $info_capacitaciones[] = $capacitacion;
                        }
                        $query = $this->mod_laboral->obtener_tipo($rut, 'laboral');
                        $info_antecedentes = array();
                        $antecedente = array();
                        foreach ($query->result() as $row) {
                            $antecedente['id'] = $row->id;
                            $antecedente['nombre_empresa'] = $row->nombre_empresa;
                            $antecedente['pais'] = $row->pais;
                            $antecedente['mes_inicio'] = $row->mes_inicio;
                            $antecedente['mes_termino'] = $row->mes_termino;
                            $antecedente['inicio'] = $row->inicio;
                            $antecedente['termino'] = $row->termino;
                            $antecedente['cargo'] = $row->cargo;
                            $antecedente['funciones'] = $row->funciones;
                            $antecedente['logros'] = $row->logros;
                            $info_antecedentes[] = $antecedente;
                        }
                        $query = $this->mod_laboral->obtener_tipo($rut, 'referencia');
                        $info_referencias = array();
                        $referencia = array();
                        foreach ($query->result() as $row) {
                            $referencia['id'] = $row->id;
                            $referencia['nombre'] = $row->nombre_ref;
                            $referencia['cargo'] = $row->cargo_ref;
                            $referencia['telefono'] = $row->telefono_ref;
                            $referencia['email'] = $row->email_ref;
                            $info_referencias[] = $referencia;
                        }
                        $data = array('usuario' => $usuario, 'info_media' => $info_media, 'info_superior' => $info_superior, 'info_capacitaciones' => $info_capacitaciones, 'info_antecedentes' => $info_antecedentes, 'info_referencias' => $info_referencias);
                        $this->load->view('curriculum', $data);
                    }
                    else {
                        redirect('/home', 'refresh');
                    }
                }
            }
        }
    }

    public function topdf() {
        if ($this->session->userdata('rol') == 'admin') {
            $this->load->library('pdf');
            if ($this->uri->segment(3) === FALSE) {
                redirect('/home', 'refresh');
            }
            else{
                $rut = $this->uri->segment(3);
                $query = $this->mod_usuario->obtener($rut);
                $usuario = array();
                $nombres = '';
                $apellidos = '';
                foreach ($query->result() as $row) {
                    $nombres = $row->nombres;
                    $apellidos = $row->apellidos;
                    $usuario['nombres'] = $row->nombres;
                    $usuario['apellidos'] = $row->apellidos;
                    $usuario['celular'] = $row->celular;
                    $usuario['ciudad'] = $row->ciudad;
                    $usuario['telefono'] = $row->telefono;
                    $usuario['email'] = $row->email;
                }
                $query = $this->mod_antecedente->obtener_tipo($rut, 'media');
                $info_media = array();
                $institucion = array();
                foreach ($query->result() as $row) {
                    $institucion['id'] = $row->id;
                    $institucion['nombre'] = $row->institucion;
                    $institucion['tipo'] = $row->tipo_institucion;
                    $institucion['pais'] = $row->pais;
                    $institucion['inicio'] = $row->inicio;
                    $institucion['termino'] = $row->termino;
                    $institucion['titulo_profesional'] = $row->titulo_profesional;
                    $info_media[] = $institucion;
                }
                $query = $this->mod_antecedente->obtener_tipo($rut, 'superior');
                $info_superior = array();
                $institucion = array();
                foreach ($query->result() as $row) {
                    $institucion['id'] = $row->id;
                    $institucion['nombre'] = $row->institucion;
                    $institucion['tipo'] = $row->tipo_institucion;
                    $institucion['pais'] = $row->pais;
                    $institucion['carrera'] = $row->carrera;
                    $institucion['inicio'] = $row->inicio;
                    $institucion['termino'] = $row->termino;
                    $institucion['situacion'] = $row->situacion;
                    $institucion['grado'] = $row->grado;
                    $info_superior[] = $institucion;
                }
                $query = $this->mod_antecedente->obtener_tipo($rut,'especializacion');
                $info_especializaciones = array();
                $especializacion = array();
                foreach($query->result() as $row){
                    $especializacion['id'] = $row->id;
                    $especializacion['institucion'] = $row->institucion;
                    $especializacion['nombre'] = $row->nombre_capacitacion;
                    $especializacion['mes_inicio'] = $row->mes_inicio;
                    $especializacion['mes_termino'] = $row->mes_termino;
                    $especializacion['inicio'] = $row->inicio;
                    $especializacion['termino'] = $row->termino;
                    $especializacion['tipo_esp'] = $row->tipo_esp;
                    $info_especializaciones[] = $especializacion;
                }
                $query = $this->mod_antecedente->obtener_tipo($rut, 'capacitacion');
                $info_capacitaciones = array();
                $capacitacion = array();
                foreach ($query->result() as $row) {
                    $capacitacion['id'] = $row->id;
                    $capacitacion['institucion'] = $row->institucion;
                    $capacitacion['nombre'] = $row->nombre_capacitacion;
                    $capacitacion['mes_inicio'] = $row->mes_inicio;
                    $capacitacion['mes_termino'] = $row->mes_termino;
                    $capacitacion['inicio'] = $row->inicio;
                    $capacitacion['termino'] = $row->termino;
                    $capacitacion['horas'] = $row->horas;
                    $capacitacion['ciudad'] = $row->ciudad;
                    $info_capacitaciones[] = $capacitacion;
                }
                $query = $this->mod_laboral->obtener_tipo($rut, 'laboral');
                $info_antecedentes = array();
                $antecedente = array();
                foreach ($query->result() as $row) {
                    $antecedente['id'] = $row->id;
                    $antecedente['nombre_empresa'] = $row->nombre_empresa;
                    $antecedente['pais'] = $row->pais;
                    $antecedente['mes_inicio'] = $row->mes_inicio;
                    $antecedente['mes_termino'] = $row->mes_termino;
                    $antecedente['inicio'] = $row->inicio;
                    $antecedente['termino'] = $row->termino;
                    $antecedente['cargo'] = $row->cargo;
                    $antecedente['funciones'] = $row->funciones;
                    $antecedente['logros'] = $row->logros;
                    $info_antecedentes[] = $antecedente;
                }
                $query = $this->mod_laboral->obtener_tipo($rut, 'referencia');
                $info_referencias = array();
                $referencia = array();
                foreach ($query->result() as $row) {
                    $referencia['id'] = $row->id;
                    $referencia['nombre'] = $row->nombre_ref;
                    $referencia['cargo'] = $row->cargo_ref;
                    $referencia['telefono'] = $row->telefono_ref;
                    $referencia['email'] = $row->email_ref;
                    $referencia['relacion'] = $row->relacion_ref;
                    $referencia['institucion'] = $row->institucion_ref;
                    $info_referencias[] = $referencia;
                }
                $this->pdf = new Pdf();
                $this->pdf->AddPage();
                $this->pdf->SetFont('Arial','B',12);
                $this->pdf->Cell(200, 10, 'CURRICULUM VITAE PARA POSTULAR AL HOSPITAL REGIONAL DE TALCA',0,1,'C');
                $this->pdf->Cell(200, 0.5, 'DR. CESAR GARAVAGNO BUROTTO',0,0,'C');
                $this->pdf->Ln(10);
                $this->pdf->SetFont('Arial','',9);
                $this->pdf->MultiCell(190,5,utf8_decode('En base a las buenas prácticas laborales impulsadas por la Presidencia de la República se ha eliminado del formato  de Curriculum la necesidad de adjuntar fotografías, fecha de nacimiento, estado civil y dirección.'),0,'C');
                $this->pdf->Ln(5);
                $this->pdf->SetFont('Arial','B',10);
                $this->pdf->Cell(50,6,'NOMBRE DEL CARGO AL QUE POSTULA',0);
                $this->pdf->SetFont('Arial','',10);
                $this->pdf->Cell(30);
                $this->pdf->Cell(110,6,'',1,0);
                $this->pdf->Ln(12);
                $this->pdf->SetFont('Arial','B',11);
                $this->pdf->Cell(60, 8,'ANTECEDENTES PERSONALES',0);
                $this->pdf->Ln(12);
                $this->pdf->SetFont('Arial','',9);
                $this->pdf->Cell(60, 6,'NOMBRE COMPLETO',1);
                $this->pdf->Cell(130, 6,utf8_decode($usuario['nombres'].' '.$usuario['apellidos']),1);
                $this->pdf->Ln(6);
                $this->pdf->Cell(60, 6,utf8_decode('CÉDULA DE IDENTIDAD'),1);
                $this->pdf->Cell(130, 6,$rut,1);
                $this->pdf->Ln(6);
                $this->pdf->Cell(60, 6,'CIUDAD DE RESIDENCIA',1);
                $this->pdf->Cell(130, 6,utf8_decode($usuario['ciudad']),1);
                $this->pdf->Ln(6);
                $this->pdf->Cell(60, 6,utf8_decode('TELÉFONO FIJO'),1);
                $this->pdf->Cell(130, 6,$usuario['telefono'],1);
                $this->pdf->Ln(6);
                $this->pdf->Cell(60, 6,utf8_decode('TELÉFONO MÓVIL'),1);
                $this->pdf->Cell(130, 6,$usuario['celular'],1);
                $this->pdf->Ln(6);
                $this->pdf->Cell(60, 6,utf8_decode('CORREO ELECTRÓNICO'),1);
                $this->pdf->Cell(130, 6,utf8_decode($usuario['email']),1);
                $this->pdf->Ln(12);
                $this->pdf->SetFont('Arial','B',11);
                $this->pdf->Cell(60, 8,utf8_decode('ANTECEDENTES ACADÉMICOS'),0);
                $this->pdf->Ln(6);
                if(sizeof($info_media)>0){
                    $this->pdf->Ln(12);
                    $this->pdf->SetFont('Arial','B',9);
                    $this->pdf->Cell(60, 6,utf8_decode('EDUCACIÓN MEDIA'),0);
                    $this->pdf->SetFont('Arial','',9);
                    $this->pdf->Ln(12);
                    foreach ($info_media as $institucion) {
                        $this->pdf->Cell(60, 6,utf8_decode('TÍTULO PROFESIONAL'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['titulo_profesional']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('COLEGIO O LICEO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['nombre']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('TIPO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['tipo']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('CIUDAD - PAÍS'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['pais']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('FECHA DE TITULACIÓN O EGRESO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['termino']),1);
                        $this->pdf->Ln(12);
                    }
                }
                if(sizeof($info_superior)>0){
                    $this->pdf->SetFont('Arial','B',9);
                    $this->pdf->Cell(60, 6,utf8_decode('EDUCACIÓN SUPERIOR'),0);
                    $this->pdf->SetFont('Arial','',9);
                    $this->pdf->Ln(12);
                    foreach ($info_superior as $institucion) {
                        $this->pdf->Cell(60, 6,utf8_decode('CARRERA'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['carrera']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('INSTITUCIÓN'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['nombre']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('TIPO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['tipo']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('SITUACIÓN'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['situacion']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('GRADO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['grado']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('CIUDAD - PAÍS'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['pais']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('FECHA DE TITULACIÓN'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['termino']),1);
                        $this->pdf->Ln(12);
                    }
                }
                if(sizeof($info_especializaciones)>0){
                    $this->pdf->AddPage();
                    $this->pdf->SetFont('Arial','B',9);
                    $this->pdf->Cell(60, 6,utf8_decode('ESTUDIOS DE ESPECIALIZACIÓN'),0);
                    $this->pdf->SetFont('Arial','',9);
                    $this->pdf->Ln(12);
                    foreach ($info_especializaciones as $esp) {
                        $this->pdf->Cell(60, 6,utf8_decode('NOMBRE'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($esp['nombre']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('TIPO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($esp['tipo_esp']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('INSTITUCIÓN'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($esp['institucion']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('PERÍODO DE ESTUDIO'),1);
                        $this->pdf->Cell(25, 6,utf8_decode('DESDE'),1);
                        $this->pdf->Cell(40, 6,utf8_decode($esp['mes_inicio'].'/'.$esp['inicio']),1);
                        $this->pdf->Cell(25, 6,utf8_decode('HASTA'),1);
                        $this->pdf->Cell(40, 6,utf8_decode($esp['mes_inicio'].'/'.$esp['inicio']),1);
                        $this->pdf->Ln(12);
                    }
                }
                if(sizeof($info_capacitaciones)>0){
                    $this->pdf->SetFont('Arial','B',9);
                    $this->pdf->Cell(60, 6,utf8_decode('CURSOS DE CAPACITACIÓN'),0);
                    $this->pdf->SetFont('Arial','',9);
                    $this->pdf->Ln(12);
                    foreach ($info_capacitaciones as $capacitacion) {
                        $this->pdf->Cell(60, 6,utf8_decode('NOMBRE DEL CURSO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($capacitacion['nombre']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('INSTITUCIÓN'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($capacitacion['institucion']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('LUGAR'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($capacitacion['ciudad']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('PERÍODO DE ESTUDIO'),1);
                        $this->pdf->Cell(25, 6,utf8_decode('DESDE'),1);
                        $this->pdf->Cell(40, 6,utf8_decode($capacitacion['mes_inicio'].'/'.$capacitacion['inicio']),1);
                        $this->pdf->Cell(25, 6,utf8_decode('HASTA'),1);
                        $this->pdf->Cell(40, 6,utf8_decode($capacitacion['mes_termino'].'/'.$capacitacion['termino']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('HORAS'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($capacitacion['horas']),1);
                        $this->pdf->Ln(12);
                    }
                }
                if(sizeof($info_antecedentes)>0){
                    $this->pdf->AddPage();
                    $this->pdf->SetFont('Arial','B',9);
                    $this->pdf->Cell(60, 6,utf8_decode('EXPERIENCIA LABORAL'),0);
                    $this->pdf->SetFont('Arial','',9);
                    $this->pdf->Ln(12);
                    foreach ($info_antecedentes as $antecedente) {
                        $this->pdf->Cell(60, 6,utf8_decode('NOMBRE DEL CARGO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($antecedente['cargo']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('NOMBRE INSTITUCIÓN O EMPRESA'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($antecedente['nombre_empresa']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('PERÍODO DE CONTRATO'),1);
                        $this->pdf->Cell(25, 6,utf8_decode('DESDE'),1);
                        $this->pdf->Cell(40, 6,utf8_decode($antecedente['mes_inicio'].'/'.$antecedente['inicio']),1);
                        $this->pdf->Cell(25, 6,utf8_decode('HASTA'),1);
                        $this->pdf->Cell(40, 6,utf8_decode($antecedente['mes_termino'].'/'.$antecedente['termino']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('FUNCIONES Y TAREAS'),1);
                        $this->pdf->MultiCell(130,6,utf8_decode($antecedente['funciones']),1);
                        $this->pdf->Ln(6);
                    }
                }
                if(sizeof($info_referencias)>0){
                    //$this->pdf->AddPage();
                    $this->pdf->SetFont('Arial','B',9);
                    $this->pdf->Cell(60, 6,utf8_decode('REFERENCIAS LABORALES'),0);
                    $this->pdf->SetFont('Arial','',9);
                    $this->pdf->MultiCell(130,6,utf8_decode('(La institución se reserva el derecho a solicitar referencias laborales a las instituciones en las que el postulante haya señalado trabajar que estén señaladas en este currículo)'),0);
                    $this->pdf->Ln(12);
                    foreach ($info_referencias as $referencia) {
                        $this->pdf->Cell(60, 6,utf8_decode('NOMBRE'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($referencia['nombre']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('CARGO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($referencia['cargo']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('INSTITUCIÓN'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($referencia['institucion']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('CORREO ELECTRÓNICO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($referencia['email']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('TELÉFONO DE CONTACTO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($referencia['telefono']),1);
                        $this->pdf->Ln(12);
                    }
                }
                ob_end_clean();
                $this->pdf->Output("cv_".strtolower($apellidos)."_".strtolower($nombres)."_".$rut.".pdf", 'I');
            }
        }
        else {
            redirect('/home', 'refresh');
        }
    }

    public function ver_cv() {
        if ($this->session->userdata('rut') == $this->uri->segment(3)) {
            $this->load->library('pdf');
            if ($this->uri->segment(3) === FALSE) {
                redirect('/home', 'refresh');
            }
            else{
                $rut = $this->uri->segment(3);
                $query = $this->mod_usuario->obtener($rut);
                $usuario = array();
                $nombres = '';
                $apellidos = '';
                foreach ($query->result() as $row) {
                    $nombres = $row->nombres;
                    $apellidos = $row->apellidos;
                    $usuario['nombres'] = $row->nombres;
                    $usuario['apellidos'] = $row->apellidos;
                    $usuario['celular'] = $row->celular;
                    $usuario['ciudad'] = $row->ciudad;
                    $usuario['telefono'] = $row->telefono;
                    $usuario['email'] = $row->email;
                }
                $query = $this->mod_antecedente->obtener_tipo($rut, 'media');
                $info_media = array();
                $institucion = array();
                foreach ($query->result() as $row) {
                    $institucion['id'] = $row->id;
                    $institucion['nombre'] = $row->institucion;
                    $institucion['tipo'] = $row->tipo_institucion;
                    $institucion['pais'] = $row->pais;
                    $institucion['inicio'] = $row->inicio;
                    $institucion['termino'] = $row->termino;
                    $institucion['titulo_profesional'] = $row->titulo_profesional;
                    $info_media[] = $institucion;
                }
                $query = $this->mod_antecedente->obtener_tipo($rut, 'superior');
                $info_superior = array();
                $institucion = array();
                foreach ($query->result() as $row) {
                    $institucion['id'] = $row->id;
                    $institucion['nombre'] = $row->institucion;
                    $institucion['tipo'] = $row->tipo_institucion;
                    $institucion['pais'] = $row->pais;
                    $institucion['carrera'] = $row->carrera;
                    $institucion['inicio'] = $row->inicio;
                    $institucion['termino'] = $row->termino;
                    $institucion['situacion'] = $row->situacion;
                    $institucion['grado'] = $row->grado;
                    $info_superior[] = $institucion;
                }
                $query = $this->mod_antecedente->obtener_tipo($rut,'especializacion');
                $info_especializaciones = array();
                $especializacion = array();
                foreach($query->result() as $row){
                    $especializacion['id'] = $row->id;
                    $especializacion['institucion'] = $row->institucion;
                    $especializacion['nombre'] = $row->nombre_capacitacion;
                    $especializacion['mes_inicio'] = $row->mes_inicio;
                    $especializacion['mes_termino'] = $row->mes_termino;
                    $especializacion['inicio'] = $row->inicio;
                    $especializacion['termino'] = $row->termino;
                    $especializacion['tipo_esp'] = $row->tipo_esp;
                    $info_especializaciones[] = $especializacion;
                }
                $query = $this->mod_antecedente->obtener_tipo($rut, 'capacitacion');
                $info_capacitaciones = array();
                $capacitacion = array();
                foreach ($query->result() as $row) {
                    $capacitacion['id'] = $row->id;
                    $capacitacion['institucion'] = $row->institucion;
                    $capacitacion['nombre'] = $row->nombre_capacitacion;
                    $capacitacion['mes_inicio'] = $row->mes_inicio;
                    $capacitacion['mes_termino'] = $row->mes_termino;
                    $capacitacion['inicio'] = $row->inicio;
                    $capacitacion['termino'] = $row->termino;
                    $capacitacion['horas'] = $row->horas;
                    $capacitacion['ciudad'] = $row->ciudad;
                    $info_capacitaciones[] = $capacitacion;
                }
                $query = $this->mod_laboral->obtener_tipo($rut, 'laboral');
                $info_antecedentes = array();
                $antecedente = array();
                foreach ($query->result() as $row) {
                    $antecedente['id'] = $row->id;
                    $antecedente['nombre_empresa'] = $row->nombre_empresa;
                    $antecedente['pais'] = $row->pais;
                    $antecedente['mes_inicio'] = $row->mes_inicio;
                    $antecedente['mes_termino'] = $row->mes_termino;
                    $antecedente['inicio'] = $row->inicio;
                    $antecedente['termino'] = $row->termino;
                    $antecedente['cargo'] = $row->cargo;
                    $antecedente['funciones'] = $row->funciones;
                    $antecedente['logros'] = $row->logros;
                    $info_antecedentes[] = $antecedente;
                }
                $query = $this->mod_laboral->obtener_tipo($rut, 'referencia');
                $info_referencias = array();
                $referencia = array();
                foreach ($query->result() as $row) {
                    $referencia['id'] = $row->id;
                    $referencia['nombre'] = $row->nombre_ref;
                    $referencia['cargo'] = $row->cargo_ref;
                    $referencia['telefono'] = $row->telefono_ref;
                    $referencia['email'] = $row->email_ref;
                    $referencia['relacion'] = $row->relacion_ref;
                    $referencia['institucion'] = $row->institucion_ref;
                    $info_referencias[] = $referencia;
                }
                $this->pdf = new Pdf();
                $this->pdf->AddPage();
                $this->pdf->SetFont('Arial','B',12);
                $this->pdf->Cell(200, 10, 'CURRICULUM VITAE PARA POSTULAR AL HOSPITAL REGIONAL DE TALCA',0,1,'C');
                $this->pdf->Cell(200, 0.5, 'DR. CESAR GARAVAGNO BUROTTO',0,0,'C');
                $this->pdf->Ln(10);
                $this->pdf->SetFont('Arial','',9);
                $this->pdf->MultiCell(190,5,utf8_decode('En base a las buenas prácticas laborales impulsadas por la Presidencia de la República se ha eliminado del formato  de Curriculum la necesidad de adjuntar fotografías, fecha de nacimiento, estado civil y dirección.'),0,'C');
                $this->pdf->Ln(5);
                $this->pdf->SetFont('Arial','B',10);
                $this->pdf->Cell(50,6,'NOMBRE DEL CARGO AL QUE POSTULA',0);
                $this->pdf->SetFont('Arial','',10);
                $this->pdf->Cell(30);
                $this->pdf->Cell(110,6,'',1,0);
                $this->pdf->Ln(12);
                $this->pdf->SetFont('Arial','B',11);
                $this->pdf->Cell(60, 8,'ANTECEDENTES PERSONALES',0);
                $this->pdf->Ln(12);
                $this->pdf->SetFont('Arial','',9);
                $this->pdf->Cell(60, 6,'NOMBRE COMPLETO',1);
                $this->pdf->Cell(130, 6,utf8_decode($usuario['nombres'].' '.$usuario['apellidos']),1);
                $this->pdf->Ln(6);
                $this->pdf->Cell(60, 6,utf8_decode('CÉDULA DE IDENTIDAD'),1);
                $this->pdf->Cell(130, 6,$rut,1);
                $this->pdf->Ln(6);
                $this->pdf->Cell(60, 6,'CIUDAD DE RESIDENCIA',1);
                $this->pdf->Cell(130, 6,utf8_decode($usuario['ciudad']),1);
                $this->pdf->Ln(6);
                $this->pdf->Cell(60, 6,utf8_decode('TELÉFONO FIJO'),1);
                $this->pdf->Cell(130, 6,$usuario['telefono'],1);
                $this->pdf->Ln(6);
                $this->pdf->Cell(60, 6,utf8_decode('TELÉFONO MÓVIL'),1);
                $this->pdf->Cell(130, 6,$usuario['celular'],1);
                $this->pdf->Ln(6);
                $this->pdf->Cell(60, 6,utf8_decode('CORREO ELECTRÓNICO'),1);
                $this->pdf->Cell(130, 6,utf8_decode($usuario['email']),1);
                $this->pdf->Ln(12);
                $this->pdf->SetFont('Arial','B',11);
                $this->pdf->Cell(60, 8,utf8_decode('ANTECEDENTES ACADÉMICOS'),0);
                $this->pdf->Ln(6);
                if(sizeof($info_media)>0){
                    $this->pdf->Ln(12);
                    $this->pdf->SetFont('Arial','B',9);
                    $this->pdf->Cell(60, 6,utf8_decode('EDUCACIÓN MEDIA'),0);
                    $this->pdf->SetFont('Arial','',9);
                    $this->pdf->Ln(12);
                    foreach ($info_media as $institucion) {
                        $this->pdf->Cell(60, 6,utf8_decode('TÍTULO PROFESIONAL'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['titulo_profesional']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('COLEGIO O LICEO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['nombre']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('TIPO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['tipo']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('CIUDAD - PAÍS'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['pais']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('FECHA DE TITULACIÓN O EGRESO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['termino']),1);
                        $this->pdf->Ln(12);
                    }
                }
                if(sizeof($info_superior)>0){
                    $this->pdf->SetFont('Arial','B',9);
                    $this->pdf->Cell(60, 6,utf8_decode('EDUCACIÓN SUPERIOR'),0);
                    $this->pdf->SetFont('Arial','',9);
                    $this->pdf->Ln(12);
                    foreach ($info_superior as $institucion) {
                        $this->pdf->Cell(60, 6,utf8_decode('CARRERA'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['carrera']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('INSTITUCIÓN'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['nombre']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('TIPO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['tipo']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('SITUACIÓN'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['situacion']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('GRADO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['grado']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('CIUDAD - PAÍS'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['pais']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('FECHA DE TITULACIÓN'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($institucion['termino']),1);
                        $this->pdf->Ln(12);
                    }
                }
                if(sizeof($info_especializaciones)>0){
                    $this->pdf->AddPage();
                    $this->pdf->SetFont('Arial','B',9);
                    $this->pdf->Cell(60, 6,utf8_decode('ESTUDIOS DE ESPECIALIZACIÓN'),0);
                    $this->pdf->SetFont('Arial','',9);
                    $this->pdf->Ln(12);
                    foreach ($info_especializaciones as $esp) {
                        $this->pdf->Cell(60, 6,utf8_decode('NOMBRE'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($esp['nombre']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('TIPO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($esp['tipo_esp']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('INSTITUCIÓN'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($esp['institucion']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('PERÍODO DE ESTUDIO'),1);
                        $this->pdf->Cell(25, 6,utf8_decode('DESDE'),1);
                        $this->pdf->Cell(40, 6,utf8_decode($esp['mes_inicio'].'/'.$esp['inicio']),1);
                        $this->pdf->Cell(25, 6,utf8_decode('HASTA'),1);
                        $this->pdf->Cell(40, 6,utf8_decode($esp['mes_inicio'].'/'.$esp['inicio']),1);
                        $this->pdf->Ln(12);
                    }
                }
                if(sizeof($info_capacitaciones)>0){
                    $this->pdf->SetFont('Arial','B',9);
                    $this->pdf->Cell(60, 6,utf8_decode('CURSOS DE CAPACITACIÓN'),0);
                    $this->pdf->SetFont('Arial','',9);
                    $this->pdf->Ln(12);
                    foreach ($info_capacitaciones as $capacitacion) {
                        $this->pdf->Cell(60, 6,utf8_decode('NOMBRE DEL CURSO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($capacitacion['nombre']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('INSTITUCIÓN'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($capacitacion['institucion']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('LUGAR'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($capacitacion['ciudad']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('PERÍODO DE ESTUDIO'),1);
                        $this->pdf->Cell(25, 6,utf8_decode('DESDE'),1);
                        $this->pdf->Cell(40, 6,utf8_decode($capacitacion['mes_inicio'].'/'.$capacitacion['inicio']),1);
                        $this->pdf->Cell(25, 6,utf8_decode('HASTA'),1);
                        $this->pdf->Cell(40, 6,utf8_decode($capacitacion['mes_termino'].'/'.$capacitacion['termino']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('HORAS'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($capacitacion['horas']),1);
                        $this->pdf->Ln(12);
                    }
                }
                if(sizeof($info_antecedentes)>0){
                    $this->pdf->AddPage();
                    $this->pdf->SetFont('Arial','B',9);
                    $this->pdf->Cell(60, 6,utf8_decode('EXPERIENCIA LABORAL'),0);
                    $this->pdf->SetFont('Arial','',9);
                    $this->pdf->Ln(12);
                    foreach ($info_antecedentes as $antecedente) {
                        $this->pdf->Cell(60, 6,utf8_decode('NOMBRE DEL CARGO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($antecedente['cargo']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('NOMBRE INSTITUCIÓN O EMPRESA'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($antecedente['nombre_empresa']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('PERÍODO DE CONTRATO'),1);
                        $this->pdf->Cell(25, 6,utf8_decode('DESDE'),1);
                        $this->pdf->Cell(40, 6,utf8_decode($antecedente['mes_inicio'].'/'.$antecedente['inicio']),1);
                        $this->pdf->Cell(25, 6,utf8_decode('HASTA'),1);
                        $this->pdf->Cell(40, 6,utf8_decode($antecedente['mes_termino'].'/'.$antecedente['termino']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('FUNCIONES Y TAREAS'),1);
                        $this->pdf->MultiCell(130,6,utf8_decode($antecedente['funciones']),1);
                        $this->pdf->Ln(6);
                    }
                }
                if(sizeof($info_referencias)>0){
                   $this->pdf->AddPage();
                    $this->pdf->SetFont('Arial','B',9);
                    $this->pdf->Cell(60, 6,utf8_decode('REFERENCIAS LABORALES'),0);
                    $this->pdf->SetFont('Arial','',9);
                    $this->pdf->MultiCell(130,6,utf8_decode('(La institución se reserva el derecho a solicitar referencias laborales a las instituciones en las que el postulante haya señalado trabajar que estén señaladas en este currículo)'),0);
                    $this->pdf->Ln(12);
                    foreach ($info_referencias as $referencia) {
                        $this->pdf->Cell(60, 6,utf8_decode('NOMBRE'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($referencia['nombre']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('CARGO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($referencia['cargo']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('INSTITUCIÓN'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($referencia['institucion']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('CORREO ELECTRÓNICO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($referencia['email']),1);
                        $this->pdf->Ln(6);
                        $this->pdf->Cell(60, 6,utf8_decode('TELÉFONO DE CONTACTO'),1);
                        $this->pdf->Cell(130, 6,utf8_decode($referencia['telefono']),1);
                        $this->pdf->Ln(12);
                    }
                }
                ob_end_clean();
                $this->pdf->Output("cv_".strtolower($apellidos)."_".strtolower($nombres)."_".$rut.".pdf", 'I');
            }
        }
        else {
            redirect('/home', 'refresh');
        }
    }


    public function descargar_oferta(){
      if ($this->session->userdata('rol') == 'admin') {
        if ($this->uri->segment(3) === FALSE) {
            redirect('/home', 'refresh');
        }
        else{
          $id_oferta = $this->uri->segment(3);
          $query = $this->mod_postulacion->obtener_postulaciones_oferta($id_oferta);
          foreach($query->result() as $row){
            $rut_postulante = $row->rut_postulante;
            $query = $this->mod_usuario->obtener($rut_postulante);
            $usuario = array();
            $nombres = '';
            $apellidos = '';
            foreach ($query->result() as $row) {
                $nombres = $row->nombres;
                $apellidos = $row->apellidos;
                $usuario['nombres'] = $row->nombres;
                $usuario['apellidos'] = $row->apellidos;
                $usuario['celular'] = $row->celular;
                $usuario['ciudad'] = $row->ciudad;
                $usuario['telefono'] = $row->telefono;
                $usuario['email'] = $row->email;
            }
            $query = $this->mod_antecedente->obtener_tipo($rut_postulante, 'media');
            $info_media = array();
            $institucion = array();
            foreach ($query->result() as $row) {
                $institucion['id'] = $row->id;
                $institucion['nombre'] = $row->institucion;
                $institucion['tipo'] = $row->tipo_institucion;
                $institucion['pais'] = $row->pais;
                $institucion['inicio'] = $row->inicio;
                $institucion['termino'] = $row->termino;
                $institucion['titulo_profesional'] = $row->titulo_profesional;
                $info_media[] = $institucion;
            }
            $query = $this->mod_antecedente->obtener_tipo($rut_postulante, 'superior');
            $info_superior = array();
            $institucion = array();
            foreach ($query->result() as $row) {
                $institucion['id'] = $row->id;
                $institucion['nombre'] = $row->institucion;
                $institucion['tipo'] = $row->tipo_institucion;
                $institucion['pais'] = $row->pais;
                $institucion['carrera'] = $row->carrera;
                $institucion['inicio'] = $row->inicio;
                $institucion['termino'] = $row->termino;
                $institucion['situacion'] = $row->situacion;
                $institucion['grado'] = $row->grado;
                $info_superior[] = $institucion;
            }
            $query = $this->mod_antecedente->obtener_tipo($rut_postulante,'especializacion');
            $info_especializaciones = array();
            $especializacion = array();
            foreach($query->result() as $row){
                $especializacion['id'] = $row->id;
                $especializacion['institucion'] = $row->institucion;
                $especializacion['nombre'] = $row->nombre_capacitacion;
                $especializacion['mes_inicio'] = $row->mes_inicio;
                $especializacion['mes_termino'] = $row->mes_termino;
                $especializacion['inicio'] = $row->inicio;
                $especializacion['termino'] = $row->termino;
                $especializacion['tipo_esp'] = $row->tipo_esp;
                $info_especializaciones[] = $especializacion;
            }
            $query = $this->mod_antecedente->obtener_tipo($rut_postulante, 'capacitacion');
            $info_capacitaciones = array();
            $capacitacion = array();
            foreach ($query->result() as $row) {
                $capacitacion['id'] = $row->id;
                $capacitacion['institucion'] = $row->institucion;
                $capacitacion['nombre'] = $row->nombre_capacitacion;
                $capacitacion['mes_inicio'] = $row->mes_inicio;
                $capacitacion['mes_termino'] = $row->mes_termino;
                $capacitacion['inicio'] = $row->inicio;
                $capacitacion['termino'] = $row->termino;
                $capacitacion['horas'] = $row->horas;
                $capacitacion['ciudad'] = $row->ciudad;
                $info_capacitaciones[] = $capacitacion;
            }
            $query = $this->mod_laboral->obtener_tipo($rut_postulante, 'laboral');
            $info_antecedentes = array();
            $antecedente = array();
            foreach ($query->result() as $row) {
                $antecedente['id'] = $row->id;
                $antecedente['nombre_empresa'] = $row->nombre_empresa;
                $antecedente['pais'] = $row->pais;
                $antecedente['mes_inicio'] = $row->mes_inicio;
                $antecedente['mes_termino'] = $row->mes_termino;
                $antecedente['inicio'] = $row->inicio;
                $antecedente['termino'] = $row->termino;
                $antecedente['cargo'] = $row->cargo;
                $antecedente['funciones'] = $row->funciones;
                $antecedente['logros'] = $row->logros;
                $info_antecedentes[] = $antecedente;
            }
            $query = $this->mod_laboral->obtener_tipo($rut_postulante, 'referencia');
            $info_referencias = array();
            $referencia = array();
            foreach ($query->result() as $row) {
                $referencia['id'] = $row->id;
                $referencia['nombre'] = $row->nombre_ref;
                $referencia['cargo'] = $row->cargo_ref;
                $referencia['telefono'] = $row->telefono_ref;
                $referencia['email'] = $row->email_ref;
                $referencia['relacion'] = $row->relacion_ref;
                $referencia['institucion'] = $row->institucion_ref;
                $info_referencias[] = $referencia;
            }
            $this->pdf = new Pdf();
            $this->pdf->AddPage();
            $this->pdf->SetFont('Arial','B',12);
            $this->pdf->Cell(200, 10, 'CURRICULUM VITAE PARA POSTULAR AL HOSPITAL REGIONAL DE TALCA',0,1,'C');
            $this->pdf->Cell(200, 0.5, 'DR. CESAR GARAVAGNO BUROTTO',0,0,'C');
            $this->pdf->Ln(10);
            $this->pdf->SetFont('Arial','',9);
            $this->pdf->MultiCell(190,5,utf8_decode('En base a las buenas prácticas laborales impulsadas por la Presidencia de la República se ha eliminado del formato  de Curriculum la necesidad de adjuntar fotografías, fecha de nacimiento, estado civil y dirección.'),0,'C');
            $this->pdf->Ln(5);
            $this->pdf->SetFont('Arial','B',10);
            $this->pdf->Cell(50,6,'NOMBRE DEL CARGO AL QUE POSTULA',0);
            $this->pdf->SetFont('Arial','',10);
            $this->pdf->Cell(30);
            $this->pdf->Cell(110,6,'',1,0);
            $this->pdf->Ln(12);
            $this->pdf->SetFont('Arial','B',11);
            $this->pdf->Cell(60, 8,'ANTECEDENTES PERSONALES',0);
            $this->pdf->Ln(12);
            $this->pdf->SetFont('Arial','',9);
            $this->pdf->Cell(60, 6,'NOMBRE COMPLETO',1);
            $this->pdf->Cell(130, 6,utf8_decode($usuario['nombres'].' '.$usuario['apellidos']),1);
            $this->pdf->Ln(6);
            $this->pdf->Cell(60, 6,utf8_decode('CÉDULA DE IDENTIDAD'),1);
            $this->pdf->Cell(130, 6,$rut_postulante,1);
            $this->pdf->Ln(6);
            $this->pdf->Cell(60, 6,'CIUDAD DE RESIDENCIA',1);
            $this->pdf->Cell(130, 6,utf8_decode($usuario['ciudad']),1);
            $this->pdf->Ln(6);
            $this->pdf->Cell(60, 6,utf8_decode('TELÉFONO FIJO'),1);
            $this->pdf->Cell(130, 6,$usuario['telefono'],1);
            $this->pdf->Ln(6);
            $this->pdf->Cell(60, 6,utf8_decode('TELÉFONO MÓVIL'),1);
            $this->pdf->Cell(130, 6,$usuario['celular'],1);
            $this->pdf->Ln(6);
            $this->pdf->Cell(60, 6,utf8_decode('CORREO ELECTRÓNICO'),1);
            $this->pdf->Cell(130, 6,utf8_decode($usuario['email']),1);
            $this->pdf->Ln(12);
            $this->pdf->SetFont('Arial','B',11);
            $this->pdf->Cell(60, 8,utf8_decode('ANTECEDENTES ACADÉMICOS'),0);
            $this->pdf->Ln(6);
            if(sizeof($info_media)>0){
                $this->pdf->Ln(12);
                $this->pdf->SetFont('Arial','B',9);
                $this->pdf->Cell(60, 6,utf8_decode('EDUCACIÓN MEDIA'),0);
                $this->pdf->SetFont('Arial','',9);
                $this->pdf->Ln(12);
                foreach ($info_media as $institucion) {
                    $this->pdf->Cell(60, 6,utf8_decode('TÍTULO PROFESIONAL'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($institucion['titulo_profesional']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('COLEGIO O LICEO'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($institucion['nombre']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('TIPO'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($institucion['tipo']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('CIUDAD - PAÍS'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($institucion['pais']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('FECHA DE TITULACIÓN O EGRESO'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($institucion['termino']),1);
                    $this->pdf->Ln(12);
                }
            }
            if(sizeof($info_superior)>0){
                $this->pdf->SetFont('Arial','B',9);
                $this->pdf->Cell(60, 6,utf8_decode('EDUCACIÓN SUPERIOR'),0);
                $this->pdf->SetFont('Arial','',9);
                $this->pdf->Ln(12);
                foreach ($info_superior as $institucion) {
                    $this->pdf->Cell(60, 6,utf8_decode('CARRERA'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($institucion['carrera']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('INSTITUCIÓN'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($institucion['nombre']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('TIPO'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($institucion['tipo']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('SITUACIÓN'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($institucion['situacion']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('GRADO'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($institucion['grado']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('CIUDAD - PAÍS'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($institucion['pais']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('FECHA DE TITULACIÓN'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($institucion['termino']),1);
                    $this->pdf->Ln(12);
                }
            }
            $this->pdf->AddPage();
            if(sizeof($info_especializaciones)>0){
                $this->pdf->SetFont('Arial','B',9);
                $this->pdf->Cell(60, 6,utf8_decode('ESTUDIOS DE ESPECIALIZACIÓN'),0);
                $this->pdf->SetFont('Arial','',9);
                $this->pdf->Ln(12);
                foreach ($info_especializaciones as $esp) {
                    $this->pdf->Cell(60, 6,utf8_decode('NOMBRE'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($esp['nombre']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('TIPO'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($esp['tipo_esp']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('INSTITUCIÓN'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($esp['institucion']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('PERÍODO DE ESTUDIO'),1);
                    $this->pdf->Cell(25, 6,utf8_decode('DESDE'),1);
                    $this->pdf->Cell(40, 6,utf8_decode($esp['mes_inicio'].'/'.$esp['inicio']),1);
                    $this->pdf->Cell(25, 6,utf8_decode('HASTA'),1);
                    $this->pdf->Cell(40, 6,utf8_decode($esp['mes_inicio'].'/'.$esp['inicio']),1);
                    $this->pdf->Ln(12);
                }
            }
            if(sizeof($info_capacitaciones)>0){
                $this->pdf->SetFont('Arial','B',9);
                $this->pdf->Cell(60, 6,utf8_decode('CURSOS DE CAPACITACIÓN'),0);
                $this->pdf->SetFont('Arial','',9);
                $this->pdf->Ln(12);
                foreach ($info_capacitaciones as $capacitacion) {
                    $this->pdf->Cell(60, 6,utf8_decode('NOMBRE DEL CURSO'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($capacitacion['nombre']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('INSTITUCIÓN'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($capacitacion['institucion']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('LUGAR'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($capacitacion['ciudad']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('PERÍODO DE ESTUDIO'),1);
                    $this->pdf->Cell(25, 6,utf8_decode('DESDE'),1);
                    $this->pdf->Cell(40, 6,utf8_decode($capacitacion['mes_inicio'].'/'.$capacitacion['inicio']),1);
                    $this->pdf->Cell(25, 6,utf8_decode('HASTA'),1);
                    $this->pdf->Cell(40, 6,utf8_decode($capacitacion['mes_termino'].'/'.$capacitacion['termino']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('HORAS'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($capacitacion['horas']),1);
                    $this->pdf->Ln(12);
                }
            }
            if(sizeof($info_antecedentes)>0){
                $this->pdf->AddPage();
                $this->pdf->SetFont('Arial','B',9);
                $this->pdf->Cell(60, 6,utf8_decode('EXPERIENCIA LABORAL'),0);
                $this->pdf->SetFont('Arial','',9);
                $this->pdf->Ln(12);
                foreach ($info_antecedentes as $antecedente) {
                    $this->pdf->Cell(60, 6,utf8_decode('NOMBRE DEL CARGO'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($antecedente['cargo']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('NOMBRE INSTITUCIÓN O EMPRESA'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($antecedente['nombre_empresa']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('PERÍODO DE CONTRATO'),1);
                    $this->pdf->Cell(25, 6,utf8_decode('DESDE'),1);
                    $this->pdf->Cell(40, 6,utf8_decode($antecedente['mes_inicio'].'/'.$antecedente['inicio']),1);
                    $this->pdf->Cell(25, 6,utf8_decode('HASTA'),1);
                    $this->pdf->Cell(40, 6,utf8_decode($antecedente['mes_termino'].'/'.$antecedente['termino']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('FUNCIONES Y TAREAS'),1);
                    $this->pdf->MultiCell(130,6,utf8_decode($antecedente['funciones']),1);
                    $this->pdf->Ln(6);
                }
            }
            if(sizeof($info_referencias)>0){
               $this->pdf->AddPage();
                $this->pdf->SetFont('Arial','B',9);
                $this->pdf->Cell(60, 6,utf8_decode('REFERENCIAS LABORALES'),0);
                $this->pdf->SetFont('Arial','',9);
                $this->pdf->MultiCell(130,6,utf8_decode('(La institución se reserva el derecho a solicitar referencias laborales a las instituciones en las que el postulante haya señalado trabajar que estén señaladas en este currículo)'),0);
                $this->pdf->Ln(12);
                foreach ($info_referencias as $referencia) {
                    $this->pdf->Cell(60, 6,utf8_decode('NOMBRE'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($referencia['nombre']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('CARGO'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($referencia['cargo']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('INSTITUCIÓN'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($referencia['institucion']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('CORREO ELECTRÓNICO'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($referencia['email']),1);
                    $this->pdf->Ln(6);
                    $this->pdf->Cell(60, 6,utf8_decode('TELÉFONO DE CONTACTO'),1);
                    $this->pdf->Cell(130, 6,utf8_decode($referencia['telefono']),1);
                    $this->pdf->Ln(12);
                }
            }
            //ob_end_clean();
            $name = "cv_".$rut_postulante.".pdf";
            //$name = 'cv.pdf';
            $data = $this->pdf->Output('', 'S');
            $this->zip->add_data($name, $data);
          }
          $this->zip->download('listado_postulantes_oferta_'.$id_oferta.'.zip');
      }

    }
    else{
      redirect('/home', 'refresh');
    }
  }

  public function postulantes_oferta(){
    if ($this->session->userdata('rol') == 'admin') {
      if ($this->uri->segment(3) === FALSE) {
          redirect('/home', 'refresh');
      }
      else{
        $id_oferta = $this->uri->segment(3);
        $query = $this->mod_postulacion->obtener_postulaciones_oferta($id_oferta);
        $this->pdf = new Pdf();
        foreach($query->result() as $row){
          $rut_postulante = $row->rut_postulante;
          $cargo = $row->titulo;
          $query = $this->mod_usuario->obtener($rut_postulante);
          $usuario = array();
          $nombres = '';
          $apellidos = '';
          foreach ($query->result() as $row) {
              $nombres = $row->nombres;
              $apellidos = $row->apellidos;
              $usuario['nombres'] = $row->nombres;
              $usuario['apellidos'] = $row->apellidos;
              $usuario['celular'] = $row->celular;
              $usuario['ciudad'] = $row->ciudad;
              $usuario['telefono'] = $row->telefono;
              $usuario['email'] = $row->email;
          }
          $this->pdf->SetName($apellidos." ".$nombres,$rut_postulante);
          $query = $this->mod_antecedente->obtener_tipo($rut_postulante, 'media');
          $info_media = array();
          $institucion = array();
          foreach ($query->result() as $row) {
              $institucion['id'] = $row->id;
              $institucion['nombre'] = $row->institucion;
              $institucion['tipo'] = $row->tipo_institucion;
              $institucion['pais'] = $row->pais;
              $institucion['inicio'] = $row->inicio;
              $institucion['termino'] = $row->termino;
              $institucion['titulo_profesional'] = $row->titulo_profesional;
              $info_media[] = $institucion;
          }
          $query = $this->mod_antecedente->obtener_tipo($rut_postulante, 'superior');
          $info_superior = array();
          $institucion = array();
          foreach ($query->result() as $row) {
              $institucion['id'] = $row->id;
              $institucion['nombre'] = $row->institucion;
              $institucion['tipo'] = $row->tipo_institucion;
              $institucion['pais'] = $row->pais;
              $institucion['carrera'] = $row->carrera;
              $institucion['inicio'] = $row->inicio;
              $institucion['termino'] = $row->termino;
              $institucion['situacion'] = $row->situacion;
              $institucion['grado'] = $row->grado;
              $info_superior[] = $institucion;
          }
          $query = $this->mod_antecedente->obtener_tipo($rut_postulante,'especializacion');
          $info_especializaciones = array();
          $especializacion = array();
          foreach($query->result() as $row){
              $especializacion['id'] = $row->id;
              $especializacion['institucion'] = $row->institucion;
              $especializacion['nombre'] = $row->nombre_capacitacion;
              $especializacion['mes_inicio'] = $row->mes_inicio;
              $especializacion['mes_termino'] = $row->mes_termino;
              $especializacion['inicio'] = $row->inicio;
              $especializacion['termino'] = $row->termino;
              $especializacion['tipo_esp'] = $row->tipo_esp;
              $info_especializaciones[] = $especializacion;
          }
          $query = $this->mod_antecedente->obtener_tipo($rut_postulante, 'capacitacion');
          $info_capacitaciones = array();
          $capacitacion = array();
          foreach ($query->result() as $row) {
              $capacitacion['id'] = $row->id;
              $capacitacion['institucion'] = $row->institucion;
              $capacitacion['nombre'] = $row->nombre_capacitacion;
              $capacitacion['mes_inicio'] = $row->mes_inicio;
              $capacitacion['mes_termino'] = $row->mes_termino;
              $capacitacion['inicio'] = $row->inicio;
              $capacitacion['termino'] = $row->termino;
              $capacitacion['horas'] = $row->horas;
              $capacitacion['ciudad'] = $row->ciudad;
              $info_capacitaciones[] = $capacitacion;
          }
          $query = $this->mod_laboral->obtener_tipo($rut_postulante, 'laboral');
          $info_antecedentes = array();
          $antecedente = array();
          foreach ($query->result() as $row) {
              $antecedente['id'] = $row->id;
              $antecedente['nombre_empresa'] = $row->nombre_empresa;
              $antecedente['pais'] = $row->pais;
              $antecedente['mes_inicio'] = $row->mes_inicio;
              $antecedente['mes_termino'] = $row->mes_termino;
              $antecedente['inicio'] = $row->inicio;
              $antecedente['termino'] = $row->termino;
              $antecedente['cargo'] = $row->cargo;
              $antecedente['funciones'] = $row->funciones;
              $antecedente['logros'] = $row->logros;
              $info_antecedentes[] = $antecedente;
          }
          $query = $this->mod_laboral->obtener_tipo($rut_postulante, 'referencia');
          $info_referencias = array();
          $referencia = array();
          foreach ($query->result() as $row) {
              $referencia['id'] = $row->id;
              $referencia['nombre'] = $row->nombre_ref;
              $referencia['cargo'] = $row->cargo_ref;
              $referencia['telefono'] = $row->telefono_ref;
              $referencia['email'] = $row->email_ref;
              $referencia['relacion'] = $row->relacion_ref;
              $referencia['institucion'] = $row->institucion_ref;
              $info_referencias[] = $referencia;
          }

          $this->pdf->AddPage();
          $this->pdf->SetFont('Arial','B',12);
          $this->pdf->Cell(200, 10, 'CURRICULUM VITAE PARA POSTULAR AL HOSPITAL REGIONAL DE TALCA',0,1,'C');
          $this->pdf->Cell(200, 0.5, 'DR. CESAR GARAVAGNO BUROTTO',0,0,'C');
          $this->pdf->Ln(10);
          $this->pdf->SetFont('Arial','',9);
          $this->pdf->MultiCell(190,5,utf8_decode('En base a las buenas prácticas laborales impulsadas por la Presidencia de la República se ha eliminado del formato  de Curriculum la necesidad de adjuntar fotografías, fecha de nacimiento, estado civil y dirección.'),0,'C');
          $this->pdf->Ln(5);
          $this->pdf->SetFont('Arial','B',10);
          $this->pdf->Cell(50,6,'NOMBRE DEL CARGO AL QUE POSTULA',0);
          $this->pdf->SetFont('Arial','',10);
          $this->pdf->Cell(30);
          $this->pdf->Cell(110,6,utf8_decode($cargo),1,0);
          $this->pdf->Ln(12);
          $this->pdf->SetFont('Arial','B',11);
          $this->pdf->Cell(60, 8,'ANTECEDENTES PERSONALES',0);
          $this->pdf->Ln(12);
          $this->pdf->SetFont('Arial','',9);
          $this->pdf->Cell(60, 6,'NOMBRE COMPLETO',1);
          $this->pdf->Cell(130, 6,utf8_decode($usuario['nombres'].' '.$usuario['apellidos']),1);
          $this->pdf->Ln(6);
          $this->pdf->Cell(60, 6,utf8_decode('CÉDULA DE IDENTIDAD'),1);
          $this->pdf->Cell(130, 6,$rut_postulante,1);
          $this->pdf->Ln(6);
          $this->pdf->Cell(60, 6,'CIUDAD DE RESIDENCIA',1);
          $this->pdf->Cell(130, 6,utf8_decode($usuario['ciudad']),1);
          $this->pdf->Ln(6);
          $this->pdf->Cell(60, 6,utf8_decode('TELÉFONO FIJO'),1);
          $this->pdf->Cell(130, 6,$usuario['telefono'],1);
          $this->pdf->Ln(6);
          $this->pdf->Cell(60, 6,utf8_decode('TELÉFONO MÓVIL'),1);
          $this->pdf->Cell(130, 6,$usuario['celular'],1);
          $this->pdf->Ln(6);
          $this->pdf->Cell(60, 6,utf8_decode('CORREO ELECTRÓNICO'),1);
          $this->pdf->Cell(130, 6,utf8_decode($usuario['email']),1);
          $this->pdf->Ln(12);
          $this->pdf->SetFont('Arial','B',11);
          $this->pdf->Cell(60, 8,utf8_decode('ANTECEDENTES ACADÉMICOS'),0);
          $this->pdf->Ln(6);
          if(sizeof($info_media)>0){
              $this->pdf->Ln(12);
              $this->pdf->SetFont('Arial','B',9);
              $this->pdf->Cell(60, 6,utf8_decode('EDUCACIÓN MEDIA'),0);
              $this->pdf->SetFont('Arial','',9);
              $this->pdf->Ln(12);
              foreach ($info_media as $institucion) {
                  $this->pdf->Cell(60, 6,utf8_decode('TÍTULO PROFESIONAL'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($institucion['titulo_profesional']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('COLEGIO O LICEO'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($institucion['nombre']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('TIPO'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($institucion['tipo']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('CIUDAD - PAÍS'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($institucion['pais']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('FECHA DE TITULACIÓN O EGRESO'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($institucion['termino']),1);
                  $this->pdf->Ln(12);
              }
          }
          if(sizeof($info_superior)>0){
              $this->pdf->SetFont('Arial','B',9);
              $this->pdf->Cell(60, 6,utf8_decode('EDUCACIÓN SUPERIOR'),0);
              $this->pdf->SetFont('Arial','',9);
              $this->pdf->Ln(12);
              foreach ($info_superior as $institucion) {
                  $this->pdf->Cell(60, 6,utf8_decode('CARRERA'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($institucion['carrera']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('INSTITUCIÓN'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($institucion['nombre']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('TIPO'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($institucion['tipo']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('SITUACIÓN'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($institucion['situacion']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('GRADO'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($institucion['grado']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('CIUDAD - PAÍS'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($institucion['pais']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('FECHA DE TITULACIÓN'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($institucion['termino']),1);
                  $this->pdf->Ln(12);
              }
          }
          if(sizeof($info_especializaciones)>0){
              $this->pdf->AddPage();
              $this->pdf->SetFont('Arial','B',9);
              $this->pdf->Cell(60, 6,utf8_decode('ESTUDIOS DE ESPECIALIZACIÓN'),0);
              $this->pdf->SetFont('Arial','',9);
              $this->pdf->Ln(12);
              foreach ($info_especializaciones as $esp) {
                  $this->pdf->Cell(60, 6,utf8_decode('NOMBRE'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($esp['nombre']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('TIPO'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($esp['tipo_esp']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('INSTITUCIÓN'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($esp['institucion']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('PERÍODO DE ESTUDIO'),1);
                  $this->pdf->Cell(25, 6,utf8_decode('DESDE'),1);
                  $this->pdf->Cell(40, 6,utf8_decode($esp['mes_inicio'].'/'.$esp['inicio']),1);
                  $this->pdf->Cell(25, 6,utf8_decode('HASTA'),1);
                  $this->pdf->Cell(40, 6,utf8_decode($esp['mes_inicio'].'/'.$esp['inicio']),1);
                  $this->pdf->Ln(12);
              }
          }
          if(sizeof($info_capacitaciones)>0){
              $this->pdf->SetFont('Arial','B',9);
              $this->pdf->Cell(60, 6,utf8_decode('CURSOS DE CAPACITACIÓN'),0);
              $this->pdf->SetFont('Arial','',9);
              $this->pdf->Ln(12);
              foreach ($info_capacitaciones as $capacitacion) {
                  $this->pdf->Cell(60, 6,utf8_decode('NOMBRE DEL CURSO'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($capacitacion['nombre']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('INSTITUCIÓN'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($capacitacion['institucion']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('LUGAR'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($capacitacion['ciudad']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('PERÍODO DE ESTUDIO'),1);
                  $this->pdf->Cell(25, 6,utf8_decode('DESDE'),1);
                  $this->pdf->Cell(40, 6,utf8_decode($capacitacion['mes_inicio'].'/'.$capacitacion['inicio']),1);
                  $this->pdf->Cell(25, 6,utf8_decode('HASTA'),1);
                  $this->pdf->Cell(40, 6,utf8_decode($capacitacion['mes_termino'].'/'.$capacitacion['termino']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('HORAS'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($capacitacion['horas']),1);
                  $this->pdf->Ln(12);
              }
          }
          if(sizeof($info_antecedentes)>0){
              $this->pdf->AddPage();
              $this->pdf->SetFont('Arial','B',9);
              $this->pdf->Cell(60, 6,utf8_decode('EXPERIENCIA LABORAL'),0);
              $this->pdf->SetFont('Arial','',9);
              $this->pdf->Ln(12);
              foreach ($info_antecedentes as $antecedente) {
                  $this->pdf->Cell(60, 6,utf8_decode('NOMBRE DEL CARGO'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($antecedente['cargo']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('NOMBRE INSTITUCIÓN O EMPRESA'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($antecedente['nombre_empresa']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('PERÍODO DE CONTRATO'),1);
                  $this->pdf->Cell(25, 6,utf8_decode('DESDE'),1);
                  $this->pdf->Cell(40, 6,utf8_decode($antecedente['mes_inicio'].'/'.$antecedente['inicio']),1);
                  $this->pdf->Cell(25, 6,utf8_decode('HASTA'),1);
                  $this->pdf->Cell(40, 6,utf8_decode($antecedente['mes_termino'].'/'.$antecedente['termino']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('FUNCIONES Y TAREAS'),1);
                  $this->pdf->MultiCell(130,6,utf8_decode($antecedente['funciones']),1);
                  $this->pdf->Ln(6);
              }
          }
          if(sizeof($info_referencias)>0){
              $this->pdf->SetFont('Arial','B',9);
              $this->pdf->Cell(60, 6,utf8_decode('REFERENCIAS LABORALES'),0);
              $this->pdf->SetFont('Arial','',9);
              $this->pdf->MultiCell(130,6,utf8_decode('(La institución se reserva el derecho a solicitar referencias laborales a las instituciones en las que el postulante haya señalado trabajar que estén señaladas en este currículo)'),0);
              $this->pdf->Ln(12);
              foreach ($info_referencias as $referencia) {
                  $this->pdf->Cell(60, 6,utf8_decode('NOMBRE'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($referencia['nombre']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('CARGO'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($referencia['cargo']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('INSTITUCIÓN'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($referencia['institucion']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('CORREO ELECTRÓNICO'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($referencia['email']),1);
                  $this->pdf->Ln(6);
                  $this->pdf->Cell(60, 6,utf8_decode('TELÉFONO DE CONTACTO'),1);
                  $this->pdf->Cell(130, 6,utf8_decode($referencia['telefono']),1);
                  $this->pdf->Ln(12);
              }
          }

        }
        ob_end_clean();
        $this->pdf->Output("cv_postulantes_oferta".$id_oferta.".pdf", 'I');
    }

  }
  else{
    redirect('/home', 'refresh');
  }
}

}
