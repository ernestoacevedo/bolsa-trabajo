<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->library('email');
        $this->load->model('mod_usuario');
        $this->load->model('mod_mensajes');
        $this->load->helper('correo');
        $this->load->library('form_validation');
  }

  public function registrar(){
    $this->form_validation->set_rules('rut_reg', 'Usuario', 'required');
    $this->form_validation->set_rules('email_reg', 'Correo electrónico', 'required');
    $this->form_validation->set_rules('pass_reg', 'Contraseña', 'required');
    if ($this->form_validation->run() == FALSE){
      echo json_encode(array("status"=>false,"message"=>validation_errors()));
    }
    else{
      $data = array();
      $data['rut'] = $this->input->post('rut_reg');
      $data['nombres'] = $this->input->post('nombres_reg');
      $data['apellidos'] = $this->input->post('apellidos_reg');
      $data['email'] = $this->input->post('email_reg');
      $data['password'] = sha1($this->input->post('pass_reg'));
      $registro = false;
      $msg = '';
      $query = $this->mod_usuario->obtener($this->input->post('rut_reg'));
      if ($query->num_rows()<=0){
        $data['reemplazo'] = 0;
        $data['recibir_info'] = 1;
        $registro = $this->mod_usuario->nuevo($data);
        if($registro){
          $config = obtener_config();
          $this->email->initialize($config);
          $query = $this->mod_mensajes->obtener();
          $data['mensaje'] = '';
          if ($query->num_rows() > 0){
            foreach($query->result() as $row){
              $data['mensaje'] = $row->msg_registro;
            }
          }
          $html_email = $this->load->view('mail', $data, true);
          $this->email->from(obtener_correo());
          $this->email->to($this->input->post('email_reg'));
          $this->email->subject('Registro satisfactorio');
          $this->email->message($html_email);
          $status = $this->email->send();

          $msg = 'Usuario registrado correctamente';
        }
        else{
          $msg = 'Error al registrar la cuenta';
        }
      }
      else{
        $msg = 'El RUT ya se encuentra registrado';
      }
      echo json_encode(array("status"=>$registro,"message"=>$msg));
    }
  }

  public function editar(){
    $data['rut'] = $this->input->post('rut');
    $data['nombres'] = $this->input->post('nombres');
    $data['apellidos'] = $this->input->post('apellidos');
    $data['ciudad'] = $this->input->post('ciudad');
    $data['telefono'] = $this->input->post('telefono');
    $data['celular'] = $this->input->post('celular');
    $data['email'] = $this->input->post('email');
    $data['reemplazo'] = ($this->input->post('reemplazo')=='on') ? 1 : 0;
    $data['tipo_reemplazo'] = $this->input->post('tipo_reemplazo');
    $data['actualizacion'] = $this->input->post('actualizacion');
    $data['titulo'] = $this->input->post('titulo');
    $data['nivel_estudios'] = $this->input->post('nivel_estudios');
    $update = $this->mod_usuario->actualizar($data);
    if($update){
      echo json_encode(array("status"=>$update,"message"=>"Información actualizada correctamente","redirect"=>site_url('dashboard/curriculum')));
    }
    else{
      echo json_encode(array("status"=>$update,"message"=>"Ocurrió un error en la actualización"));
    }
  }

  public function editar_config(){
    $data['rut'] = $this->session->userdata('rut');
    $data['recibir_info'] = $this->input->post('recibir_info');
    $update = $this->mod_usuario->actualizar($data);
    if($update){
      echo json_encode(array("status"=>true,"message"=>"Ajustes actualizados correctamente"));
    }
    else{
      echo json_encode(array("status"=>false,"message"=>"Ocurrió un error al guardar los cambios"));
    }
  }

  public function editar_academica(){
    $data['rut'] = $this->session->userdata('rut');
    $data['info_academica'] = $this->input->post('info_academica');
    $update = $this->mod_usuario->actualizar($data);
    if($update){
      echo json_encode(array("status"=>$update,"message"=>"Información actualizada correctamente"));
    }
    else{
      echo json_encode(array("status"=>$update,"message"=>"Ocurrió un error en la actualización"));
    }
  }

  public function editar_laboral(){
    $data['rut'] = $this->session->userdata('rut');
    $data['info_laboral'] = $this->input->post('info_laboral');
    $update = $this->mod_usuario->actualizar($data);
    if($update){
      echo json_encode(array("status"=>$update,"message"=>"Información actualizada correctamente"));
    }
    else{
      echo json_encode(array("status"=>$update,"message"=>"Ocurrió un error en la actualización"));
    }
  }

  public function eliminar(){
    if($this->session->userdata('rol')=='admin'){
      $id = $this->uri->segment(3);
      $this->mod_usuario->eliminar($id);
      redirect('/admin/usuarios','refresh');
    }
  }
}
