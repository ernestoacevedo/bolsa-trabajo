<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Documento extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->model('mod_documento');
        $this->load->library('upload');
  }

  public function subir(){
    $config['upload_path'] = './documentos';
    $config['allowed_types'] = 'pdf|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx|rtf|txt';
    $config['max_size'] = '20000';
    $config['encrypt_name'] = true;
    $config['remove_spaces'] = true;
    $this->upload->initialize($config);
    if (!$this->upload->do_upload('file')){
      $data = array('error' => $this->upload->display_errors());
      redirect('dashboard/curriculum', 'refresh');
    }
    else{
      $info = array('upload_data' => $this->upload->data());
      $data['rut_postulante'] = $this->session->userdata('rut');
      $data['tipo_documento'] = $this->input->post('tipo_documento');
      $data['ruta_documento'] = $info['upload_data']['file_name'];
      $this->mod_documento->nuevo($data);
      redirect('dashboard/curriculum', 'refresh');
    }
  }

  public function eliminar(){
    if($this->session->userdata('rol')=='user'){
      $id = $this->uri->segment(3);
      $this->mod_documento->eliminar($id);
      redirect('/dashboard/curriculum','refresh');
    }
  }

  public function documentos_usuario(){
    if ($this->uri->segment(3) === FALSE) {
        redirect('/home', 'refresh');
    }
    else{
      $query = $this->mod_documento->obtener($this->uri->segment(3));
      $documentos = array();
      $documento = array();
      foreach($query->result() as $row){
        $documento['id'] = $row->id;
        $documento['rut_postulante'] = $row->rut_postulante;
        $documento['tipo_documento'] = $row->tipo_documento;
        $documento['ruta_documento'] = base_url().'documentos/'.$row->ruta_documento;
        $documentos[] = $documento;
      }
      echo json_encode($documentos);
    }

  }
}
