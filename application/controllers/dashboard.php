<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->model('mod_usuario');
        $this->load->model('mod_documento');
        $this->load->model('mod_antecedente');
        $this->load->model('mod_oferta');
        $this->load->model('mod_postulacion');
        $this->load->model('mod_laboral');
  }

  public function index(){
    $user = $this->session->userdata('user_data');
    if (!isset($user)) {
      redirect('/home','refresh');
    }
    else {
      if($this->session->userdata('rol')=='user'){
        $query = $this->mod_oferta->ultimas_ofertas();
        $ofertas = array();
        $oferta = array();
        foreach($query->result() as $row){
          $oferta['id'] = $row->id;
          if(strlen($row->titulo)>60){
            $oferta['titulo'] = substr($row->titulo,0,60).'...';
          }
          else{
            $oferta['titulo'] = $row->titulo;
          }
          $oferta['descripcion'] = $row->descripcion;
          $oferta['horas'] = $row->horas;
          $oferta['puestos'] = $row->puestos;
          $oferta['remuneracion'] = $row->remuneracion;
          $oferta['fecha_publicacion'] = date("d/m/Y", strtotime($row->fecha_publicacion));
          $oferta['fecha_cierre'] = $row->fecha_cierre;
          $oferta['caracteristicas'] = $row->caracteristicas;
          $oferta['deseables'] = $row->deseables;
          $oferta['activa'] = $row->activa;
          $oferta['legal'] = $row->legal;
          $oferta['antecedentes'] = $row->antecedentes;
          $oferta['entrevista_psico'] = $row->entrevista_psico;
          $oferta['entrevista_pers'] = $row->entrevista_pers;
          $oferta['seleccion_final'] = $row->seleccion_final;

          $ofertas[] = $oferta;
        }
        $query = $this->mod_postulacion->obtener_ultimos_seleccionados();
        $listado = array();
        $postulante = array();
        foreach($query->result() as $row){
          $postulante['rut'] = $row->rut_postulante;
          $listado[] = $postulante;
        }
        $data = array('recientes'=>$ofertas,'seleccionados'=>$listado);
        $this->load->view('header');
        $this->load->view('dashboard',$data);
        $this->load->view('footer');
      }
      else{
        redirect('/home','refresh');
      }
    }
  }

  public function curriculum(){
    if($this->session->userdata('rol')=='user'){
      $query = $this->mod_usuario->obtener($this->session->userdata('rut'));
      $usuario = array();
      foreach($query->result() as $row){
        $usuario['rut'] = $row->rut;
        $usuario['nombres'] = $row->nombres;
        $usuario['apellidos'] = $row->apellidos;
        $usuario['telefono'] = $row->telefono;
        $usuario['celular'] = $row->celular;
        $usuario['ciudad'] = $row->ciudad;
        $usuario['email'] = $row->email;
        $usuario['reemplazo'] = $row->reemplazo;
        $usuario['tipo_reemplazo'] = $row->tipo_reemplazo;
        $usuario['titulo'] = $row->titulo;
        $usuario['nivel_estudios'] = $row->nivel_estudios;
      }
      $query = $this->mod_antecedente->obtener_tipo($this->session->userdata('rut'),'media');
      $info_media = array();
      $institucion = array();
      foreach($query->result() as $row){
        $institucion['id'] = $row->id;
        if(strlen($row->institucion)>30){
        $institucion['nombre'] = substr($row->institucion,0,30).'...';
        }
        else{
        $institucion['nombre'] = $row->institucion;
        }
        $institucion['tipo'] = $row->tipo_institucion;
        $institucion['pais'] = $row->pais;
        $institucion['inicio'] = $row->inicio;
        $institucion['termino'] = $row->termino;
        $info_media[] = $institucion;
      }
      $query = $this->mod_antecedente->obtener_tipo($this->session->userdata('rut'),'superior');
      $info_superior = array();
      $institucion = array();
      foreach($query->result() as $row){
        $institucion['id'] = $row->id;
        if(strlen($row->institucion)>30){
        $institucion['nombre'] = substr($row->institucion,0,30).'...';
        }
        else{
        $institucion['nombre'] = $row->institucion;
        }
        $institucion['tipo'] = $row->tipo_institucion;
        $institucion['pais'] = $row->pais;
        if(strlen($row->carrera)>30){
        $institucion['carrera'] = substr($row->carrera,0,30).'...';
        }
        else{
        $institucion['carrera'] = $row->carrera;
        }
        $institucion['inicio'] = $row->inicio;
        $institucion['termino'] = $row->termino;
        $institucion['situacion'] = $row->situacion;
        $institucion['grado'] = $row->grado;
        $info_superior[] = $institucion;
      }
      $query = $this->mod_antecedente->obtener_tipo($this->session->userdata('rut'),'especializacion');
      $info_especializaciones = array();
      $especializacion = array();
      foreach($query->result() as $row){
        $especializacion['id'] = $row->id;
        $especializacion['institucion'] = $row->institucion;
        $especializacion['nombre'] = $row->nombre_capacitacion;
        $especializacion['mes_inicio'] = $row->mes_inicio;
        $especializacion['mes_termino'] = $row->mes_termino;
        $especializacion['inicio'] = $row->inicio;
        $especializacion['termino'] = $row->termino;
        $especializacion['tipo_esp'] = $row->tipo_esp;
        $info_especializaciones[] = $especializacion;
      }
      $query = $this->mod_antecedente->obtener_tipo($this->session->userdata('rut'),'capacitacion');
      $info_capacitaciones = array();
      $capacitacion = array();
      foreach($query->result() as $row){
        $capacitacion['id'] = $row->id;
        $capacitacion['institucion'] = $row->institucion;
        $capacitacion['nombre'] = $row->nombre_capacitacion;
        $capacitacion['mes_inicio'] = $row->mes_inicio;
        $capacitacion['mes_termino'] = $row->mes_termino;
        $capacitacion['inicio'] = $row->inicio;
        $capacitacion['termino'] = $row->termino;
        $capacitacion['horas'] = $row->horas;
        $info_capacitaciones[] = $capacitacion;
      }
      $query = $this->mod_laboral->obtener_tipo($this->session->userdata('rut'),'laboral');
      $info_antecedentes = array();
      $antecedente = array();
      foreach($query->result() as $row){
        $antecedente['id'] = $row->id;
        $antecedente['nombre_empresa'] = $row->nombre_empresa;
        $antecedente['pais'] = $row->pais;
        $antecedente['mes_inicio'] = $row->mes_inicio;
        $antecedente['mes_termino'] = $row->mes_termino;
        $antecedente['inicio'] = $row->inicio;
        $antecedente['termino'] = $row->termino;
        $antecedente['cargo'] = $row->cargo;
        $antecedente['funciones'] = $row->funciones;
        $antecedente['logros'] = $row->logros;
        $info_antecedentes[] = $antecedente;
      }
      $query = $this->mod_laboral->obtener_tipo($this->session->userdata('rut'),'referencia');
      $info_referencias = array();
      $referencia = array();
      foreach($query->result() as $row){
        $referencia['id'] = $row->id;
        $referencia['nombre'] = $row->nombre_ref;
        $referencia['cargo'] = $row->cargo_ref;
        $referencia['telefono'] = $row->telefono_ref;
        $referencia['email'] = $row->email_ref;
        $info_referencias[] = $referencia;
      }
      $query = $this->mod_documento->obtener($this->session->userdata('rut'));
      $documentos = array();
      $documento = array();
      foreach($query->result() as $row){
        $documento['id'] = $row->id;
        $documento['rut_postulante'] = $row->rut_postulante;
        $documento['tipo_documento'] = $row->tipo_documento;
        $documento['ruta_documento'] = $row->ruta_documento;
        $documentos[] = $documento;
      }
      $data = array('usuario'=>$usuario,'info_media'=>$info_media,'info_superior'=>$info_superior,'info_especializaciones'=>$info_especializaciones,'info_capacitaciones'=>$info_capacitaciones,'info_antecedentes'=>$info_antecedentes,'info_referencias'=>$info_referencias,'documentos'=>$documentos);
      $this->load->view('header');
      $this->load->view('curriculum_vitae',$data);
      $this->load->view('footer');
    }
    else{
      redirect('/home','refresh');
    }

  }

  public function personal(){
    if($this->session->userdata('rol')=='user'){
      $query = $this->mod_usuario->obtener($this->session->userdata('rut'));
      $usuario = array();
      foreach($query->result() as $row){
        $usuario['nombres'] = $row->nombres;
        $usuario['apellidos'] = $row->apellidos;
        $usuario['telefono'] = $row->telefono;
        $usuario['celular'] = $row->celular;
        $usuario['ciudad'] = $row->ciudad;
        $usuario['email'] = $row->email;

      }
      $data = array('usuario'=>$usuario);
      $this->load->view('header');
      $this->load->view('info_personal',$data);
      $this->load->view('footer');
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function academica(){
    if($this->session->userdata('rol')=='user'){
      $query = $this->mod_antecedente->obtener_tipo($this->session->userdata('rut'),'media');
      $info_media = array();
      $institucion = array();
      foreach($query->result() as $row){
        $institucion['id'] = $row->id;
        if(strlen($row->institucion)>30){
    		$institucion['nombre'] = substr($row->institucion,0,30).'...';
    	}
    	else{
			$institucion['nombre'] = $row->institucion;
    	}
        $institucion['tipo'] = $row->tipo_institucion;
        $institucion['pais'] = $row->pais;
        $institucion['inicio'] = $row->inicio;
        $institucion['termino'] = $row->termino;
        $info_media[] = $institucion;
      }
      $query = $this->mod_antecedente->obtener_tipo($this->session->userdata('rut'),'superior');
      $info_superior = array();
      $institucion = array();
      foreach($query->result() as $row){
        $institucion['id'] = $row->id;
        if(strlen($row->institucion)>30){
    		$institucion['nombre'] = substr($row->institucion,0,30).'...';
    	}
    	else{
			$institucion['nombre'] = $row->institucion;
    	}
        $institucion['tipo'] = $row->tipo_institucion;
        $institucion['pais'] = $row->pais;
        if(strlen($row->carrera)>30){
    		$institucion['carrera'] = substr($row->carrera,0,30).'...';
    	}
    	else{
			$institucion['carrera'] = $row->carrera;
    	}
        $institucion['inicio'] = $row->inicio;
        $institucion['termino'] = $row->termino;
        $institucion['situacion'] = $row->situacion;
        $institucion['grado'] = $row->grado;
        $info_superior[] = $institucion;
      }
      $query = $this->mod_antecedente->obtener_tipo($this->session->userdata('rut'),'especializacion');
      $info_especializaciones = array();
      $especializacion = array();
      foreach($query->result() as $row){
        $especializacion['id'] = $row->id;
        $especializacion['institucion'] = $row->institucion;
        $especializacion['nombre'] = $row->nombre_capacitacion;
        $especializacion['mes_inicio'] = $row->mes_inicio;
        $especializacion['mes_termino'] = $row->mes_termino;
        $especializacion['inicio'] = $row->inicio;
        $especializacion['termino'] = $row->termino;
        $especializacion['tipo_esp'] = $row->tipo_esp;
        $info_especializaciones[] = $especializacion;
      }
      $query = $this->mod_antecedente->obtener_tipo($this->session->userdata('rut'),'capacitacion');
      $info_capacitaciones = array();
      $capacitacion = array();
      foreach($query->result() as $row){
        $capacitacion['id'] = $row->id;
        $capacitacion['institucion'] = $row->institucion;
        $capacitacion['nombre'] = $row->nombre_capacitacion;
        $capacitacion['mes_inicio'] = $row->mes_inicio;
        $capacitacion['mes_termino'] = $row->mes_termino;
        $capacitacion['inicio'] = $row->inicio;
        $capacitacion['termino'] = $row->termino;
        $capacitacion['horas'] = $row->horas;
        $info_capacitaciones[] = $capacitacion;
      }
      $data = array('info_media'=>$info_media,'info_superior'=>$info_superior,'info_especializaciones'=>$info_especializaciones,'info_capacitaciones'=>$info_capacitaciones);
      $this->load->view('header');
      $this->load->view('info_academica',$data);
      $this->load->view('footer');
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function laboral(){
    if($this->session->userdata('rol')=='user'){
      $query = $this->mod_laboral->obtener_tipo($this->session->userdata('rut'),'laboral');
      $info_antecedentes = array();
      $antecedente = array();
      foreach($query->result() as $row){
        $antecedente['id'] = $row->id;
        $antecedente['nombre_empresa'] = $row->nombre_empresa;
        $antecedente['pais'] = $row->pais;
        $antecedente['mes_inicio'] = $row->mes_inicio;
        $antecedente['mes_termino'] = $row->mes_termino;
        $antecedente['inicio'] = $row->inicio;
        $antecedente['termino'] = $row->termino;
        $antecedente['cargo'] = $row->cargo;
        $antecedente['funciones'] = $row->funciones;
        $antecedente['logros'] = $row->logros;
        $info_antecedentes[] = $antecedente;
      }
      $query = $this->mod_laboral->obtener_tipo($this->session->userdata('rut'),'referencia');
      $info_referencias = array();
      $referencia = array();
      foreach($query->result() as $row){
        $referencia['id'] = $row->id;
        $referencia['nombre'] = $row->nombre_ref;
        $referencia['cargo'] = $row->cargo_ref;
        $referencia['telefono'] = $row->telefono_ref;
        $referencia['email'] = $row->email_ref;
        $info_referencias[] = $referencia;
      }
      $data = array('info_antecedentes'=>$info_antecedentes,'info_referencias'=>$info_referencias);
      $this->load->view('header');
      $this->load->view('info_laboral',$data);
      $this->load->view('footer');
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function documentos(){
    if($this->session->userdata('rol')=='user'){
      $query = $this->mod_documento->obtener($this->session->userdata('rut'));
      $documentos = array();
      $documento = array();
      foreach($query->result() as $row){
        $documento['id'] = $row->id;
        $documento['rut_postulante'] = $row->rut_postulante;
        $documento['tipo_documento'] = $row->tipo_documento;
        $documento['ruta_documento'] = $row->ruta_documento;
        $documentos[] = $documento;
      }
      $data = array('documentos'=>$documentos);
      $this->load->view('header');
      $this->load->view('documentos',$data);
      $this->load->view('footer');
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function ofertas(){
    if($this->session->userdata('rol')=='user'){
        $query = $this->mod_oferta->obtener_ofertas();
        $ofertas = array();
        $oferta = array();
        foreach($query->result() as $row){
          $oferta['id'] = $row->id;
          $oferta['titulo'] = $row->titulo;
          $oferta['activa'] = $row->activa;
          $oferta['fecha_publicacion'] = date("d/m/Y", strtotime($row->fecha_publicacion));
          $oferta['fecha_cierre'] = date("d/m/Y", strtotime($row->fecha_cierre));

          $ofertas[] = $oferta;
        }
        $data = array('ofertas'=>$ofertas);
        $this->load->view('header');
        $this->load->view('ofertas_user',$data);
        $this->load->view('footer');
      }
      else{
        redirect('/home','refresh');
      }
  }

  public function etapas(){
    if($this->session->userdata('rol')=='user'){
        $this->load->view('header');
        $this->load->view('etapas');
        $this->load->view('footer');
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function config(){
    if($this->session->userdata('rol')=='user'){
        $query = $this->mod_usuario->obtener($this->session->userdata('rut'));
        $usuario = array();
        foreach($query->result() as $row){
          $usuario['recibir_info'] = $row->recibir_info;
        }
        $data = array('usuario'=>$usuario);
        $this->load->view('header');
        $this->load->view('configuracion',$data);
        $this->load->view('footer');
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function historial(){
    if($this->session->userdata('rol')=='user'){
        $query = $this->mod_postulacion->historial($this->session->userdata('rut'));
        $postulaciones = array();
        $postulacion = array();
        foreach($query->result() as $row){
          $postulacion['id'] = $row->id;
          $postulacion['rut_postulante'] = $row->rut_postulante;
          $postulacion['titulo'] = $row->titulo;
          $postulacion['id_oferta'] = $row->id_oferta;
          $postulacion['nombres'] = $row->nombres;
          $postulacion['apellidos'] = $row->apellidos;
          $postulacion['fecha'] = date("d/m/Y", strtotime($row->fecha));
          $postulacion['estado'] = $row->estado;
          $postulacion['etapa'] = $row->etapa;

          $postulaciones[] = $postulacion;
        }
        $data = array('postulaciones'=>$postulaciones);
        $this->load->view('header');
        $this->load->view('historial',$data);
        $this->load->view('footer');

      }
      else{
        redirect('/home','refresh');
      }
  }
}
