<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   *	- or -
   * 		http://example.com/index.php/welcome/index
   *	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  public function __construct(){
        parent::__construct();
        $this->load->model('mod_usuario');
  }

  public function log_in(){
    $user = $this->input->post('rut_log');
		$pass = $this->input->post('pass_log');
		if($this->mod_usuario->loguear($user,$pass)){
      if($user=='admin'){
        $newdata = array(
                     'nombre'  => 'Administrador',
                     'rol' => 'admin',
                     'rut' => 'admin',
                     'logged_in' => true
                 );
        $this->session->set_userdata($newdata);

        echo json_encode(array('login'=>true,'redirect'=>site_url('admin')));
      }
      else{
        $query = $this->mod_usuario->obtener($user);
        $newdata = array();
        foreach ($query->result() as $row){
            $nombre = explode(' ', $row->nombres);
            $newdata['nombre'] = $nombre[0];
            $newdata['email'] = $row->email;
            $newdata['rut'] = $row->rut;
            $newdata['rol'] = 'user';
            $newdata['logged_in'] = true;
        }
        $this->session->set_userdata($newdata);

        echo json_encode(array('login'=>true,'redirect'=>site_url('dashboard')));
      }
		}
		else{
			echo json_encode(array('login'=>false));
		}
  }


  public function log_out(){
    $array_items = array('nombre' => '', 'email' => '', 'rut' => '','rol' => '', 'logged_in' => false);
    $this->session->unset_userdata($array_items);
    redirect('/home', 'refresh');
  }

  public function cambiar_password(){
    $user = $this->session->userdata('user_data');
    if (!isset($user)) {
      redirect('/home','refresh');
    }
    else {
      $this->load->view('header');
      $this->load->view('cambio_pass');
      $this->load->view('footer');
    }
  }

  public function cambio_password(){
    $user = $this->input->post('rut');
    $pass = $this->input->post('pass_actual');
    $newpass = $this->input->post('pass_nueva');
    if($this->mod_usuario->loguear($user,$pass)){
      $resultado = $this->mod_usuario->cambiar_password($user,$newpass);
      echo json_encode(array('cambio'=>$resultado));
    }
    else{
      echo json_encode(array('cambio'=>false,'message'=>utf8_encode('Clave incorrecta')));
    }
  }
}
