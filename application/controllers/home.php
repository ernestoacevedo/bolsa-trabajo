<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('mod_oferta');
    }

	public function index()
	{
		$query = $this->mod_oferta->obtener_ofertas_home();
    	$ofertas = array();
    	$oferta = array();
    	foreach($query->result() as $row){
    		$oferta['id'] = $row->id;
    		if(strlen($row->titulo)>60){
    			$oferta['titulo'] = substr($row->titulo,0,60).'...';
    		}
    		else{
    			$oferta['titulo'] = $row->titulo;
    		}
    		$oferta['descripcion'] = $row->descripcion;
		    $oferta['horas'] = $row->horas;
		    $oferta['puestos'] = $row->puestos;
		    $oferta['remuneracion'] = $row->remuneracion;
		    $oferta['fecha_publicacion'] = date("d/m/Y", strtotime($row->fecha_publicacion));
		    $oferta['fecha_cierre'] = $row->fecha_cierre;
		    $oferta['caracteristicas'] = $row->caracteristicas;
		    $oferta['deseables'] = $row->deseables;
		    $oferta['activa'] = $row->activa;
		    $oferta['legal'] = $row->legal;
		    $oferta['antecedentes'] = $row->antecedentes;
		    $oferta['entrevista_psico'] = $row->entrevista_psico;
		    $oferta['entrevista_pers'] = $row->entrevista_pers;
		    $oferta['seleccion_final'] = $row->seleccion_final;

    		$ofertas[] = $oferta;
    	}
    	$data = array('ofertas'=>$ofertas);
		$this->load->view('header');
		$this->load->view('home',$data);
		$this->load->view('footer');
	}

	public function instrucciones(){
		$this->load->view('header');
		$this->load->view('instrucciones_seleccion');
		$this->load->view('footer');
	}

	public function reemplazo(){
		$this->load->view('header');
		$this->load->view('reemplazo');
		$this->load->view('footer');
	}

	public function enviar_mail(){
		$nombre = $_POST['name'];
		$from = $_POST['email'];
		$telefono = $_POST['fono'];
		$to = "eacevedo@debian.cl";
		$subject = "Bolsa de reemplazos hospital de Talca";
		$message = "Se adjuntan datos de ".$nombre."\n"."Teléfono: ".$telefono."\n";

		$file_name = $_FILES['attachment']['name'];
		$temp_name = $_FILES['attachment']['tmp_name'];
		$file_type = $_FILES['attachment']['type'];

		$file = $temp_name;
		$content = chunk_split(base64_encode(file_get_contents($file)));
		$uid = md5(uniqid(time()));

		//mail headers
		$header = "From: ".$from."\r\n";
		//$header .= "Reply-To: ".$replyto."\r\n";
		$header .= "MIME-Version: 1.0\r\n";

		$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
		$header .= "This is a multi-part message in MIME format.\r\n";

		//texto
		$header .= "--".$uid."\r\n";
		$header .= "Content-Type:text/plain; charset=iso-8859-1\r\n";
		$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
		$header .= $message."\r\n\r\n";

		//multipart
		$header .= "--".$uid."\r\n";
		$header .= "Content-Type: ".$file_type."; name=\"".$file_name."\"\r\n";
		$header .= "Content-Transfer-Encoding: base64\r\n";
		$header .= "Content-Disposition: attachment; filename=\"".$file_name."\"\r\n";
		$header .= $content."\r\n\r\n";

		if(mail($to,$subject,"",$header)){
			$this->session->set_flashdata(array('type'=>'success','msg'=>'Datos enviados correctamente'));
			redirect('/home/reemplazo','refresh');
		}
		else{
			$this->session->set_flashdata(array('type'=>'alert','msg'=>'Ocurrió un problema al enviar el correo'));
			redirect('/home/reemplazo','refresh');
		}
	}
}
