<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ofertas extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('mod_oferta');
    }

	public function index($index = 0)
	{
		$per_page = 5;
		$query = $this->mod_oferta->ofertas_paginacion($per_page,$index);
    	$ofertas = array();
    	$oferta = array();
    	foreach($query->result() as $row){
    		$oferta['id'] = $row->id;
    		$oferta['titulo'] = $row->titulo;
    		$oferta['descripcion'] = $row->descripcion;		    
    		$ofertas[] = $oferta;
    	}
		$this->load->library('pagination');

		$config['base_url'] = site_url('ofertas/index/');
		$config['total_rows'] = $this->mod_oferta->total_ofertas();
		$config['per_page'] = $per_page; 
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['cur_tag_open'] = '<li class="current"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li class="arrow">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li class="arrow">';
		$config['prev_tag_close'] = '</li>';
		
		$this->pagination->initialize($config); 
		
		
    	$data = array('ofertas'=>$ofertas);
		$this->load->view('header');
		$this->load->view('buscar',$data);
		$this->load->view('footer');
	}
}