<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reemplazo extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->model('mod_reemplazo');
  }

  public function activar(){
    if($this->session->userdata('rol')=='user'){
      $data = array();
      $data['rut_postulante'] = $this->session->userdata('rut');
      $data['tipo_reemplazo'] = $this->input->post('tipo_reemplazo');
      $data['estado'] = 1;
      $query = $this->mod_reemplazo->obtener($this->session->userdata('rut'));
      if ($query->num_rows()<=0){
        $resultado = $this->mod_reemplazo->nuevo($data);
        if($resultado){
          echo json_encode(array("status"=>$resultado,"message"=>"Registrado correctamente"));
        }
        else{
          echo json_encode(array("status"=>$resultado,"message"=>"Ocurrió un error durante el registro"));
        }
      }
      else {
        $resultado = $this->mod_reemplazo->actualizar($data);
        if($resultado){
          echo json_encode(array("status"=>$resultado,"message"=>"Registro actualizado correctamente"));
        }
        else{
          echo json_encode(array("status"=>$resultado,"message"=>"Ocurrió un error durante el registro"));
        }
      }
    }
  }


}
