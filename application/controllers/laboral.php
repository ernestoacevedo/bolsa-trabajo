<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laboral extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->model('mod_laboral');
  }

  public function agregar(){
    if($this->session->userdata('rol')=='user'){
      $data['rut_postulante'] = $this->session->userdata('rut');
      $data['tipo_antecedente'] = 'laboral';
      $data['nombre_empresa'] = $this->input->post('nombre_empresa');
      $data['pais'] = $this->input->post('pais');
      $data['cargo'] = $this->input->post('cargo');
      $data['funciones'] = $this->input->post('funciones');
      $data['logros'] = $this->input->post('logros');
      $data['mes_inicio'] = $this->input->post('mes_inicio');
      $data['mes_termino'] = $this->input->post('mes_termino');
      $data['inicio'] = $this->input->post('inicio');
      $data['termino'] = $this->input->post('termino');

      $insert = $this->mod_laboral->nuevo($data);
      if($insert){
        echo json_encode(array('status'=>$insert,'message'=>'Antecedente agregado correctamente','redirect'=>site_url('dashboard/curriculum')));
      }
      else{
        echo json_encode(array('status'=>$insert,'message'=>'Ocurrió un error al insertar el antecedente'));
      }
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function agregar_referencia(){
    if($this->session->userdata('rol')=='user'){
      $data['rut_postulante'] = $this->session->userdata('rut');
      $data['tipo_antecedente'] = 'referencia';
      $data['nombre_ref'] = $this->input->post('nombre_ref');
      $data['cargo_ref'] = $this->input->post('cargo_ref');
      $data['relacion_ref'] = $this->input->post('relacion_ref');
      $data['institucion_ref'] = $this->input->post('institucion_ref');
      $data['telefono_ref'] = $this->input->post('telefono_ref');
      $data['email_ref'] = $this->input->post('email_ref');
      $insert = $this->mod_laboral->nuevo($data);
      if($insert){
        echo json_encode(array('status'=>$insert,'message'=>'Antecedente agregado correctamente','redirect'=>site_url('dashboard/curriculum')));
      }
      else{
        echo json_encode(array('status'=>$insert,'message'=>'Ocurrió un error al insertar el antecedente'));
      }
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function obtener(){
    if($this->session->userdata('rol')=='user'){
      $id = $this->uri->segment(3);
      $query = $this->mod_laboral->obtener($id);
      $data = array();
      foreach($query->result() as $row){
        $data['id'] = $row->id;
        $data['empresa'] = $row->nombre_empresa;
        $data['cargo'] = $row->cargo;
        $data['pais'] = $row->pais;
        $data['inicio'] = $row->inicio;
        $data['termino'] = $row->termino;
        $data['mes_inicio'] = $row->mes_inicio;
        $data['mes_termino'] = $row->mes_termino;
        $data['funciones'] = $row->funciones;
        $data['logros'] = $row->logros;
        $data['nombre_ref'] = $row->nombre_ref;
        $data['cargo_ref'] = $row->cargo_ref;
        $data['email_ref'] = $row->email_ref;
        $data['telefono_ref'] = $row->telefono_ref;
        $data['rut_postulante'] = $row->rut_postulante;
        $data['tipo_antecedente'] = $row->tipo_antecedente;
        $data['relacion_ref'] = $row->relacion_ref;
        $data['institucion_ref'] = $row->institucion_ref;
      }
      echo json_encode($data);
    }
  }

  public function editar(){
    if($this->session->userdata('rol')=='user'){
      $data['id'] = $this->input->post('id_exp');
      $data['nombre_empresa'] = $this->input->post('nombre_empresa');
      $data['pais'] = $this->input->post('pais');
      $data['cargo'] = $this->input->post('cargo');
      $data['funciones'] = $this->input->post('funciones');
      $data['logros'] = $this->input->post('logros');
      $data['mes_inicio'] = $this->input->post('mes_inicio');
      $data['mes_termino'] = $this->input->post('mes_termino');
      $data['inicio'] = $this->input->post('inicio');
      $data['termino'] = $this->input->post('termino');

      $insert = $this->mod_laboral->actualizar($data);
      if($insert){
        echo json_encode(array('status'=>$insert,'message'=>'Antecedente actualizado correctamente','redirect'=>site_url('dashboard/curriculum')));
      }
      else{
        echo json_encode(array('status'=>$insert,'message'=>'Ocurrió un error al actualizar el antecedente'));
      }
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function editar_referencia(){
    if($this->session->userdata('rol')=='user'){
      $data['id'] = $this->input->post('id_ref');
      $data['nombre_ref'] = $this->input->post('nombre_ref');
      $data['cargo_ref'] = $this->input->post('cargo_ref');
      $data['telefono_ref'] = $this->input->post('telefono_ref');
      $data['email_ref'] = $this->input->post('email_ref');
      $data['relacion_ref'] = $this->input->post('relacion_ref');
      $data['institucion_ref'] = $this->input->post('institucion_ref');
      $insert = $this->mod_laboral->actualizar($data);
      if($insert){
        echo json_encode(array('status'=>$insert,'message'=>'Antecedente agregado correctamente','redirect'=>site_url('dashboard/curriculum')));
      }
      else{
        echo json_encode(array('status'=>$insert,'message'=>'Ocurrió un error al insertar el antecedente'));
      }
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function eliminar(){
    $id = $this->uri->segment(3);
    $this->mod_laboral->eliminar($id);
    redirect('dashboard/curriculum','refresh');
  }

}
