<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oferta extends CI_Controller {

  public function __construct()
    {
        parent::__construct();
        $this->load->model('mod_oferta');
        $this->load->model('mod_postulacion');
        $this->load->library('upload');
    }

  public function index(){
    redirect('/home','refresh');
  }

  public function ver(){
    $oferta_id = '';
    if ($this->uri->segment(3) === FALSE){
        redirect('/home','refresh');
    }
    else{
        $oferta_id = $this->uri->segment(3);
    }
    $query = $this->mod_oferta->obtener($oferta_id);
    $oferta = array();
    if ($query->num_rows() > 0){
      foreach($query->result() as $row){
        $oferta['id'] = $row->id;
        $oferta['titulo'] = $row->titulo;
        $oferta['descripcion'] = $row->descripcion;
        $oferta['horas'] = $row->horas;
        $oferta['puestos'] = $row->puestos;
        $oferta['remuneracion'] = $row->remuneracion;
        $oferta['fecha_publicacion'] = date("d/m/Y", strtotime($row->fecha_publicacion));
        $oferta['fecha_cierre'] = date("d/m/Y", strtotime($row->fecha_cierre));
        $oferta['caracteristicas'] = $row->caracteristicas;
        $oferta['deseables'] = $row->deseables;
        $oferta['activa'] = $row->activa;
        $oferta['estado'] = $row->estado;
        $oferta['legal'] = $row->legal;
        $oferta['antecedentes'] = $row->antecedentes;
        $oferta['entrevista_psico'] = $row->entrevista_psico;
        $oferta['entrevista_pers'] = $row->entrevista_pers;
        $oferta['seleccion_final'] = $row->seleccion_final;
        $oferta['ruta_descripcion'] = $row->ruta_descripcion;
        $oferta['grado'] = $row->grado;
        $oferta['jornada'] = $row->jornada;
        $oferta['tipo_contrato'] = $row->tipo_contrato;
        $oferta['num_depto'] = $row->num_depto;
        $oferta['email_depto'] = $row->email_depto;
        if($this->session->userdata('rol')!='admin'){
          $actualizar = $this->mod_oferta->update_visitas($oferta_id);
        }
        $data = array('oferta'=>$oferta);
        $this->load->view('header');
        $this->load->view('oferta',$data);
        $this->load->view('footer');
      }
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function buscar(){
    $texto = $this->input->post('texto_busc');
    $query = $this->mod_oferta->buscar_oferta($texto);
    $ofertas = array();
    $oferta = array();
    if ($query->num_rows() > 0){
      foreach($query->result() as $row){
        $oferta['id'] = $row->id;
        $oferta['titulo'] = $row->titulo;
        $oferta['descripcion'] = $row->descripcion;
        $oferta['horas'] = $row->horas;
        $oferta['puestos'] = $row->puestos;
        $oferta['remuneracion'] = $row->remuneracion;
        $oferta['fecha_publicacion'] = $row->fecha_publicacion;
        $oferta['fecha_cierre'] = $row->fecha_cierre;
        $oferta['caracteristicas'] = $row->caracteristicas;
        $oferta['deseables'] = $row->deseables;
        $oferta['activa'] = $row->activa;
        $oferta['legal'] = $row->legal;
        $oferta['antecedentes'] = $row->antecedentes;
        $oferta['entrevista_psico'] = $row->entrevista_psico;
        $oferta['entrevista_pers'] = $row->entrevista_pers;
        $oferta['seleccion_final'] = $row->seleccion_final;

        $ofertas[] = $oferta;
      }
      $response = array('status'=>true,'ofertas'=>$ofertas);
      echo json_encode($response);
    }
    else{
      echo json_encode(array('status'=>false,'message'=>'No se encontraron resultados'));
    }
  }

  public function buscar_termino(){
    $texto = $this->input->post('termino');
    $query = $this->mod_oferta->buscar_oferta($texto);
    $ofertas = array();
    $oferta = array();
    if ($query->num_rows() > 0){
      foreach($query->result() as $row){
        $oferta['id'] = $row->id;
        $oferta['titulo'] = $row->titulo;
        $oferta['url'] = site_url('oferta/ver/'.$row->id);

        $ofertas[] = $oferta;
      }
      $response = array('status'=>true,'ofertas'=>$ofertas);
      echo json_encode($response);
    }
    else{
      echo json_encode(array('status'=>false,'message'=>'No se encontraron resultados'));
    }
  }


  public function editar(){
    if($this->session->userdata('rol')=='admin'){
      $data['id'] = $this->input->post('id');
      $data['titulo'] = $this->input->post('titulo');
      $data['descripcion'] = $this->input->post('descripcion');
      $data['horas'] = $this->input->post('horas');
      $data['puestos'] = $this->input->post('puestos');
      $data['remuneracion'] = $this->input->post('remuneracion');
      $data['fecha_cierre'] = $this->input->post('fecha_cierre');
      $data['caracteristicas'] = $this->input->post('caracteristicas');
      $data['deseables'] = $this->input->post('deseables');
      $data['activa'] = $this->input->post('activa');
      $data['estado'] = $this->input->post('estado');
      $data['legal'] = $this->input->post('legal');
      $data['antecedentes'] = $this->input->post('antecedentes');
      $data['entrevista_psico'] = $this->input->post('entrevista_psico');
      $data['entrevista_pers'] = $this->input->post('entrevista_pers');
      $data['seleccion_final'] = $this->input->post('seleccion_final');
      $data['grado'] = $this->input->post('grado');
      $data['jornada'] = $this->input->post('jornada');
      $data['tipo_contrato'] = $this->input->post('tipo_contrato');
      $data['email_depto'] = $this->input->post('email_depto');
      $data['num_depto'] = $this->input->post('num_depto');
      $config['upload_path'] = './doc_ofertas';
      $config['allowed_types'] = 'pdf|jpg|jpeg|png|doc|docx|xls|xlsx';
      $config['max_size'] = '16384';
      $config['encrypt_name'] = true;
      $config['remove_spaces'] = true;
      $this->upload->initialize($config);
      if (!$this->upload->do_upload('file')){
        $upload = false;
      }
      else{
        $info = array('upload_data' => $this->upload->data());
        $data['ruta_descripcion'] = $info['upload_data']['file_name'];
      }
      $registro = $this->mod_oferta->actualizar($data);
      if($registro){
        $this->session->set_flashdata(array('type'=>'success','msg'=>'Oferta actualizada correctamente'));
      }
      else{
        $this->session->set_flashdata(array('type'=>'alert','msg'=>'Ocurrió un error al actualizar'));
      }
      redirect('/admin','refresh');
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function avance(){
    $oferta_id = '';
    if ($this->uri->segment(3) === FALSE){
        redirect('/home','refresh');
    }
    else{
        $oferta_id = $this->uri->segment(3);
    }
    $query = $this->mod_oferta->obtener($oferta_id);
    $oferta = array();
    if ($query->num_rows() > 0){
      foreach($query->result() as $row){
        $oferta['id'] = $row->id;
        $oferta['titulo'] = $row->titulo;
        $oferta['horas'] = $row->horas;
        $oferta['puestos'] = $row->puestos;
        $oferta['remuneracion'] = $row->remuneracion;
        $oferta['fecha_cierre'] = date("d/m/Y", strtotime($row->fecha_cierre));
        $oferta['activa'] = $row->activa;

        $query = $this->mod_postulacion->avance($oferta_id);
        $maximo = 1;
        if ($query->num_rows() > 0){
          foreach($query->result() as $row){
            $maximo = $row->etapa;
          }
        }
        $maximo = ($maximo==null) ? 1 : $maximo;
        $query = $this->mod_postulacion->buscar_postulantes($oferta_id);
        $etapa1 = array();
        $etapa2 = array();
        $etapa3 = array();
        $etapa4 = array();
        foreach($query->result() as $row){
          switch ($row->etapa) {
            case '1':
              $etapa1[] = $row->rut_postulante;
              break;
            case '2':
              $etapa1[] = $row->rut_postulante;
              $etapa2[] = $row->rut_postulante;
              break;
            case '3':
              $etapa1[] = $row->rut_postulante;
              $etapa2[] = $row->rut_postulante;
              $etapa3[] = $row->rut_postulante;
              break;
            case '4':
              $etapa1[] = $row->rut_postulante;
              $etapa2[] = $row->rut_postulante;
              $etapa3[] = $row->rut_postulante;
              $etapa4[] = $row->rut_postulante;
              break;
          }
        }
        $data = array('oferta'=>$oferta,'avance'=>$maximo,'etapa1'=>$etapa1,'etapa2'=>$etapa2,'etapa3'=>$etapa3,'etapa4'=>$etapa4);
        $this->load->view('header');
        $this->load->view('avance',$data);
        $this->load->view('footer');
      }
    }
    else{
      redirect('/home','refresh');
    }
  }

  public function data_avance(){
    $oferta_id = '';
    if ($this->uri->segment(3) === FALSE){
        redirect('/home','refresh');
    }
    else{
        $oferta_id = $this->uri->segment(3);
    }
    $query = $this->mod_oferta->obtener($oferta_id);
    $oferta = array();
    if ($query->num_rows() > 0){
      foreach($query->result() as $row){
        $oferta['id'] = $row->id;
        $oferta['titulo'] = $row->titulo;
        $oferta['horas'] = $row->horas;
        $oferta['puestos'] = $row->puestos;
        $oferta['remuneracion'] = $row->remuneracion;
        $oferta['fecha_cierre'] = date("d/m/Y", strtotime($row->fecha_cierre));
        $oferta['activa'] = $row->activa;

        $query = $this->mod_postulacion->avance($oferta_id);
        $maximo = 1;
        if ($query->num_rows() > 0){
          foreach($query->result() as $row){
            $maximo = $row->etapa;
          }
        }
        $maximo = ($maximo==null) ? 1 : $maximo;
        $query = $this->mod_postulacion->buscar_postulantes($oferta_id);
        $etapa1 = array();
        $etapa2 = array();
        $etapa3 = array();
        $etapa4 = array();
        foreach($query->result() as $row){
          switch ($row->etapa) {
            case '1':
              $etapa1[] = $row->rut_postulante;
              break;
            case '2':
              $etapa1[] = $row->rut_postulante;
              $etapa2[] = $row->rut_postulante;
              break;
            case '3':
              $etapa1[] = $row->rut_postulante;
              $etapa2[] = $row->rut_postulante;
              $etapa3[] = $row->rut_postulante;
              break;
            case '4':
              $etapa1[] = $row->rut_postulante;
              $etapa2[] = $row->rut_postulante;
              $etapa3[] = $row->rut_postulante;
              $etapa4[] = $row->rut_postulante;
              break;
          }
        }

        $data = array('oferta'=>$oferta,'avance'=>(int)$maximo,'etapa1'=>$etapa1,'etapa2'=>$etapa2,'etapa3'=>$etapa3,'etapa4'=>$etapa4);
        echo json_encode($data);
      }
    }
    else{
      echo json_encode(array('msg'=>'No se encontraron resultados'));
    }
  }

  public function eliminar(){
    if($this->session->userdata('rol')=='admin'){
      $id = $this->uri->segment(3);
      $this->mod_oferta->eliminar($id);
      redirect('/admin/ofertas','refresh');
    }
  }

}
