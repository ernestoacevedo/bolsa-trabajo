<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

  public function __construct()
    {
        parent::__construct();
        $this->load->model(array('mod_documento','mod_oferta','mod_postulacion','mod_usuario','mod_mensajes','mod_instructivo','mod_reemplazo','mod_antecedente'));
        $this->load->library('upload');
        $this->load->library('email');
		    $this->load->library('form_validation');
        $this->load->helper('correo');
    }

  public function index(){
    $user = $this->session->userdata('user_data');
    if (!isset($user)) {
      redirect('/home','refresh');
    }
    else {
      if($this->session->userdata('rol')=='admin'){
        $data = array();
        $query = $this->mod_usuario->obtener_usuarios();
        $data['total_usuarios'] = $query->num_rows();
        $query = $this->mod_oferta->obtener_todas();
        $data['total_ofertas'] = $query->num_rows();
        $query = $this->mod_postulacion->obtener_postulaciones();
        $data['total_postulaciones'] = $query->num_rows();
        $query = $this->mod_postulacion->obtener_recientes();
        $recientes = array();
        $reciente = array();
        foreach($query->result() as $row){
          $reciente['nombres'] = $row->nombres;
          $reciente['apellidos'] = $row->apellidos;
          $reciente['rut'] = $row->rut_postulante;
          $reciente['id_oferta'] = $row->id_oferta;
          if(strlen($row->titulo)>20){
            $reciente['titulo'] = substr($row->titulo,0,20).'...';
          }
          else{
            $reciente['titulo'] = $row->titulo;
          }
          $recientes[] = $reciente;
        }
        $query = $this->mod_oferta->obtener_vistas();
        $populares = array();
        $oferta = array();
        foreach($query->result() as $row){
          $oferta['id_oferta'] = $row->id;
          $oferta['vistas'] = $row->vistas;
          if(strlen($row->titulo)>40){
            $oferta['titulo'] = substr($row->titulo,0,40).'...';
          }
          else{
            $oferta['titulo'] = $row->titulo;
          }
          $populares[] = $oferta;
        }
        $data['populares'] = $populares;
        $data['recientes'] = $recientes;
        $this->load->view('header');
        $this->load->view('admin',$data);
        $this->load->view('footer');
      }
      else{
        redirect('/home','refresh');
      }
    }
  }
  public function publicarOferta(){
    $user = $this->session->userdata('user_data');
    if (!isset($user)) {
      redirect('/home','refresh');
    }
    else {
      if($this->session->userdata('rol')=='admin'){
        $this->load->view('header');
        $this->load->view('publicar');
        $this->load->view('footer');
      }
      else{
        redirect('/home','refresh');
      }
    }
  }

  public function postulaciones(){
    $user = $this->session->userdata('user_data');
    if (!isset($user)) {
      redirect('/home','refresh');
    }
    else {
      if($this->session->userdata('rol')=='admin'){
        $query = $this->mod_postulacion->obtener_postulaciones();
        $postulaciones = array();
        $postulacion = array();
        foreach($query->result() as $row){
          $postulacion['id'] = $row->id;
          $postulacion['rut_postulante'] = $row->rut_postulante;
          $postulacion['titulo'] = $row->titulo;
          $postulacion['id_oferta'] = $row->id_oferta;
          $postulacion['nombres'] = $row->nombres;
          $postulacion['apellidos'] = $row->apellidos;
          $postulacion['fecha'] = date("d/m/Y", strtotime($row->fecha));
          $postulacion['estado'] = $row->estado;
          $postulacion['etapa'] = $row->etapa;
          $postulacion['reemplazo'] = $row->reemplazo;
          $postulaciones[] = $postulacion;
        }
        $data = array('postulaciones'=>$postulaciones);
        $this->load->view('header');
        $this->load->view('postulaciones',$data);
        $this->load->view('footer');
      }
      else{
        redirect('/home','refresh');
      }
    }

  }

  public function ofertas(){
    $user = $this->session->userdata('user_data');
    if (!isset($user)) {
      redirect('/home','refresh');
    }
    else {
      if($this->session->userdata('rol')=='admin'){
        $query = $this->mod_oferta->obtener_todas();
        $ofertas = array();
        $oferta = array();
        foreach($query->result() as $row){
          $oferta['id'] = $row->id;
          if(strlen($row->titulo)>30){
    			$oferta['titulo'] = substr($row->titulo,0,30).'...';
    		}
    		else{
    			$oferta['titulo'] = $row->titulo;
    		}
          $oferta['activa'] = $row->activa;
          $oferta['estado'] = $row->estado;
          $oferta['fecha_publicacion'] = date("d/m/Y", strtotime($row->fecha_publicacion));
          $oferta['fecha_cierre'] = date("d/m/Y", strtotime($row->fecha_cierre));

          $ofertas[] = $oferta;
        }
        $data = array('ofertas'=>$ofertas);
        $this->load->view('header');
        $this->load->view('ofertas',$data);
        $this->load->view('footer');
      }
      else{
        redirect('/home','refresh');
      }
    }
  }

  public function usuarios(){
    $user = $this->session->userdata('user_data');
    if (!isset($user)) {
      redirect('/home','refresh');
    }
    else {
      if($this->session->userdata('rol')=='admin'){
        $query = $this->mod_usuario->obtener_usuarios();
        $usuarios = array();
        $usuario = array();
        foreach($query->result() as $row){
          $usuario['rut'] = $row->rut;
          $usuario['nombres'] = $row->nombres;
          $usuario['apellidos'] = $row->apellidos;
          //$usuario['ciudad'] = $row->ciudad;
          $usuario['ciudad'] = ($row->ciudad=='') ? '&mdash;' : $row->ciudad;
          $usuario['email'] = $row->email;
          //$usuario['celular'] = $row->celular;
          $usuario['celular'] = ($row->celular=='') ? '&mdash;' : $row->celular;
          $usuario['reemplazo'] = $row->reemplazo;
          $nivel = ($row->nivel_estudios=='') ? '&mdash;' : obtenerNivel(($row->nivel_estudios)-1);
          $usuario['nivel'] = $nivel;
          $usuarios[] = $usuario;
        }
        $data = array('usuarios'=>$usuarios);
        $this->load->view('header');
        $this->load->view('usuarios',$data);
        $this->load->view('footer');
      }
      else{
        redirect('/home','refresh');
      }
    }
  }

  public function lista_reemplazos(){
    $user = $this->session->userdata('user_data');
    if (!isset($user)) {
      redirect('/home','refresh');
    }
    else {
      if($this->session->userdata('rol')=='admin'){
        $query = $this->mod_usuario->obtener_reemplazos();
        $usuarios = array();
        $usuario = array();
        foreach($query->result() as $row){
          $usuario['rut'] = $row->rut;
          $usuario['nombres'] = $row->nombres;
          $usuario['apellidos'] = $row->apellidos;
          $usuario['tipo_reemplazo'] = (int)$row->tipo_reemplazo;
          $usuarios[] = $usuario;
        }
        $data = array('usuarios'=>$usuarios);
        $this->load->view('header');
        $this->load->view('lista_reemplazos',$data);
        $this->load->view('footer');
      }
      else{
        redirect('/home','refresh');
      }
    }
  }

  public function publicar(){
    $this->form_validation->set_rules('titulo', 'Título', 'required');
    $this->form_validation->set_rules('descripcion', 'Descripción', 'required');
    $this->form_validation->set_rules('fecha_cierre', 'Fecha cierre', 'required');
    if ($this->form_validation->run() == FALSE){
      $this->session->set_flashdata(array('type'=>'alert','msg'=>'Ocurrió un problema al publicar la oferta'));
      redirect('/admin','refresh');
    }
    else{
      $upload = true;
      $data['titulo'] = $this->input->post('titulo');
      $data['descripcion'] = $this->input->post('descripcion');
      $data['horas'] = $this->input->post('horas');
      $data['puestos'] = $this->input->post('puestos');
      $data['remuneracion'] = $this->input->post('remuneracion');
      $data['fecha_publicacion'] = $this->input->post('fecha_publicacion');
      $data['fecha_cierre'] = $this->input->post('fecha_cierre');
      $data['caracteristicas'] = $this->input->post('caracteristicas');
      $data['deseables'] = $this->input->post('deseables');
      $data['activa'] = $this->input->post('activa');
      $data['estado'] = $this->input->post('estado');
      $data['legal'] = $this->input->post('legal');
      $data['antecedentes'] = $this->input->post('antecedentes');
      $data['entrevista_psico'] = $this->input->post('entrevista_psico');
      $data['entrevista_pers'] = $this->input->post('entrevista_pers');
      $data['seleccion_final'] = $this->input->post('seleccion_final');
      $data['vistas'] = '0';
      $data['ruta_descripcion'] = '';
      $data['email_depto'] = $this->input->post('email_depto');
      $data['num_depto'] = $this->input->post('num_depto');
      $data['grado'] = $this->input->post('grado');
      $data['jornada'] = $this->input->post('jornada');
      $data['tipo_contrato'] = $this->input->post('tipo_contrato');
      $config['upload_path'] = './doc_ofertas';
      $config['allowed_types'] = 'pdf|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx';
      $config['max_size'] = '20000';
      $config['encrypt_name'] = true;
      $config['remove_spaces'] = true;
      $this->upload->initialize($config);
      if (!$this->upload->do_upload('file')){
        $upload = false;
      }
      else{
        $info = array('upload_data' => $this->upload->data());
        $data['ruta_descripcion'] = $info['upload_data']['file_name'];
      }
      $registro = $this->mod_oferta->nuevo($data);
      if($registro){
        $this->session->set_flashdata(array('type'=>'success','msg'=>'Oferta publicada exitosamente'));
      }
      else{
        $this->session->set_flashdata(array('type'=>'alert','msg'=>'Ocurrió un problema al publicar la oferta'));
      }
      redirect('/admin','refresh');
    }
  }

  public function editar(){
    $user = $this->session->userdata('user_data');
    if (!isset($user)) {
      redirect('/home','refresh');
    }
    else {
      if($this->session->userdata('rol')=='admin'){
        if ($this->uri->segment(3) === FALSE){
            redirect('/admin','refresh');
        }
        else{
            $oferta_id = $this->uri->segment(3);
        }
        $query = $this->mod_oferta->obtener($oferta_id);
        $oferta = array();
        if ($query->num_rows() > 0){
          foreach($query->result() as $row){
            $oferta['id'] = $row->id;
            $oferta['titulo'] = $row->titulo;
            $oferta['descripcion'] = $row->descripcion;
            $oferta['horas'] = $row->horas;
            $oferta['puestos'] = $row->puestos;
            $oferta['remuneracion'] = $row->remuneracion;
            $oferta['fecha_cierre'] = date("d/m/Y", strtotime($row->fecha_cierre));
            $oferta['caracteristicas'] = $row->caracteristicas;
            $oferta['deseables'] = $row->deseables;
            $oferta['activa'] = $row->activa;
            $oferta['estado'] = $row->estado;
            $oferta['legal'] = $row->legal;
            $oferta['antecedentes'] = $row->antecedentes;
            $oferta['entrevista_psico'] = $row->entrevista_psico;
            $oferta['entrevista_pers'] = $row->entrevista_pers;
            $oferta['seleccion_final'] = $row->seleccion_final;
            $oferta['ruta_descripcion'] = $row->ruta_descripcion;
            $oferta['grado'] = $row->grado;
            $oferta['jornada'] = $row->jornada;
            $oferta['tipo_contrato'] = $row->tipo_contrato;
            $oferta['email_depto'] = $row->email_depto;
            $oferta['num_depto'] = $row->num_depto;
          }
        }
        else{
          redirect('/admin','refresh');
        }
        $data = array('oferta'=>$oferta);
        $this->load->view('header');
        $this->load->view('editar',$data);
        $this->load->view('footer');
      }
      else{
        redirect('/home','refresh');
      }
    }
  }

  public function enviar(){
    $user = $this->session->userdata('user_data');
    if (!isset($user)) {
      redirect('/home','refresh');
    }
    else {
      if($this->session->userdata('rol')=='admin'){
        $this->load->view('header');
        $this->load->view('nuevo_correo');
        $this->load->view('footer');
      }
      else{
        redirect('/home','refresh');
      }
    }
  }

  public function enviar_correo(){
    $query = $this->mod_usuario->obtener_notificables();
    $usuarios = array();
    foreach($query->result() as $row){
      $usuarios[] = $row->email;
    }
    $config = obtener_config();
    $this->email->initialize($config);
    $data = array('mensaje'=>$this->input->post('contenido'));
    $html_email = $this->load->view('mail', $data, true);
    $this->email->from(obtener_correo());
    $this->email->cc(obtener_correo());
    $this->email->bcc($usuarios);
    $this->email->subject($this->input->post('asunto'));
    $this->email->message($html_email);
    $status = $this->email->send();
    if($status=='1'){
      $this->session->set_flashdata(array('type'=>'success','msg'=>'Correo enviado satisfactoriamente'));
      redirect('/admin','refresh');
    }
    else{
      $this->session->set_flashdata(array('type'=>'alert','msg'=>'Se produjo un error al enviar el correo'));
      redirect('/admin','refresh');
    }
  }

  public function mensajes(){
    $user = $this->session->userdata('user_data');
    if (!isset($user)) {
      redirect('/home','refresh');
    }
    else {
      if($this->session->userdata('rol')=='admin'){
        $query = $this->mod_mensajes->obtener();
        $config = array();
        if ($query->num_rows() > 0){
          foreach($query->result() as $row){
            $config['id'] = $row->id;
            $config['msg_registro'] = $row->msg_registro;
            $config['msg_inicio'] = $row->msg_inicio;
            $config['msg_avance'] = $row->msg_avance;
            $config['msg_termino'] = $row->msg_termino;
            $config['msg_seleccion'] = $row->msg_seleccion;
          }
        }
        else{
          $config['id'] = "";
          $config['msg_registro'] = "";
          $config['msg_inicio'] = "";
          $config['msg_avance'] = "";
          $config['msg_termino'] = "";
          $config['msg_seleccion'] = "";
        }
        $data = array('config'=>$config);
        $this->load->view('header');
        $this->load->view('panel_mensajes',$data);
        $this->load->view('footer');
      }
      else{
        redirect('/home','refresh');
      }
    }
  }

  public function actualizar_config(){
    $operacion = false;
    $data['msg_inicio'] = $this->input->post('inicio');
    $data['msg_registro'] = $this->input->post('registro');
    $data['msg_avance'] = $this->input->post('avance');
    $data['msg_termino'] = $this->input->post('termino');
    $data['msg_seleccion'] = $this->input->post('seleccion');
    if($this->input->post('id')==""){
      $operacion = $this->mod_mensajes->nuevo($data);
    }
    else{
      $data['id'] = $this->input->post('id');
      $operacion = $this->mod_mensajes->actualizar($data);
    }
    if($operacion){
      $this->session->set_flashdata(array('type'=>'success','msg'=>'Configuración actualizada correctamente'));
    }
    else{
      $this->session->set_flashdata(array('type'=>'alert','msg'=>'Ocurrió un problema al guardar la configuración'));
    }
    redirect('/admin','refresh');
  }

  public function instructivos(){
    $user = $this->session->userdata('user_data');
    if (!isset($user)) {
      redirect('/home','refresh');
    }
    else {
      if($this->session->userdata('rol')=='admin'){
        $query = $this->mod_instructivo->obtener_instructivos();
        $instructivos = array();
        $instructivo = array();
        foreach($query->result() as $row){
          $instructivo['id'] = $row->id;
          $instructivo['titulo_oferta'] = $row->titulo;
          $instructivo['id_oferta'] = $row->id_oferta;
          $instructivo['ruta_documento'] = $row->ruta_documento;
          $instructivos[] = $instructivo;
        }
        $query = $this->mod_oferta->obtener_todas();
        $ofertas = array();
        $oferta = array();
        foreach($query->result() as $row){
          $oferta['id'] = $row->id;
          $oferta['titulo'] = $row->titulo;
          $oferta['activa'] = $row->activa;
          $ofertas[] = $oferta;
        }
        $data = array('instructivos'=>$instructivos,'ofertas'=>$ofertas);
        $this->load->view('header');
        $this->load->view('instructivos',$data);
        $this->load->view('footer');
      }
      else{
        redirect('/home','refresh');
      }
    }
  }

  public function ver_documentos(){
    $user = $this->session->userdata('user_data');
    if (!isset($user)) {
      redirect('/home','refresh');
    }
    else {
      if($this->session->userdata('rol')=='admin'){
        $query = $this->mod_documento->obtener_documentos_postulantes();
        $documentos = array();
        $documento = array();
        foreach($query->result() as $row){
          $documento['id'] = $row->id;
          $documento['rut_postulante'] = $row->rut_postulante;
          $documento['nombres'] = $row->nombres;
          $documento['apellidos'] = $row->apellidos;
          $documento['tipo_documento'] = $row->tipo_documento;
          $documento['ruta_documento'] = $row->ruta_documento;
          $documentos[] = $documento;
        }
        $data = array('documentos'=>$documentos);
        $this->load->view('header');
        $this->load->view('ver_documentos',$data);
        $this->load->view('footer');
      }
      else{
        redirect('/home','refresh');
      }
    }
  }

  public function exportar_datos(){
    $user = $this->session->userdata('user_data');
    if (!isset($user)) {
      redirect('/home','refresh');
    }
    else {
      if($this->session->userdata('rol')=='admin'){
        $query = $this->mod_usuario->obtener_usuarios();
        $usuarios = array();
        $usuario = array();
        foreach($query->result() as $row){
          $usuario['rut'] = $row->rut;
          $usuario['nombres'] = $row->nombres;
          $usuario['apellidos'] = $row->apellidos;
          $usuarios[] = $usuario;
        }
        $data = array('usuarios'=>$usuarios);
        $this->load->view('header');
        $this->load->view('exportar',$data);
        $this->load->view('footer');
      }
      else{
        redirect('/home','refresh');
      }
    }
  }

  public function exportar(){
    $rut = $this->input->post('rut_postulante');
    $data['titulo'] = '';
    if($rut!=''){
      $query = $this->mod_usuario->obtener($rut);
      $data['titulo'] = "Datos postulante"."[".$rut."]";
    }
    else{
      $query = $this->mod_usuario->obtener_usuarios();
      $data['titulo'] = "Listado de postulantes";
    }
    $i=0;
    $content = '';
    foreach($query->result() as $row){
      $nivel = ($row->nivel_estudios=='') ? 'No se han ingresado datos' : obtenerNivel(($row->nivel_estudios)-1);
      $ultima_postulacion = '';
      $query_post = $this->mod_postulacion->obtener_ultima_postulacion($row->rut);
      if ($query_post->num_rows()>0){
        foreach ($query_post->result() as $rowpost) {
          $ultima_postulacion = $rowpost->titulo;
        }
      }
      $titulo = '';
      $reemp = ($row->reemplazo =='1') ? 'Si' : 'No';
      $act = ($row->actualizacion=='') ? '' : date('d/m/Y',strtotime($row->actualizacion));
      $content .= "<tr><td>".$row->rut."</td><td>".utf8_encode($row->apellidos).' '.utf8_encode($row->nombres)."</td><td>".utf8_encode($row->ciudad)."</td><td>".$nivel."</td><td>".utf8_encode($row->titulo)."</td><td>".utf8_encode($ultima_postulacion)."</td><td>".$row->email."</td><td>".$row->celular."</td><td>".$reemp."</td><td>".$act."</td></tr>";
    }

    $data['header'] = $header;
    $data['contenido'] = $content;
    $this->load->view('view_excel',$data);
  }
}
