<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_reemplazo extends CI_Model{

  public function __construct()
  {
    parent::__construct();
  }

  function nuevo($data){
    return $this->db->insert('reemplazos',$data);
  }

  function obtener($rut){
    $this->db->where('rut_postulante',$rut);
    return $this->db->get('reemplazos');
  }

  function actualizar($data){
      $this->db->where('rut_postulante',$data['rut_postulante']);
      return $this->db->update('reemplazos',$data);
  }

  function obtener_todos(){
      return $this->db->get('reemplazos');
  }

  function obtener_listado(){
    return $this->db->query("SELECT r.id,r.tipo_reemplazo,r.estado,u.nombres,u.apellidos,u.rut
        FROM reemplazos AS r
        LEFT JOIN usuarios AS u ON r.rut_postulante = u.rut");
  }

}
