<?php if ( ! defined('BASEPATH')) exit('No se permite el acceso directo al script');

    class mod_oferta extends CI_Model{

      public function __construct(){
            parent::__construct();
      }

        function nuevo($data){
            return $this->db->insert('ofertas',$data);
        }

        function actualizar($data){
            $this->db->where('id',$data['id']);       
            return $this->db->update('ofertas',$data);
        }

        function obtener($id){
            $this->db->where('id',$id);
            return $this->db->get('ofertas');
        }

        function buscar_oferta($text){
            $this->db->like('titulo', $text);
            $this->db->or_like('descripcion', $text);
            $this->db->where('activa',"1");
            $this->db->where('estado','1');
            return $this->db->get('ofertas');
        }

        function obtener_ofertas(){
            $this->db->select('*');
            $this->db->from('ofertas');
            $this->db->where('activa',"1");
            $this->db->where('estado','1');
            return $this->db->get();
        }
        
        function total_ofertas(){
            $this->db->select('*');
            $this->db->from('ofertas');
            $this->db->where('activa',"1");
            $this->db->where('estado','1');
            return $this->db->get()->num_rows();
        }
        
        function ofertas_paginacion($limit,$row){
	        $this->db->where('activa',"1");
            $this->db->where('estado','1');
	        return $this->db->get('ofertas',$limit,$row);
        }

        function obtener_ofertas_home(){
            $this->db->select('*');
            $this->db->from('ofertas');
            $this->db->where('activa',"1");
            $this->db->where('estado','1');
            $this->db->order_by('fecha_publicacion', 'desc'); 
            $this->db->limit(6);
            return $this->db->get();
        }

        function obtener_todas(){
            $this->db->select('*');
            $this->db->from('ofertas');
            return $this->db->get();
        }

        function update_visitas($id){
            return $this->db->query("UPDATE ofertas 
                      SET vistas = vistas + 1 
                      WHERE id =".$id.""); 
        }

        function obtener_vistas(){
            $this->db->select('*');
            $this->db->from('ofertas');
            $this->db->where('activa',"1");
            $this->db->where('estado','1');
            $this->db->order_by('vistas', 'desc'); 
            $this->db->limit(5);
            return $this->db->get();
        }

        function ultimas_ofertas(){
            $this->db->select('*');
            $this->db->from('ofertas');
            $this->db->where('activa',"1");
            $this->db->where('estado','1');
            $this->db->order_by('fecha_publicacion', 'desc'); 
            $this->db->limit(5);
            return $this->db->get();
        }

        function eliminar($id){
            return $this->db->delete('ofertas', array('id' => $id)); 
        }


    }