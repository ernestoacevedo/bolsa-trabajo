<?php if ( ! defined('BASEPATH')) exit('No se permite el acceso directo al script');

    class mod_postulacion extends CI_Model{

      public function __construct(){
            parent::__construct();
      }

        function nuevo($data){
            return $this->db->insert('postulaciones',$data);
        }

        function verificar_postulacion($rut,$id){
            $this->db->select('*');
            $this->db->from('postulaciones');
            $this->db->where('rut_postulante',$rut);
            $this->db->where('id_oferta',$id);
            return $this->db->get();
        }

        function actualizar($data){
            $this->db->where('id',$data['id']);
            return $this->db->update('postulaciones',$data);
        }

        function obtener($id){
            return $this->db->query("SELECT p.id,p.id_oferta,p.rut_postulante,u.nombres,u.apellidos,u.email,p.fecha,p.estado,p.etapa
                FROM postulaciones AS p
                LEFT JOIN usuarios AS u ON p.rut_postulante = u.rut
                WHERE p.id=".$id."");
        }

        function buscar_postulantes($id_oferta){
            $this->db->where('id_oferta',$id_oferta);
            return $this->db->get('postulaciones');
        }

        function avance($id_oferta){
            $this->db->select_max('etapa');
            $this->db->where('id_oferta',$id_oferta);
            return $this->db->get('postulaciones');
        }

        function obtener_postulaciones(){
            return $this->db->query("SELECT p.id,p.id_oferta,of.titulo,p.rut_postulante,u.reemplazo,u.nombres,u.apellidos,p.fecha,p.estado,p.etapa
                FROM postulaciones AS p
                LEFT JOIN usuarios AS u ON p.rut_postulante = u.rut
                LEFT JOIN ofertas AS of ON p.id_oferta = of.id");
        }

        function obtener_recientes(){
            return $this->db->query("SELECT p.id,p.id_oferta,of.titulo,p.rut_postulante,u.nombres,u.apellidos,p.fecha
                FROM postulaciones AS p
                LEFT JOIN usuarios AS u ON p.rut_postulante = u.rut
                LEFT JOIN ofertas AS of ON p.id_oferta = of.id
                ORDER BY p.fecha DESC
                LIMIT 5");
        }

        function obtener_ultima_postulacion($rut){
          $consulta = "SELECT of.titulo,p.fecha,p.estado,p.etapa
              FROM postulaciones AS p
              LEFT JOIN usuarios AS u ON p.rut_postulante = u.rut
              LEFT JOIN ofertas AS of ON p.id_oferta = of.id
              WHERE p.rut_postulante='".$rut."' ORDER BY p.fecha DESC LIMIT 1";
          return $this->db->query($consulta);
        }

        function historial($rut){
            $consulta = "SELECT p.id,p.id_oferta,of.titulo,p.rut_postulante,u.nombres,u.apellidos,p.fecha,p.estado,p.etapa
                FROM postulaciones AS p
                LEFT JOIN usuarios AS u ON p.rut_postulante = u.rut
                LEFT JOIN ofertas AS of ON p.id_oferta = of.id
                WHERE p.rut_postulante='".$rut."'";
            return $this->db->query($consulta);
        }

        function obtener_ultimos_seleccionados(){
            $this->db->select('*');
            $this->db->from('postulaciones');
            $this->db->where('etapa','4');
            $this->db->order_by('actualizacion', 'desc');
            $this->db->limit(5);
            return $this->db->get();

        }

        function obtener_postulaciones_oferta($id_oferta){
          // $this->db->select('*');
          // $this->db->from('postulaciones');
          // $this->db->where('id_oferta',$id_oferta);
          // return $this->db->get();
          $consulta = "SELECT p.id,p.id_oferta,of.titulo,p.rut_postulante,u.nombres,u.apellidos,p.fecha,p.estado,p.etapa
              FROM postulaciones AS p
              LEFT JOIN usuarios AS u ON p.rut_postulante = u.rut
              LEFT JOIN ofertas AS of ON p.id_oferta = of.id
              WHERE p.id_oferta='".$id_oferta."'";
          return $this->db->query($consulta);

        }

        function eliminar($id){
            return $this->db->delete('postulaciones', array('id' => $id));
        }

    }
