<?php if ( ! defined('BASEPATH')) exit('No se permite el acceso directo al script');

    class mod_instructivo extends CI_Model{

      public function __construct(){
            parent::__construct();
      }

        function nuevo($data){
            return $this->db->insert('instructivos',$data);
        }


        function obtener($id_oferta){
            $this->db->where('id_oferta',$id_oferta);
            return $this->db->get('instructivos');
        }

        function obtener_instructivos(){
            // $this->db->select('*');
            // $this->db->from('instructivos');
            // return $this->db->get();
            return $this->db->query("SELECT ins.id_oferta, ins.id, ins.ruta_documento, of.titulo 
                FROM instructivos AS ins
                LEFT JOIN ofertas AS of ON ins.id_oferta = of.id"); 
        }

        function eliminar($id){
            return $this->db->delete('instructivos', array('id' => $id)); 
        }


    }