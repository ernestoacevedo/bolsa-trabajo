<?php if ( ! defined('BASEPATH')) exit('No se permite el acceso directo al script');

    class mod_usuario extends CI_Model{

      public function __construct(){
            parent::__construct();
      }

      function nuevo($data){
        return $this->db->insert('usuarios',$data);
      }

      function loguear($rut,$password){
            $this->db->where('rut',$rut);
            $query = $this->db->get('usuarios');
            $row = $query->row();
            if($row!=null){
                if($row->password==sha1($password)){
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }

        function actualizar($data){
            $this->db->where('rut',$data['rut']);
            return $this->db->update('usuarios',$data);
        }

        function cambiar_password($rut,$newpass){
            $data = array('password'=>sha1($newpass),'rut'=>$rut);
            $this->db->where('rut',$rut);
            return $this->db->update('usuarios',$data);
        }

        function obtener($rut){
            $this->db->where('rut',$rut);
            return $this->db->get('usuarios');
        }

        function obtener_usuarios(){
            $this->db->select('*');
            $this->db->from('usuarios');
            $this->db->where('nombres !=','Administrador');
            $this->db->order_by('apellidos','asc');
            return $this->db->get();
        }

        function obtener_reemplazos(){
            $this->db->select('*');
            $this->db->from('usuarios');
            $this->db->where('nombres !=','Administrador');
            $this->db->where('reemplazo','1');
            $this->db->order_by('apellidos','asc');
            return $this->db->get();
        }

        function obtener_notificables(){
            $this->db->select('*');
            $this->db->from('usuarios');
            $this->db->where('recibir_info','1');
            $this->db->where('nombres !=','Administrador');
            $this->db->order_by('apellidos','asc');
            return $this->db->get();
        }

        function eliminar($id){
            $this->db->delete('documentos',array('rut_postulante'=>$id));
            $this->db->delete('postulaciones',array('rut_postulante'=>$id));
            $this->db->delete('antecedentes_academicos', array('rut_postulante' => $id));
            $this->db->delete('antecedentes_laborales', array('rut_postulante' => $id));
            return $this->db->delete('usuarios', array('rut' => $id));
        }


    }
