<?php if ( ! defined('BASEPATH')) exit('No se permite el acceso directo al script');

    class mod_antecedente extends CI_Model{

      public function __construct(){
            parent::__construct();
      }

        function nuevo($data){
            return $this->db->insert('antecedentes_academicos',$data);
        }

        function actualizar($data){
            $this->db->where('id',$data['id']);
            return $this->db->update('antecedentes_academicos',$data);
        }

        function obtener($id){
            $this->db->where('id',$id);
            return $this->db->get('antecedentes_academicos');
        }

        function obtener_antecedentes($rut){
            $this->db->select('*');
            $this->db->from('antecedentes_academicos');
            $this->db->where('rut_postulante',$rut);
            return $this->db->get();
        }

        function obtener_nivel_educacion($rut){
          $this->db->select('*');
          $this->db->from('antecedentes_academicos');
          $query = "rut_postulante = '".$rut."' AND tipo_antecedente='media' OR tipo_antecedente='superior'";
          $this->db->where($query);
          return $this->db->get();
        }

        function obtener_tipo($rut,$tipo){
            $this->db->select('*');
            $this->db->from('antecedentes_academicos');
            $this->db->where('rut_postulante',$rut);
            $this->db->where('tipo_antecedente',$tipo);
            return $this->db->get();
        }

        function eliminar($id){
            return $this->db->delete('antecedentes_academicos', array('id' => $id));
        }

    }
