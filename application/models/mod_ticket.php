<?php if ( ! defined('BASEPATH')) exit('No se permite el acceso directo al script');

    class mod_ticket extends CI_Model{

      public function __construct(){
            parent::__construct();
      }

        function nuevo($data){
            return $this->db->insert('tickets',$data);
        }

        function obtener($rut_postulante){
            $this->db->where('rut_postulante',$rut_postulante);
            return $this->db->get('tickets');
        }

        function obtener_tickets(){
            $this->db->select('*');
            $this->db->from('tickets');
            return $this->db->get();
        }

        function buscar_ticket($codigo){
            $this->db->where('codigo',$codigo);
            return $this->db->get('tickets');
        }

    }