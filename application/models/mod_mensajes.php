<?php if ( ! defined('BASEPATH')) exit('No se permite el acceso directo al script');

    class mod_mensajes extends CI_Model{

      public function __construct(){
            parent::__construct();
      }

        function nuevo($data){
            return $this->db->insert('mensajes',$data);
        }

        function actualizar($data){
            $this->db->where('id',$data['id']);       
            return $this->db->update('mensajes',$data);
        }


        function obtener(){
            return $this->db->get('mensajes');
        }

        function eliminar($id){
            return $this->db->delete('mensajes', array('id' => $id)); 
        }


    }