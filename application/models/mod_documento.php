<?php if ( ! defined('BASEPATH')) exit('No se permite el acceso directo al script');

    class mod_documento extends CI_Model{

      public function __construct(){
            parent::__construct();
      }

        function nuevo($data){
            return $this->db->insert('documentos',$data);
        }


        function obtener($rut_postulante){
            $this->db->where('rut_postulante',$rut_postulante);
            return $this->db->get('documentos');
        }

        function obtener_documentos(){
            $this->db->select('*');
            $this->db->from('documentos');
            return $this->db->get();
        }

        function obtener_documentos_postulantes(){
            return $this->db->query("SELECT d.id,d.rut_postulante,u.nombres,u.apellidos,d.tipo_documento,d.ruta_documento
                FROM documentos AS d 
                LEFT JOIN usuarios AS u ON d.rut_postulante = u.rut"); 
        }

        function eliminar($id){
            return $this->db->delete('documentos', array('id' => $id)); 
        }


    }