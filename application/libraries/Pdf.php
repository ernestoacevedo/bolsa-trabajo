<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    // Incluimos el archivo fpdf
    require_once APPPATH."/third_party/fpdf/fpdf.php";

    //Extendemos la clase Pdf de la clase fpdf para que herede todas sus variables y funciones
    class Pdf extends FPDF {
        public function __construct() {
            parent::__construct();
            $this->postulante = '';
            $this->rut = '';
        }

        public function SetName($name,$rut){
          $this->postulante = $name;
          $this->rut = $rut;
        }

        public function Header(){
            $this->Image(base_url().'assets/img/header_sm.jpg' , 10 ,8, 190 , 25,'jpg');
            $this->Ln(25);
       }

       public function Footer(){
            // Go to 1.5 cm from bottom
            $this->SetY(-15);
            // Select Arial italic 8
            $this->SetFont('Arial','I',8);
            // Print centered page number
            if($this->postulante==''){
              $this->Cell(0,10,'',0,0,'C');
            }
            else{
              $this->Cell(0,10,utf8_decode('Postulante: '.$this->postulante.". RUT: ".$this->rut),0,0,'C');
            }

       }
    }
?>;
