<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('obtenerEtapa')){
	function obtenerEtapa($index){
		switch($index){
      case 0:
        return 'Recepción de Currículum';
        break;
      case 1:
        return 'Entrevista Psicológica';
        break;
      case 2:
        return 'Entrevista Personal';
        break;
      case 3:
        return 'Seleccionado';
        break;
    }
	}
}

if ( ! function_exists('obtenerEtapas')){
	function obtenerEtapas($index){
		switch($index){
      case 0:
        return 'Recepción de Currículum';
        break;
      case 1:
        return 'Entrevista Psicológica';
        break;
      case 2:
        return 'Entrevista Personal';
        break;
      case 3:
        return 'Selección';
        break;
    }
	}
}

if ( ! function_exists('obtenerEstado')){
	function obtenerEstado($index){
		switch($index){
      case 0:
        return 'Inactiva';
        break;
      case 1:
        return 'Activa';
        break;
      case 2:
        return 'Terminada';
        break;
    }
	}
}
if ( ! function_exists('obtenerNivel')){
	function obtenerNivel($index){
		switch($index){
      case 0:
        return 'Medio';
        break;
      case 1:
        return 'Técnico Nivel Medio';
        break;
      case 2:
        return 'Técnico Nivel Superior';
        break;
      case 3:
        return 'Profesional';
        break;
			default:
				return '';
				break;
    }
	}
}
if ( ! function_exists('obtenerTipoReemplazo')){
	function obtenerTipoReemplazo($index){
		switch($index){
      case 0:
        return 'Administrativos';
        break;
      case 1:
        return 'Auxiliares';
        break;
      case 2:
        return 'Técnicos en enfermería';
        break;
      case 3:
        return 'Enfermeros';
        break;
      case 4:
        return 'Kinesiólogos';
        break;
      case 5:
        return 'Otros profesionales área clínica';
        break;
      case 6:
        return 'Profesionales del área administrativa o de operaciones';
        break;

    }
	}
}
