<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('print_menu')){
	function print_menu($nombre){  // MENÚ DE ADMIN
		$salida = '<h4>Bienvenido, '.$nombre.'</h4>
	                <ul class="accordion" data-accordion>
                      <li class="menulink"><a href="'.site_url('admin').'"><i class="fa fa-wrench fa-fw"></i> Panel de Administración</a></li>
	                  <li class="accordion-navigation">
	                    <a href="#panel1a"><i class="fa fa-suitcase fa-fw"></i> Ofertas de Trabajo</a>
	                    <div id="panel1a" class="content">
	                      <ul>
	                        <li><a href="'.site_url('admin/publicarOferta').'"><i class="fa fa-pencil-square-o fa-fw"></i> Publicar Oferta</a></li>
	                        <li><a href="'.site_url('admin/ofertas').'"><i class="fa fa-search fa-fw"></i> Ver Ofertas</a></li>
	                        <li><a href="'.site_url('admin/postulaciones').'"><i class="fa fa-check-circle fa-fw"></i> Ver Postulaciones</a></li>
	                      </ul>
	                    </div>
	                  </li>
	                  <li class="accordion-navigation">
	                    <a href="#panel2a"><i class="fa fa-users fa-fw"></i> Usuarios </a>
	                    <div id="panel2a" class="content">
	                      <ul>
	                        <li><a href="'.site_url('admin/usuarios').'"><i class="fa fa-list-alt fa-fw"></i> Usuarios Registrados</a></li>
                            <li><a href="'.site_url('admin/exportar_datos').'"><i class="fa fa-download fa-fw"></i> Exportar Datos</a></li>
	                      </ul>
	                    </div>
	                  </li>
                      <li class="accordion-navigation">
                        <a href="#panel2b"><i class="fa fa-user-plus fa-fw"></i> Reemplazos </a>
                        <div id="panel2b" class="content">
                          <ul>
                            <li><a href="'.site_url('admin/lista_reemplazos').'"><i class="fa fa-list-ul fa-fw"></i> Ver Listado </a></li>
                          </ul>
                        </div>
                      </li>
                      <li class="accordion-navigation">
                        <a href="#panel3a"><i class="fa fa-archive fa-fw"></i> Documentos </a>
                        <div id="panel3a" class="content">
                          <ul>
                            <li><a href="'.site_url('admin/instructivos').'"><i class="fa fa-files-o fa-fw"></i> Ver Instructivos</a></li>
                            <li><a href="'.site_url('admin/ver_documentos').'"><i class="fa fa-eye fa-fw"></i> Ver Documentos</a></li>
                          </ul>
                        </div>
                      </li>
                      <li class="accordion-navigation">
                        <a href="#panel3b"><i class="fa fa-envelope-o fa-fw"></i> Correo Electrónico </a>
                        <div id="panel3b" class="content">
                          <ul>
                            <li><a href="'.site_url('admin/mensajes').'"><i class="fa fa-comment fa-fw"></i> Mensajes Predeterminados</a></li>
                            <li><a href="'.site_url('admin/enviar').'"><i class="fa fa-envelope-square fa-fw"></i> Enviar Correo Masivo</a></li>
                          </ul>
                        </div>
                      </li>
	                  <li class="accordion-navigation">
	                    <a href="#panel4a"><i class="fa fa-user fa-fw"></i> Cuenta</a>
	                    <div id="panel4a" class="content">
	                      <ul>
	                        <li><a href="'.site_url('login/cambiar_password').'"><i class="fa fa-unlock-alt fa-fw"></i> Cambiar Contraseña</a></li>
	                        <li><a href="'.site_url('login/log_out').'"><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesión</a></li>
	                      </ul>


	                    </div>
	                  </li>
	                </ul>';
	    return $salida;
	}
}

if ( ! function_exists('print_menu_user')){
	function print_menu_user($nombre){ // MENÚ DE USUARIO
		$salida = '<h4>Bienvenido, '.$nombre.'</h4>
	                <ul class="accordion" data-accordion>
	                  <li class="menulink"><a href="'.site_url('home').'"><i class="fa fa-home fa-fw"></i> Inicio</a></li>
                      <li class="menulink"><a href="'.site_url('dashboard').'"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a></li>
                      <li class="accordion-navigation">
                        <a href="#panel1a"><i class="fa fa-check-circle fa-fw"></i> Postulaciones</a>
                        <div id="panel1a" class="content">
                          <ul>
                            <li><a href="'.site_url('dashboard/ofertas').'"><i class="fa fa-newspaper-o fa-fw"></i> Ver Ofertas</a></li>
                            <li><a href="'.site_url('dashboard/historial').'"><i class="fa fa-calendar fa-fw"></i> Historial de Postulaciones</a></li>
                          </ul>
                        </div>
                      </li>
                      <li class="accordion-navigation">
                        <a href="#panel2a"><i class="fa fa-file-text-o fa-fw"></i> Currículum Vitae</a>
                        <div id="panel2a" class="content">
                          <ul>
                            <li><a href="'.site_url('dashboard/curriculum').'"><i class="fa fa-pencil fa-fw"></i> Completar Currículum</a></li>
                          </ul>
                        </div>
                      </li>
                      <li class="accordion-navigation">
                        <a href="#panel3a"><i class="fa fa-user fa-fw"></i> Cuenta</a>
                        <div id="panel3a" class="content">
                          <ul>
                            <li><a href="'.site_url('login/cambiar_password').'"><i class="fa fa-unlock-alt fa-fw"></i> Cambiar Contraseña</a></li>
                            <li><a href="'.site_url('dashboard/config').'"><i class="fa fa-cog fa-fw"></i> Configuración</a></li>
                            <li><a href="'.site_url('login/log_out').'"><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesión</a></li>
                          </ul>


                        </div>
                      </li>
                    </ul>';
	    return $salida;
	}
}

if ( ! function_exists('print_login')){
    function print_login(){ // MENÚ PARA INICIAR SESIÓN
        $salida = '<h4>Iniciar sesión</h4>
                                    <form id="form_login" action="'.site_url('login/log_in').'">
                                        <div class="row">
                                                <div class="large-12 columns">
                                                    <div class="row collapse">
                                                        <div class="small-2 columns">
                                                                <span class="prefix"><i class="fa fa-user"></i></span>
                                                        </div>
                                                        <div class="small-10 columns">
                                                                <input id="rut_log" name="rut_log" type="text" placeholder="RUT (Ej: 12345678-9)" required pattern="^0*(\d{1,3}(\.?\d{3})*)\-?([\dkK])$"/>
                                                        </div>
                                                    </div>
                                                    <div class="row collapse">
                                                        <div class="small-2 columns">
                                                                <span class="prefix"><i class="fa fa-lock"></i></span>
                                                        </div>
                                                        <div class="small-10 columns">
                                                                <input id="pass_log" name="pass_log" type="password" placeholder="Contraseña" />
                                                        </div>
                                                    </div>
                                                    <a id="login_btn" href="#" class="button expand" type="submit">Entrar</a>
                                                    <br>
                                                    <a id="registrar" href="#"> Registrar cuenta </a>
                                                    <br>
                                                    <a href="#" data-reveal-id="modalRecuperar"> ¿Olvidó su clave? </a>
                                                </div>
                                        </div>
                                    </form>';
        return $salida;
    }
}

if ( ! function_exists('print_paises')){
	function print_paises(){
		echo '<option value="Afganistan">Afganistan</option>
                    <option value="Albania">Albania</option>
                    <option value="Alemania">Alemania</option>
                    <option value="Andorra">Andorra</option>
                    <option value="Angola">Angola</option>
                    <option value="Anguilla">Anguilla</option>
                    <option value="Arabia Saudita">Arabia Saudita</option>
                    <option value="Argelia">Argelia</option>
                    <option value="Argentina">Argentina</option>
                    <option value="Armenia">Armenia</option>
                    <option value="Aruba">Aruba</option>
                    <option value="Australia">Australia</option>
                    <option value="Austria">Austria</option>
                    <option value="Azerbaiyan">Azerbaiyan</option>
                    <option value="Bahamas">Bahamas</option>
                    <option value="Bahrein">Bahrein</option>
                    <option value="Bangladesh">Bangladesh</option>
                    <option value="Barbados">Barbados</option>
                    <option value="Belgica">Belgica</option>
                    <option value="Belice">Belice</option>
                    <option value="Benin">Benin</option>
                    <option value="Bermudas">Bermudas</option>
                    <option value="Bielorrusia">Bielorrusia</option>
                    <option value="Birmania">Birmania</option>
                    <option value="Bolivia">Bolivia</option>
                    <option value="Bosnia y Herzegovina">Bosnia y Herzegovina</option>
                    <option value="Botswana">Botswana</option>
                    <option value="Brasil">Brasil</option>
                    <option value="Brunei">Brunei</option>
                    <option value="Bulgaria">Bulgaria</option>
                    <option value="Burkina Faso">Burkina Faso</option>
                    <option value="Burundi">Burundi</option>
                    <option value="Butan">Butan</option>
                    <option value="Cabo Verde">Cabo Verde</option>
                    <option value="Camboya">Camboya</option>
                    <option value="Camerun">Camerun</option>
                    <option value="Canada">Canada</option>
                    <option value="Chad">Chad</option>
                    <option value="Chile"selected>Chile</option>
                    <option value="China">China</option>
                    <option value="Chipre">Chipre</option>
                    <option value="Colombia">Colombia</option>
                    <option value="Comores">Comores</option>
                    <option value="Congo">Congo</option>
                    <option value="Corea">Corea</option>
                    <option value="Corea del Norte">Corea del Norte</option>
                    <option value="Costa de Marfíl">Costa de Marfíl</option>
                    <option value="Costa Rica">CostaRica</option>
                    <option value="Croacia">Croacia</option>
                    <option value="Cuba">Cuba</option>
                    <option value="Dinamarca">Dinamarca</option>
                    <option value="Djibouti">Djibouti</option>
                    <option value="Dominica">Dominica</option>
                    <option value="Ecuador">Ecuador</option>
                    <option value="Egipto">Egipto</option>
                    <option value="El Salvador">El Salvador</option>
                    <option value="Emiratos Arabes Unidos">Emiratos Arabes Unidos</option>
                    <option value="Eritrea">Eritrea</option>
                    <option value="Eslovenia">Eslovenia</option>
                    <option value="España">España</option>
                    <option value="Estados Unidos">Estados Unidos</option>
                    <option value="Estonia">Estonia</option>
                    <option value="Etiopia">Etiopia</option>
                    <option value="Fiji">Fiji</option>
                    <option value="Filipinas">Filipinas</option>
                    <option value="Finlandia">Finlandia</option>
                    <option value="Francia">Francia</option>
                    <option value="Gabon">Gabon</option>
                    <option value="Gambia">Gambia</option>
                    <option value="Georgia">Georgia</option>
                    <option value="Ghana">Ghana</option>
                    <option value="Gibraltar">Gibraltar</option>
                    <option value="Granada">Granada</option>
                    <option value="Grecia">Grecia</option>
                    <option value="Groenlandia">Groenlandia</option>
                    <option value="Guadalupe">Guadalupe</option>
                    <option value="Guam">Guam</option>
                    <option value="Guatemala">Guatemala</option>
                    <option value="Guayana">Guayana</option>
                    <option value="Guayana Francesa">Guayana Francesa</option>
                    <option value="Guinea">Guinea</option>
                    <option value="Haiti">Haiti</option>
                    <option value="Honduras">Honduras</option>
                    <option value="Hungria">Hungria</option>
                    <option value="India">India</option>
                    <option value="Indonesia">Indonesia</option>
                    <option value="Irak">Irak</option>
                    <option value="Iran">Iran</option>
                    <option value="Irlanda">Irlanda</option>
                    <option value="Islandia">Islandia</option>
                    <option value="Islas Caiman">Islas Caiman</option>
                    <option value="Islas Faroe">Islas Faroe</option>
                    <option value="Islas Malvinas">Islas Malvinas</option>
                    <option value="Israel">Israel</option>
                    <option value="Italia">Italia</option>
                    <option value="Jamaica">Jamaica</option>
                    <option value="Japon">Japon</option>
                    <option value="Jordania">Jordania</option>
                    <option value="Kazajistan">Kazajistan</option>
                    <option value="Kenia">Kenia</option>
                    <option value="Kirguizistan">Kirguizistan</option>
                    <option value="Kiribati">Kiribati</option>
                    <option value="Kuwait">Kuwait</option>
                    <option value="Laos">Laos</option>
                    <option value="Lesotho">Lesotho</option>
                    <option value="Letonia">Letonia</option>
                    <option value="Libano">Libano</option>
                    <option value="Liberia">Liberia</option>
                    <option value="Libia">Libia</option>
                    <option value="Liechtenstein">Liechtenstein</option>
                    <option value="Lituania">Lituania</option>
                    <option value="Luxemburgo">Luxemburgo</option>
                    <option value="Macedonia">Macedonia</option>
                    <option value="Madagascar">Madagascar</option>
                    <option value="Malasia">Malasia</option>
                    <option value="Malawi">Malawi</option>
                    <option value="Maldivas">Maldivas</option>
                    <option value="Mali">Mali</option>
                    <option value="Malta">Malta</option>
                    <option value="Marruecos">Marruecos</option>
                    <option value="Martinica">Martinica</option>
                    <option value="Mauricio">Mauricio</option>
                    <option value="Mauritania">Mauritania</option>
                    <option value="Mayotte">Mayotte</option>
                    <option value="Mexico">Mexico</option>
                    <option value="Micronesia">Micronesia</option>
                    <option value="Moldavia">Moldavia</option>
                    <option value="Monaco">Monaco</option>
                    <option value="Mongolia">Mongolia</option>
                    <option value="Montserrat">Montserrat</option>
                    <option value="Mozambique">Mozambique</option>
                    <option value="Namibia">Namibia</option>
                    <option value="Nauru">Nauru</option>
                    <option value="Nepal">Nepal</option>
                    <option value="Nicaragua">Nicaragua</option>
                    <option value="Niger">Niger</option>
                    <option value="Nigeria">Nigeria</option>
                    <option value="Niue">Niue</option>
                    <option value="Norfolk">Norfolk</option>
                    <option value="Noruega">Noruega</option>
                    <option value="Nueva Caledonia">Nueva Caledonia</option>
                    <option value="Nueva Zelanda">Nueva Zelanda</option>
                    <option value="Oman">Oman</option>
                    <option value="Holanda">Holanda</option>
                    <option value="Panama">Panama</option>
                    <option value="Papua Nueva Guinea">Papua Nueva Guinea</option>
                    <option value="Paquistan">Paquistan</option>
                    <option value="Paraguay">Paraguay</option>
                    <option value="Peru">Peru</option>
                    <option value="Pitcairn">Pitcairn</option>
                    <option value="Polinesia Francesa">Polinesia Francesa</option>
                    <option value="Polonia">Polonia</option>
                    <option value="Portugal">Portugal</option>
                    <option value="Puerto Rico">Puerto Rico</option>
                    <option value="Qatar">Qatar</option>
                    <option value="Reino Unido">Reino Unido</option>
                    <option value="Republica Checa">Republica Checa</option>
                    <option value="República de Sudáfrica">República de Sudáfrica</option>
                    <option value="República Dominicana">República Dominicana</option>
                    <option value="República Eslovaca">República Eslovaca</option>
                    <option value="Reunión">Reunión</option>
                    <option value="Ruanda">Ruanda</option>
                    <option value="Rumania">Rumania</option>
                    <option value="Rusia">Rusia</option>
                    <option value="Sahara Occidental">Sahara Occidental</option>
                    <option value="Samoa">Samoa</option>
                    <option value="Samoa Americana">Samoa Americana</option>
                    <option value="San Marino">San Marino</option>
                    <option value="Santa Helena">Santa Helena</option>
                    <option value="Santa Lucía">Santa Lucía</option>
                    <option value="Senegal">Senegal</option>
                    <option value="Seychelles">Seychelles</option>
                    <option value="Sierra Leona">Sierra Leona</option>
                    <option value="Singapur">Singapur</option>
                    <option value="Siria">Siria</option>
                    <option value="Somalia">Somalia</option>
                    <option value="Sri Lanka">Sri Lanka</option>
                    <option value="Suazilandia">Suazilandia</option>
                    <option value="Sudán">Sudán</option>
                    <option value="Suecia">Suecia</option>
                    <option value="Suiza">Suiza</option>
                    <option value="Surinam">Surinam</option>
                    <option value="Tailandia">Tailandia</option>
                    <option value="Taiwán">Taiwán</option>
                    <option value="Tanzania">Tanzania</option>
                    <option value="Tayikistán">Tayikistán</option>
                    <option value="Timor Oriental">Timor Oriental</option>
                    <option value="Togo">Togo</option>
                    <option value="Tonga">Tonga</option>
                    <option value="Trinidad y Tobago">Trinidad y Tobago</option>
                    <option value="Túnez">Túnez</option>
                    <option value="Turkmenistán">Turkmenistán</option>
                    <option value="Turquía">Turquía</option>
                    <option value="Tuvalu">Tuvalu</option>
                    <option value="Ucrania">Ucrania</option>
                    <option value="Uganda">Uganda</option>
                    <option value="Uruguay">Uruguay</option>
                    <option value="Uzbekistán">Uzbekistán</option>
                    <option value="Vanuatu">Vanuatu</option>
                    <option value="Venezuela">Venezuela</option>
                    <option value="Vietnam">Vietnam</option>
                    <option value="Yemen">Yemen</option>
                    <option value="Yugoslavia">Yugoslavia</option>
                    <option value="Zambia">Zambia</option>
                    <option value="Zimbabue">Zimbabue</option>';
	}
}

if ( ! function_exists('print_meses')){
	function print_meses(){
		echo 	'<option value="01">Enero</option>
				<option value="02">Febrero</option>
				<option value="03">Marzo</option>
				<option value="04">Abril</option>
				<option value="05">Mayo</option>
				<option value="06">Junio</option>
				<option value="07">Julio</option>
				<option value="08">Agosto</option>
				<option value="09">Septiembre</option>
				<option value="10">Octubre</option>
				<option value="11">Noviembre</option>
				<option value="12">Diciembre</option>';
	}
}
