<div id="principal" class="row">
    <div class="large-4 small-12 columns">
        <div class="panel sidepanel">
            <?php echo print_menu_user($this->session->userdata('nombre')); ?>
        </div>
    </div>
    <div class="contenido large-8 columns">
        <div class="row">
            <div class="large-6 columns">
                <div class="panel">
                    <h4><i class="fa fa-clock-o"></i> Ofertas Recientes</h4>
                    <hr>
                    <ul>
                        <?php
                                                                                            foreach ($recientes as $oferta) {
                                                                                                echo '<li><a href="'.site_url('oferta/ver').'/'.$oferta['id'].'">'.$oferta['titulo'].'</a>'.'</li>';

                                                                                            }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="large-6 columns">
                <div class="panel">
                    <h4><i class="fa fa-thumbs-up"></i> Últimos Seleccionados</h4>
                    <hr>
                    <ul>
                        <?php
                                                                                        foreach ($seleccionados as $postulante) {
                                                                                            // echo '<li><a href="'.site_url('cv/ver').'/'.$reciente['rut'].'">'.$reciente['nombres'].' '.$reciente['apellidos'].'</a> en <a href="'.site_url('oferta/ver').'/'.$reciente['id_oferta'].'">'.$reciente['titulo'].'</a></li>';
                                                                                            echo '<li>'.$postulante['rut'].'</li>';
                                                                                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- <div class=" large-12 columns"> -->
                <!-- <div class="row"> -->

                        <div class="large-6 columns">
                            <div class="panel">
                            <h4>Consultar por RUT</h4>
                            <hr>
                            <form id="form_dashboard" action="<?php echo site_url('postulacion/buscar');?>">
                                <div class="row">
                                    <div class="large-12 columns">
                                        <div class="row collapse postfix-round">
                                            <div class="small-9 columns">
                                                <input type="text" id="rut" name="rut" placeholder="12345678-9">
                                            </div>
                                            <div class="small-3 columns">
                                                <a id="btn_postulaciones" href="#" class="button btndash postfix"><i class="fa fa-search"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div id="resultados">
                                <table id="tabla_resultados">
                                </table>
                            </div>
                            </div>
                        </div>
                        <div class="large-6 columns">
                            <div class="panel">
                            <h4>Buscar Oferta</h4>
                            <hr>
                            <form id="form_dashboard2" action="<?php echo site_url('oferta/buscar_termino');?>">
                                <div class="row">
                                    <div class="large-12 columns">
                                        <div class="row collapse postfix-round">
                                            <div class="small-9 large-9 columns">
                                                <input type="text" id="termino" name="termino" placeholder="Ingrese término">
                                            </div>
                                            <div class="small-3 large-3 columns">
                                                <a id="btn_ofertas" href="#" class="button btndash postfix"><i class="fa fa-search"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div id="resultados2">
                                <table id="tabla_resultados2">
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                <!-- </div> -->
            <!-- </div> -->
        </div>
    </div> <!-- principal -->
</div>
