<div class="row" style="margin-top: 10px;">
	<div class="large-4 small-12 columns">

		<div class="panel <?php
										if($this->session->userdata('logged_in')){
												echo 'sidepanel';
										}
				?>">
				<?php
												$user = $this->session->userdata('user_data');
												if (!isset($user)) {
												echo print_login();
												}
												else {
														if($this->session->userdata('logged_in')){
																if($this->session->userdata('rol')=='user'){
																echo print_menu_user($this->session->userdata('nombre'));
																}
																else{
																echo print_menu($this->session->userdata('nombre'));;
																}

														}
														else{
																echo print_login();
														}
												}
				?>
		</div>
    </div>
    <div id="panel_oferta" class="large-8 columns panel_avance" data-id-oferta="<?php echo $oferta['id'];?>" data-url="<?php echo site_url('postulacion/nueva');?>">
        <?php
                            echo '<h2>'.$oferta['titulo'].'</h2><br>';
                            echo '<strong>'.$oferta['puestos'].'</strong> puestos disponibles.';
        ?>

						<h3 style="font-size: 18px;">Etapa Actual: <strong><?php
                    echo obtenerEtapas($avance-1);
            ?></strong></h3>
						<table id="tabla_avance" cellspacing="0">
				        <tbody>
				                <tr><td class="fill" data-etapa="1" data-hasqtip="24" oldtitle="Recepción de Currículum" title="" aria-describedby="qtip-24"></td><td class="fill" data-etapa="2" data-hasqtip="25" oldtitle="Entrevista Psicológica" title=""></td><td class="fill" data-etapa="3" data-hasqtip="26" oldtitle="Entrevista Personal" title=""></td><td data-hasqtip="27" oldtitle="Selección" title=""></td></tr>
				        </tbody>
				    </table>
						<table id="info_avance" cellspacing="0">
				        <tbody>
				             <tr>
				             	<td class="fill">
				             		<ul>
													<?php
						            		foreach($etapa1 as $postulante){
						            			echo '<li>'.$postulante.'</li>';
						            		}
						            	?>
				             		</ul>
				             	</td>
				             	<td class="fill">
				             		<ul>
													<?php
						            		foreach($etapa2 as $postulante){
						            			echo '<li>'.$postulante.'</li>';
						            		}
						            	?>
				             		</ul>
				             	</td>
				             	<td class="fill">
				             		<ul>
													<?php
						            		foreach($etapa3 as $postulante){
						            			echo '<li>'.$postulante.'</li>';
						            		}
						            	?>
				             		</ul>
				             	</td>
				             	<td class="fill">
				             		<ul>
													<?php
						            		foreach($etapa4 as $postulante){
						            			echo '<li>'.$postulante.'</li>';
						            		}
						            	?>
				             		</ul>
				             	</td>
				             </tr>
									 </tbody>
							 </table>
    </div>
</div>
