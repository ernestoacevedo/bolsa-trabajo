<div class="row" style="margin-top: 10px;">
    <div class="large-4 small-12 columns">
        <div class="panel <?php
                        if($this->session->userdata('logged_in')){
                            echo 'sidepanel';
                        }
            ?>">
            <?php
                            $user = $this->session->userdata('user_data');
                            if (!isset($user)) {
                            echo print_login();
                            }
                            else {
                            if($this->session->userdata('logged_in')){
                                if($this->session->userdata('rol')=='user'){
                                echo print_menu_user($this->session->userdata('nombre'));
                                }
                                else{
                                echo print_menu($this->session->userdata('nombre'));;
                                }
                                
                            }
                            else{
                                echo print_login();
                            }
                            }
            ?>
        </div>
    </div>
    <div class="contenido large-8 small-12 columns">
	    <div class="panel">
		    <div class="row">
                <div class="large-12 columns">
					<div class="row collapse postfix-round">
				        <form id="form_buscar" action="<?php echo site_url('oferta/buscar')?>" onsubmit="return false;">
				        <div class="small-9 large-9 columns">
				          <input type="text" id="texto_busc" name="texto_busc" placeholder="Ingrese una palabra clave">
				        </div>
				        <div class="small-3 large-3 columns">
				          <a id="btn_buscar" href="#" class="button postfix"><i class="fa fa-search"></i> Buscar</a>
				        </div>
				        </form>
				    </div>
                </div>
		    </div>
        </div>
        <div class="row">
            <div id="panelResultados" class="large-12 columns" data-oferta-url="<?php echo site_url('/oferta/ver/');?>">
            <?php
                foreach($ofertas as $oferta){
                    echo '<h4><i class="fa fa-suitcase"></i> '.$oferta['titulo'].'</h4>
                <p><strong>Descripción </strong>'.$oferta['descripcion'].'</p>
                <ul class="inline-list">
                    <li><a href="'.site_url("/oferta/ver/".$oferta['id']).'">Ver más detalles</a></li>
                </ul>
                <hr/>';
                }
                
                echo $this->pagination->create_links();
            ?>  
            </div>
        </div>
    </div>
</div>
</div>