<div class="row" style="margin-top: 10px;">
    <div class="large-4 small-12 columns">
        <div class="panel sidepanel">
            <?php echo print_menu_user($this->session->userdata('nombre'));?>
        </div>
    </div>
    <div class="contenido large-8 columns">
        <ul class="breadcrumbs">
            <li><a href="<?php echo site_url('dashboard/personal');?>">1. Información Personal</a></li>
            <li><a href="<?php echo site_url('dashboard/academica');?>">2. Información Académica</a></li>
            <li class="current"><a href="#">3. Antecedentes Laborales</a></li>
        </ul>
        <h3><i class="fa fa-suitcase"></i> Antecedentes Laborales</h3>
        <a id="add_exp" href="#" class="button small"><i class="fa fa-plus"></i> Empresa</a>
        <a id="add_ref" href="#" class="button small"><i class="fa fa-plus"></i> Referencia</a>
        <h4> Experiencia Laboral</h4>
        <table id="tabla_laboral" class="" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Empresa
                </th>
                <th style="text-align: center;">
                    Cargo
                </th>
                <th style="text-align: center;">
                    País
                </th>
                <th style="text-align: center;">
                    Inicio
                </th>
                <th style="text-align: center;">
                    Término
                </th>
                <th style="text-align: center;">
                    Editar
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                                                                foreach($info_antecedentes as $antecedente){
                                                                                                    echo '<tr>';
                                                                                                                        echo '<td style="text-align: center;">'.$antecedente['nombre_empresa'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$antecedente['cargo'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$antecedente['pais'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$antecedente['mes_inicio'].'/'.$antecedente['inicio'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$antecedente['mes_termino'].'/'.$antecedente['termino'].'</td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="#" data-url="'.site_url('laboral/obtener').'/'.$antecedente['id'].'" class="editarexp editar"><i class="fa fa-pencil fa-lg"></i></a></td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="'.site_url('laboral/eliminar').'/'.$antecedente['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                                                                    echo '</tr>';
                                                                                                }
                ?>
            </tbody>
        </table>
        <h4> Referencias</h4>
        <table id="tabla_referencias" class="" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Contacto
                </th>
                <th style="text-align: center;">
                    Cargo
                </th>
                <th style="text-align: center;">
                    Teléfono
                </th>
                <th style="text-align: center;">
                    Correo electrónico
                </th>
                <th style="text-align: center;">
                    Editar
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                                                                foreach($info_referencias as $referencia){
                                                                                                    echo '<tr>';
                                                                                                                        echo '<td style="text-align: center;">'.$referencia['nombre'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$referencia['cargo'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$referencia['telefono'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$referencia['email'].'</td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="#" data-url="'.site_url('laboral/obtener').'/'.$referencia['id'].'" class="editarref editar"><i class="fa fa-pencil fa-lg"></i></a></td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="'.site_url('laboral/eliminar').'/'.$referencia['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                                                                    echo '</tr>';
                                                                                                }
                ?>
            </tbody>
        </table>
        <div class="row">
            <div class="small-6 large-centered columns">
                <a href="<?php echo site_url('dashboard/academica');?>" class="button warning"> Atrás </a>
                <a href="<?php echo site_url('dashboard');?>" class="button success"> Fin </a>
            </div>
        </div>
    </div>
</div>
<div id="modalLaboral" class="reveal-modal small" data-reveal aria-labelledby="expTitle" aria-hidden="true" role="dialog">
    <h2 id="expTitle">Agregar Empresa</h2>
    <form id="form_agregar_exp" action="<?php echo site_url('laboral/agregar');?>">
        <input type="hidden" id="id_exp" name="id_exp">
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Empresa</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="nombre_empresa" name="nombre_empresa">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">País</label>
            </div>
            <div class="small-10 columns">
                <select name="pais" id="pais">
                    <?php echo print_paises(); ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Cargo</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="cargo" name="cargo">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Período Inicio</label>
            </div>
            <div class="small-4 columns">
                <select name="mes_inicio" id="mes_inicio">
                    <?php echo print_meses(); ?>
                </select>
            </div>
            <div class="small-2 columns">
                <label class="right inline">Año</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="inicio" id="inicio" min="1901" value="2015">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Período Término</label>
            </div>
            <div class="small-4 columns">
                <select name="mes_termino" id="mes_termino">
                    <?php echo print_meses(); ?>
                </select>
            </div>
            <div class="small-2 columns">
                <label class="right inline">Año</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="termino" id="termino" min="1901" value="2015">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Funciones Principales</label>
            </div>
            <div class="small-10 columns">
                <textarea name="funciones" id="funciones" cols="30" rows="5"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Logros Obtenidos</label>
            </div>
            <div class="small-10 columns">
                <textarea name="logros" id="logros" cols="30" rows="5"></textarea>
            </div>
        </div>
        <a id="btn_add_exp" href="#" class="button expand">Agregar</a>
        <a id="btn_act_exp" href="#" class="button warning expand" style="display: none;" data-url="<?php echo site_url('laboral/editar');?>">Actualizar</a>
    </form>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<div id="modalReferencia" class="reveal-modal small" data-reveal aria-labelledby="refTitle" aria-hidden="true" role="dialog">
    <h2 id="refTitle">Agregar Referencia</h2>
    <form id="form_agregar_ref" action="<?php echo site_url('laboral/agregar_referencia');?>">
        <input type="hidden" id="id_ref" name="id_ref">
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Nombre Contacto</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="nombre_ref" name="nombre_ref">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Nombre Institución</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="institucion_ref" name="institucion_ref">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Cargo</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="cargo_ref" name="cargo_ref">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Relación Laboral</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="relacion_ref" name="relacion_ref">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Teléfono</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="telefono_ref" name="telefono_ref">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Correo Electrónico</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="email_ref" name="email_ref">
            </div>
        </div>
        <a id="btn_add_ref" href="#" class="button expand">Agregar</a>
        <a id="btn_act_ref" href="#" class="button warning expand" style="display: none;" data-url="<?php echo site_url('laboral/editar_referencia');?>">Actualizar</a>
    </form>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
</div>