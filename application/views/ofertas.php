<div class="row" style="margin-top: 10px;">
    <div class="large-4 small-12 columns">
        <div class="panel sidepanel">
            <?php echo print_menu($this->session->userdata('nombre'));?>
        </div>
    </div>
    <div class="contenido large-8 columns">
        <h2>Listado de Ofertas</h2>
        <table id="tabla_ofertas" class="datatable" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Título
                </th>
                <th style="text-align: center;">
                    Estado
                </th>
                <th style="text-align: center;">
                    Visibilidad
                </th>
                <th style="text-align: center;">
                    Fecha Publicación
                </th>
                <th style="text-align: center;">
                    Fecha Cierre
                </th>
                <th style="text-align: center;">
                    Descargar Postulantes
                </th>
                <th style="text-align: center;">
                    Editar
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                    foreach($ofertas as $oferta){
                                                    $visible = ($oferta['estado'] == 1) ? 'Visible' : 'Oculta';
                                                    echo '<tr>';
                                                            echo '<td style="text-align: center;">'.$oferta['titulo'].'</td>';
                                                            echo '<td style="text-align: center;">'.obtenerEstado($oferta['activa']).'</td>';
                                                            echo '<td style="text-align: center;">'.$visible.'</td>';
                                                            echo '<td style="text-align: center;">'.$oferta['fecha_publicacion'].'</td>';
                                                            echo '<td style="text-align: center;">'.$oferta['fecha_cierre'].'</td>';
                                                            echo '<td style="text-align: center;"><a target="_blank" href="'.site_url('cv/postulantes_oferta').'/'.$oferta['id'].'" class=""><i class="fa fa-users fa-lg"></i></a></td>';
                                                            echo '<td style="text-align: center;"><a href="'.site_url('admin/editar').'/'.$oferta['id'].'" data-url="'.site_url('admin/editar').'/'.$oferta['id'].'" style="color: #e67e22;"><i class="fa fa-pencil fa-lg"></i></a></td>';
                                                            echo '<td style="text-align: center;"><a href="'.site_url('oferta/eliminar').'/'.$oferta['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                    echo '</tr>';
                                                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
</div>
