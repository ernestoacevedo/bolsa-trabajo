<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title> Unidad de Reclutamiento y Selección </title>
		<script src="<?php echo base_url();?>assets/js/pace.min.js"></script>
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/pace.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/foundation.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/foundation-datepicker.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/cool-share.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.dataTables.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.qtip.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-te-1.4.0.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/b-trabajo.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.css" />
        <style>
        #loader{
            margin-left: 30%;
        }
        #circleG{
        width:93.33333333333333px;
        }
        .circleG{
        background-color:#BDDAE3;
        float:left;
        height:20px;
        margin-left:11px;
        width:20px;
        -moz-animation-name:bounce_circleG;
        -moz-animation-duration:1.9500000000000002s;
        -moz-animation-iteration-count:infinite;
        -moz-animation-direction:normal;
        -moz-border-radius:13px;
        -webkit-animation-name:bounce_circleG;
        -webkit-animation-duration:1.9500000000000002s;
        -webkit-animation-iteration-count:infinite;
        -webkit-animation-direction:normal;
        -webkit-border-radius:13px;
        -ms-animation-name:bounce_circleG;
        -ms-animation-duration:1.9500000000000002s;
        -ms-animation-iteration-count:infinite;
        -ms-animation-direction:normal;
        -ms-border-radius:13px;
        -o-animation-name:bounce_circleG;
        -o-animation-duration:1.9500000000000002s;
        -o-animation-iteration-count:infinite;
        -o-animation-direction:normal;
        -o-border-radius:13px;
        animation-name:bounce_circleG;
        animation-duration:1.9500000000000002s;
        animation-iteration-count:infinite;
        animation-direction:normal;
        border-radius:13px;
        }
        #circleG_1{
        -moz-animation-delay:0.39s;
        -webkit-animation-delay:0.39s;
        -ms-animation-delay:0.39s;
        -o-animation-delay:0.39s;
        animation-delay:0.39s;
        }
        #circleG_2{
        -moz-animation-delay:0.9099999999999999s;
        -webkit-animation-delay:0.9099999999999999s;
        -ms-animation-delay:0.9099999999999999s;
        -o-animation-delay:0.9099999999999999s;
        animation-delay:0.9099999999999999s;
        }
        #circleG_3{
        -moz-animation-delay:1.1700000000000002s;
        -webkit-animation-delay:1.1700000000000002s;
        -ms-animation-delay:1.1700000000000002s;
        -o-animation-delay:1.1700000000000002s;
        animation-delay:1.1700000000000002s;
        }
        @-moz-keyframes bounce_circleG{
        0%{
        }
        50%{
        background-color:#1DABE3}
        100%{
        }
        }
        @-webkit-keyframes bounce_circleG{
        0%{
        }
        50%{
        background-color:#1DABE3}
        100%{
        }
        }
        @-ms-keyframes bounce_circleG{
        0%{
        }
        50%{
        background-color:#1DABE3}
        100%{
        }
        }
        @-o-keyframes bounce_circleG{
        0%{
        }
        50%{
        background-color:#1DABE3}
        100%{
        }
        }
        @keyframes bounce_circleG{
        0%{
        }
        50%{
        background-color:#1DABE3}
        100%{
        }
        }
        </style>
        <script src="<?php echo base_url();?>assets/js/modernizr.js"></script>
<!--         <script src='https://www.google.com/recaptcha/api.js'></script> -->
    </head>
    <body>
        <div class="row">
            <div class="large-12 columns">


                <div class="row">
                    <div class="large-12 columns">

                        <div id="header" class="contain-to-grid">
                            <nav class="top-bar" data-topbar data-options="sticky_on: large" role="navigation">
                                <ul class="title-area">

                                    <li class="name">
                                        <h1>
                                        <a href="<?php echo base_url();?>">
                                        <img src="<?php echo base_url();?>assets/img/logo.jpg" height="60" width="200">
                                        </a>
                                        </h1>
                                    </li>
                                    <li class="toggle-topbar menu-icon"><a href="#"><span>menu</span></a></li>
                                </ul>

                                <section class="top-bar-section">
									<div class="info-top">
                                    	<div class="textwidget">
                                        	<i class="fa fa-phone"></i> Atracción y selección (71) 2412560 - (71) 2412832
                                        </div>
									</div>
                                    <ul class="right">
                                    	<!-- botones
                                        <li><a href="?php echo base_url();?>">Inicio</a></li>
                                        <li><a href="?php echo site_url('/buscar');?>">Buscar</a></li>
                                        -->
                                        <?php
/*
                                                        if($this->session->userdata('logged_in')){
                                                        if($this->session->userdata('rol')=='user'){
                                                        echo '<li class="has-dropdown">
                                                            <a href="#">'.$this->session->userdata("nombre").'</a>
                                                            <ul class="dropdown">
                                                                <li><a href="'.site_url("dashboard").'"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                                                                    <li><a href="'.site_url("cv/micv").'"><i class="fa fa-file-text-o"></i> Mi CV</a></li>
                                                                    <li><a href="#"><i class="fa fa-check-circle"></i> Postulaciones</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="'.site_url("login/log_out").'"><i class="fa fa-sign-out"></i> Cerrar Sesión</a></li>
                                                            </ul>
                                                        </li>';
                                                        }
                                                        else{
                                                        echo '<li class="has-dropdown">
                                                            <a href="#">'.$this->session->userdata("nombre").'</a>
                                                            <ul class="dropdown">
                                                                <li><a href="'.site_url('admin').'"><i class="fa fa-wrench"></i> Panel de Administración</a></li>
                                                                <li class="divider"></li>
                                                                    <li><a href="'.site_url("login/log_out").'"><i class="fa fa-sign-out"></i> Cerrar Sesión</a></li>
                                                            </ul>
                                                        </li>';
                                                        }

                                                        }
*/
                                        ?>
                                    </ul>
                                </section>
                            </nav>
                        </div>

                    </div>
                </div>
