<div class="row" style="margin-top: 10px;">



    <div class="large-4 small-12 columns">

        <div class="panel <?php
                        if($this->session->userdata('logged_in')){
                            echo 'sidepanel';
                        }
            ?>">
            <?php
                            $user = $this->session->userdata('user_data');
                            if (!isset($user)) {
                            echo print_login();
                            }
                            else {
                                if($this->session->userdata('logged_in')){
                                    if($this->session->userdata('rol')=='user'){
                                    echo print_menu_user($this->session->userdata('nombre'));
                                    }
                                    else{
                                    echo print_menu($this->session->userdata('nombre'));;
                                    }
                                    
                                }
                                else{
                                    echo print_login();
                                }
                            }
            ?>
        </div>
    </div>

    <div class="contenido large-8 columns">
        <h3><i class="fa fa-user"></i> Currículum Vitae</h3>
        <form id="form_info_personal" action="<?php echo site_url('reemplazo/editar_usuario');?>">
            <input type="hidden" name="rut" id="rut" value="<?php echo $usuario['rut'] ?>">
            <div class="row">
                <div class="small-2 columns">
                    <label for="nombres" class="right inline">Nombres</label>
                </div>
                <div class="small-10 columns">
                    <input type="text" name="nombres" id="nombres" value="<?php echo $usuario['nombres'];?>">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="apellidos" class="right inline">Apellidos</label>
                </div>
                <div class="small-10 columns">
                    <input type="text" name="apellidos" id="apellidos" value="<?php echo $usuario['apellidos'];?>">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="ciudad" class="right inline">Ciudad</label>
                </div>
                <div class="small-10 columns">
                    <input type="text" name="ciudad" id="ciudad" value="<?php echo $usuario['ciudad'];?>">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="telefono" class="right inline">Teléfono Fijo</label>
                </div>
                <div class="small-4 columns">
                    <input type="text" name="telefono" id="telefono" value="<?php echo $usuario['telefono'];?>">
                </div>
                <div class="small-2 columns">
                    <label for="celular" class="right inline">Teléfono Móvil</label>
                </div>
                <div class="small-4 columns">
                    <input type="text" name="celular" id="celular" value="<?php echo $usuario['celular'];?>">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="email" class="right inline">Correo electrónico</label>
                </div>
                <div class="small-10 columns">
                    <input type="email" name="email" id="email" value="<?php echo $usuario['email'];?>">
                </div>
            </div>
            <h3><i class="fa fa-graduation-cap"></i> Información Académica</h3>
        <a id="add_med" href="#" class="button small"><i class="fa fa-plus"></i> Enseñanza Media</a>
        <a id="add_sup" href="#" class="button small"><i class="fa fa-plus"></i> Educación Superior</a>
        <a id="add_esp" href="#" class="button small"><i class="fa fa-plus"></i> Especialización</a>
        <a id="add_cap" href="#" class="button small"><i class="fa fa-plus"></i> Capacitación</a>
        <h4 style="font-size: 20px;"> Enseñanza Media</h4>
        <table id="tabla_med" class="tablacv" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Nombre
                </th>
                <th style="text-align: center;">
                    Tipo
                </th>
                <th style="text-align: center;">
                    País
                </th>
                <th style="text-align: center;">
                    Año Inicio
                </th>
                <th style="text-align: center;">
                    Año Término
                </th>
                <th style="text-align: center;">
                    Editar
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                                                                foreach($info_media as $institucion){
                                                                                                    echo '<tr>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['nombre'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['tipo'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['pais'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['inicio'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['termino'].'</td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="#" data-url="'.site_url('reemplazo/obtener').'/'.$institucion['id'].'" class="editarmedia editar"><i class="fa fa-pencil fa-lg"></i></a></td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="'.site_url('reemplazo/eliminar').'/'.$institucion['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                                                                    echo '</tr>';
                                                                                                }
                ?>
            </tbody>
        </table>
        <h4 style="font-size: 20px;"> Educación Superior</h4>
        <table id="tabla_sup" class="tablacv" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Nombre
                </th>
                <th style="text-align: center;">
                    Tipo
                </th>
                <th style="text-align: center;">
                    Carrera
                </th>
                <th style="text-align: center;">
                    Año Término
                </th>
                <th style="text-align: center;">
                    Situación
                </th>
                <th style="text-align: center;">
                    Grado Académico
                </th>
                <th style="text-align: center;">
                    Editar
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                                                                foreach($info_superior as $institucion){
                                                                                                    echo '<tr>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['nombre'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['tipo'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['carrera'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['termino'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['situacion'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['grado'].'</td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="#" data-url="'.site_url('reemplazo/obtener').'/'.$institucion['id'].'" class="editarsup editar"><i class="fa fa-pencil fa-lg"></i></a></td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="'.site_url('reemplazo/eliminar').'/'.$institucion['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                                                                    echo '</tr>';
                                                                                                }
                ?>
            </tbody>
        </table>
        <h4 style="font-size: 20px;"> Estudios de Especialización</h4>
        <table id="tabla_cap" class="tablacv" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Nombre
                </th>
                <th style="text-align: center;">
                    Institución
                </th>
                <th style="text-align: center;">
                    Tipo
                </th>
                <th style="text-align: center;">
                    Inicio
                </th>
                <th style="text-align: center;">
                    Término
                </th>
                <th style="text-align: center;">
                    Editar
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                                                                foreach($info_especializaciones as $esp){
                                                                                                    echo '<tr>';
                                                                                                                        echo '<td style="text-align: center;">'.$esp['nombre'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$esp['institucion'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$esp['tipo_esp'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$esp['mes_inicio'].'/'.$esp['inicio'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$esp['mes_termino'].'/'.$esp['termino'].'</td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="#" data-url="'.site_url('reemplazo/obtener').'/'.$esp['id'].'" class="editaresp editar"><i class="fa fa-pencil fa-lg"></i></a></td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="'.site_url('reemplazo/eliminar').'/'.$esp['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                                                                    echo '</tr>';
                                                                                                }
                ?>
            </tbody>
        </table>
        <h4 style="font-size: 20px;"> Capacitaciones</h4>
        <table id="tabla_cap" class="tablacv" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Capacitación
                </th>
                <th style="text-align: center;">
                    Institución
                </th>
                <th style="text-align: center;">
                    Inicio
                </th>
                <th style="text-align: center;">
                    Término
                </th>
                <th style="text-align: center;">
                    Horas
                </th>
                <th style="text-align: center;">
                    Editar
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                                                                foreach($info_capacitaciones as $capacitacion){
                                                                                                    echo '<tr>';
                                                                                                                        echo '<td style="text-align: center;">'.$capacitacion['nombre'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$capacitacion['institucion'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$capacitacion['mes_inicio'].'/'.$capacitacion['inicio'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$capacitacion['mes_termino'].'/'.$capacitacion['termino'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$capacitacion['horas'].'</td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="#" data-url="'.site_url('reemplazo/obtener').'/'.$capacitacion['id'].'" class="editarcap editar"><i class="fa fa-pencil fa-lg"></i></a></td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="'.site_url('reemplazo/eliminar').'/'.$capacitacion['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                                                                    echo '</tr>';
                                                                                                }
                ?>
            </tbody>
        </table>
        <h3><i class="fa fa-suitcase"></i> Antecedentes Laborales</h3>
        <a id="add_exp" href="#" class="button small"><i class="fa fa-plus"></i> Empresa</a>
        <a id="add_ref" href="#" class="button small"><i class="fa fa-plus"></i> Referencia</a>
        <h4 style="font-size: 20px;"> Experiencia Laboral</h4>
        <table id="tabla_laboral" class="tablacv" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Empresa
                </th>
                <th style="text-align: center;">
                    Cargo
                </th>
                <th style="text-align: center;">
                    País
                </th>
                <th style="text-align: center;">
                    Inicio
                </th>
                <th style="text-align: center;">
                    Término
                </th>
                <th style="text-align: center;">
                    Editar
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                                                                foreach($info_antecedentes as $antecedente){
                                                                                                    echo '<tr>';
                                                                                                                        echo '<td style="text-align: center;">'.$antecedente['nombre_empresa'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$antecedente['cargo'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$antecedente['pais'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$antecedente['mes_inicio'].'/'.$antecedente['inicio'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$antecedente['mes_termino'].'/'.$antecedente['termino'].'</td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="#" data-url="'.site_url('reemplazo/obtener_laboral').'/'.$antecedente['id'].'" class="editarexp editar"><i class="fa fa-pencil fa-lg"></i></a></td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="'.site_url('reemplazo/eliminar_laboral').'/'.$antecedente['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                                                                    echo '</tr>';
                                                                                                }
                ?>
            </tbody>
        </table>
        <h4 style="font-size: 20px;"> Referencias</h4>
        <table id="tabla_referencias" class="tablacv" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Contacto
                </th>
                <th style="text-align: center;">
                    Cargo
                </th>
                <th style="text-align: center;">
                    Teléfono
                </th>
                <th style="text-align: center;">
                    Correo electrónico
                </th>
                <th style="text-align: center;">
                    Editar
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                                                                foreach($info_referencias as $referencia){
                                                                                                    echo '<tr>';
                                                                                                                        echo '<td style="text-align: center;">'.$referencia['nombre'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$referencia['cargo'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$referencia['telefono'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$referencia['email'].'</td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="#" data-url="'.site_url('reemplazo/obtener').'/'.$referencia['id'].'" class="editarref editar"><i class="fa fa-pencil fa-lg"></i></a></td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="'.site_url('reemplazo/eliminar').'/'.$referencia['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                                                                    echo '</tr>';
                                                                                                }
                ?>
            </tbody>
        </table>
            <div class="row">
                <div class="large-8 large-offset-4 columns">
                    <a href="<?php echo site_url('dashboard');?>" id="cancelar" class="button alert"> Cancelar </a>
                    <a id="actualizar_info_btn" href="#" class="button"> Guardar </a>
                </div>
            </div>

        </form>
    </div>
</div>
<div id="modalMedia" class="reveal-modal small" data-reveal aria-labelledby="medTitle" aria-hidden="true" role="dialog">
    <h2 id="medTitle">Agregar Establecimiento</h2>
    <form id="form_agregar_media" action="<?php echo site_url('reemplazo/agregar_media');?>">
        <input id="id_med" name="id_med" type="hidden">
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Nombre</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="institucion_med" name="institucion_med">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Tipo</label>
            </div>
            <div class="small-10 columns">
                <select name="tipo_med" id="tipo_med">
                    <option value="Científico-Humanista">Científico Humanista</option>
                    <option value="Técnico Profesional">Técnico Profesional</option>
                    <option value="Laboral">Laboral</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Título Profesional</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="titulo_med" name="titulo_med" placeholder="Opcional">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">País</label>
            </div>
            <div class="small-10 columns">
                <select name="pais_med" id="pais_med">
                    <?php echo print_paises(); ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Ciudad</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="ciudad_med" name="ciudad_med">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Año Inicio</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="inicio_med" id="inicio_med" min="1900">
            </div>
            <div class="small-2 columns">
                <label class="right inline">Año Término</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="termino_med" id="termino_med" min="1901">
            </div>
        </div>
        <a id="btn_add_med" href="#" class="button expand">Agregar</a>
        <a id="btn_act_med" href="#" class="button warning expand" style="display: none;" data-url="<?php echo site_url('reemplazo/editar_media');?>">Actualizar</a>
    </form>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!--  -->
<div id="modalSuperior" class="reveal-modal small" data-reveal aria-labelledby="supTitle" aria-hidden="true" role="dialog">
    <h2 id="supTitle">Agregar Establecimiento</h2>
    <form id="form_agregar_superior" action="<?php echo site_url('reemplazo/agregar_superior');?>">
        <input id="id_sup" name="id_sup" type="hidden">
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Nombre</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="institucion_sup" name="institucion_sup">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Tipo</label>
            </div>
            <div class="small-10 columns">
                <select name="tipo_sup" id="tipo_sup">
                    <option value="Universidad">Universidad</option>
                    <option value="Instituto Profesional">Instituto Profesional</option>
                    <option value="Centro de Formación Técnica">Centro de Formación Técnica</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">País</label>
            </div>
            <div class="small-10 columns">
                <select name="pais_sup" id="pais_sup">
                    <?php echo print_paises(); ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Ciudad</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="ciudad_sup" name="ciudad_sup">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Carrera</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="carrera_sup" name="carrera_sup">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Año Inicio</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="inicio_sup" id="inicio_sup" min="1900">
            </div>
            <div class="small-2 columns">
                <label class="right inline">Año Término</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="termino_sup" id="termino_sup" min="1901">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Situación</label>
            </div>
            <div class="small-4 columns">
                <select name="situacion_sup" id="situacion_sup">
                    <option value="No Tiene">No Tiene</option>
                    <option value="Incompleta">Incompleta</option>
                    <option value="Titulado">Titulado</option>
                    <option value="Egresado">Egresado</option>
                    <option value="En Trámite">En Trámite</option>
                </select>
            </div>
            <div class="small-2 columns">
                <label class="right inline">Grado Académico</label>
            </div>
            <div class="small-4 columns">
                <select name="grado_sup" id="grado_sup">
                    <option value="No Tiene">No Tiene</option>
                    <option value="Doctorado">Doctorado</option>
                    <option value="Licenciado">Licenciado</option>
                    <option value="Profesional">Profesional</option>
                    <option value="Técnico Profesional">Técnico Profesional</option>
                </select>
            </div>
        </div>
        <a id="btn_add_sup" href="#" class="button expand">Agregar</a>
        <a id="btn_act_sup" href="#" class="button warning expand"  style="display: none;" data-url="<?php echo site_url('reemplazo/editar_superior');?>">Actualizar</a>
    </form>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!--  -->
<div id="modalCapacitacion" class="reveal-modal small" data-reveal aria-labelledby="capTitle" aria-hidden="true" role="dialog">
    <h2 id="capTitle">Agregar Capacitación</h2>
    <form id="form_agregar_capacitacion" action="<?php echo site_url('reemplazo/agregar_capacitacion');?>">
        <input id="id_cap" name="id_cap" type="hidden">
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Institución</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="institucion_cap" name="institucion_cap" placeholder="Nombre de la Institución">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Lugar</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="ciudad_cap" name="ciudad_cap">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Capacitación</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="nombre_cap" name="nombre_cap" placeholder="Nombre del Curso / Seminario">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Período Inicio</label>
            </div>
            <div class="small-4 columns">
                <select name="mes_inicio_cap" id="mes_inicio_cap">
                    <?php echo print_meses(); ?>
                </select>
            </div>
            <div class="small-2 columns">
                <label class="right inline">Año</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="inicio_cap" id="inicio_cap" min="1901">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Período Término</label>
            </div>
            <div class="small-4 columns">
                <select name="mes_termino_cap" id="mes_termino_cap">
                    <?php echo print_meses(); ?>
                </select>
            </div>
            <div class="small-2 columns">
                <label class="right inline">Año</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="termino_cap" id="termino_cap" min="1901">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Horas</label>
            </div>
            <div class="small-4 columns end">
                <input type="number" name="horas_cap" id="horas_cap" min="0">
            </div>
        </div>
        <a id="btn_add_cap" href="#" class="button expand">Agregar</a>
        <a id="btn_act_cap" href="#" class="button warning expand"  style="display: none;" data-url="<?php echo site_url('reemplazo/editar_capacitacion');?>">Actualizar</a>
    </form>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!--  -->
<div id="modalEspecializacion" class="reveal-modal small" data-reveal aria-labelledby="espTitle" aria-hidden="true" role="dialog">
    <h2 id="espTitle">Agregar Especialización</h2>
    <form id="form_agregar_especializacion" action="<?php echo site_url('reemplazo/agregar_especializacion');?>">
        <input id="id_esp" name="id_esp" type="hidden">
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Institución</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="institucion_esp" name="institucion_esp" placeholder="Nombre de la Institución">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Nombre</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="nombre_esp" name="nombre_esp" placeholder="Nombre del Magíster / Diplomado / Post Título">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Tipo</label>
            </div>
            <div class="small-10 columns end">
                <select name="tipo_esp" id="tipo_esp">
                    <option value="Post Título">Post Título</option>
                    <option value="Magíster">Magíster</option>
                    <option value="Doctorado">Doctorado</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Período Inicio</label>
            </div>
            <div class="small-4 columns">
                <select name="mes_inicio_esp" id="mes_inicio_esp">
                    <?php echo print_meses(); ?>
                </select>
            </div>
            <div class="small-2 columns">
                <label class="right inline">Año</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="inicio_esp" id="inicio_esp" min="1901">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Período Término</label>
            </div>
            <div class="small-4 columns">
                <select name="mes_termino_esp" id="mes_termino_esp">
                    <?php echo print_meses(); ?>
                </select>
            </div>
            <div class="small-2 columns">
                <label class="right inline">Año</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="termino_esp" id="termino_esp" min="1901">
            </div>
        </div>
        <a id="btn_add_esp" href="#" class="button expand">Agregar</a>
        <a id="btn_act_esp" href="#" class="button warning expand"  style="display: none;" data-url="<?php echo site_url('reemplazo/editar_especializacion');?>">Actualizar</a>
    </form>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!--  -->
<div id="modalLaboral" class="reveal-modal small" data-reveal aria-labelledby="expTitle" aria-hidden="true" role="dialog">
    <h2 id="expTitle">Agregar Empresa</h2>
    <form id="form_agregar_exp" action="<?php echo site_url('reemplazo/agregar_laboral');?>">
        <input type="hidden" id="id_exp" name="id_exp">
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Empresa</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="nombre_empresa" name="nombre_empresa">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">País</label>
            </div>
            <div class="small-10 columns">
                <select name="pais" id="pais">
                    <?php echo print_paises(); ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Cargo</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="cargo" name="cargo">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Período Inicio</label>
            </div>
            <div class="small-4 columns">
                <select name="mes_inicio" id="mes_inicio">
                    <?php echo print_meses(); ?>
                </select>
            </div>
            <div class="small-2 columns">
                <label class="right inline">Año</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="inicio" id="inicio" min="1901" value="2015">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Período Término</label>
            </div>
            <div class="small-4 columns">
                <select name="mes_termino" id="mes_termino">
                    <?php echo print_meses(); ?>
                </select>
            </div>
            <div class="small-2 columns">
                <label class="right inline">Año</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="termino" id="termino" min="1901" value="2015">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Funciones Principales</label>
            </div>
            <div class="small-10 columns">
                <textarea name="funciones" id="funciones" cols="30" rows="5"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Logros Obtenidos</label>
            </div>
            <div class="small-10 columns">
                <textarea name="logros" id="logros" cols="30" rows="5"></textarea>
            </div>
        </div>
        <a id="btn_add_exp" href="#" class="button expand">Agregar</a>
        <a id="btn_act_exp" href="#" class="button warning expand" style="display: none;" data-url="<?php echo site_url('reemplazo/editar');?>">Actualizar</a>
    </form>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!--  -->
<div id="modalReferencia" class="reveal-modal small" data-reveal aria-labelledby="refTitle" aria-hidden="true" role="dialog">
    <h2 id="refTitle">Agregar Referencia</h2>
    <form id="form_agregar_ref" action="<?php echo site_url('reemplazo/agregar_referencia');?>">
        <input type="hidden" id="id_ref" name="id_ref">
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Nombre Contacto</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="nombre_ref" name="nombre_ref">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Nombre Institución</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="institucion_ref" name="institucion_ref">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Cargo</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="cargo_ref" name="cargo_ref">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Relación Laboral</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="relacion_ref" name="relacion_ref">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Teléfono</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="telefono_ref" name="telefono_ref">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Correo Electrónico</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="email_ref" name="email_ref">
            </div>
        </div>
        <a id="btn_add_ref" href="#" class="button expand">Agregar</a>
        <a id="btn_act_ref" href="#" class="button warning expand" style="display: none;" data-url="<?php echo site_url('reemplazo/editar_referencia');?>">Actualizar</a>
    </form>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
<!--  -->
</div> <!-- contenido principal -->

</div>