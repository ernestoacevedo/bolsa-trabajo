          <div class="row" style="margin-top: 10px;">

            <div class="large-4 small-12 columns">

              <div class="panel <?php
                        if($this->session->userdata('logged_in')){
                            echo 'sidepanel';
                        }
            ?>">
            <?php
                            $user = $this->session->userdata('user_data');
                            if (!isset($user)) {
                            echo print_login();
                            }
                            else {
                            if($this->session->userdata('logged_in')){
                                if($this->session->userdata('rol')=='user'){
                                echo print_menu_user($this->session->userdata('nombre'));
                                }
                                else{
                                echo print_menu($this->session->userdata('nombre'));;
                                }

                            }
                            else{
                                echo print_login();
                            }
                            }
            ?>
        </div>
            </div>
            <div class="contenido large-8 columns">
            <h3>Cambiar Contraseña</h3>
            <form action="<?php echo site_url('recuperar/restablecer');?>" id="form_password">
            <div class="row">
                <div class="small-2 columns">
                    <label class="right inline">Código de Verificación</label>
                </div>
                <div class="small-10 columns">
                    <input type="text" name="codigo" id="codigo" value="<?php echo $codigo;?>" readonly>
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label class="right inline">Contraseña Nueva</label>
                </div>
                <div class="small-10 columns">
                    <input type="password" name="pass_nueva" id="pass_nueva">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label class="right inline">Confirmar Contraseña</label>
                </div>
                <div class="small-10 columns">
                    <input type="password" name="pass_conf" id="pass_conf">
                </div>
            </div>
              <a href="#" id="pass_btn" class="button">Cambiar Contraseña</a>
            </form>
            </div>
          </div>
