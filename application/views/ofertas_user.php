<div class="row" style="margin-top: 10px;">
    <div class="large-4 small-12 columns">
        <div class="panel sidepanel">
            <?php echo print_menu_user($this->session->userdata('nombre'));?>
        </div>
    </div>
    <div class="contenido large-8 columns">
        <h2>Listado de Ofertas</h2>
        <table id="tabla_ofertas" class="datatable" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Título
                </th>
                <th style="text-align: center;">
                    Fecha Publicación
                </th>
                <th style="text-align: center;">
                    Fecha Cierre
                </th>
                <th style="text-align: center;">
                    Ver
                </th>
            </thead>
            <tbody>
                <?php
                                                    foreach($ofertas as $oferta){
                                                    $estado = ($oferta['activa'] == false) ? 'Terminada' : 'Activa';
                                                    echo '<tr>';
                                                            echo '<td style="text-align: center;">'.$oferta['titulo'].'</td>';
                                                            echo '<td style="text-align: center;">'.$oferta['fecha_publicacion'].'</td>';
                                                            echo '<td style="text-align: center;">'.$oferta['fecha_cierre'].'</td>';
                                                            echo '<td style="text-align: center;"><a href="'.site_url('oferta/ver').'/'.$oferta['id'].'"><i class="fa fa-eye"></i></a></td>';
                                                    echo '</tr>';
                                                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
</div>