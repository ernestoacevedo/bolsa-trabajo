<div class="row" style="margin-top: 10px;">



    <div class="large-4 small-12 columns">

        <div class="panel <?php
                        if($this->session->userdata('logged_in')){
                            echo 'sidepanel';
                        }
            ?>">
            <?php
                            $user = $this->session->userdata('user_data');
                            if (!isset($user)) {
                            echo print_login();
                            }
                            else {
                                if($this->session->userdata('logged_in')){
                                    if($this->session->userdata('rol')=='user'){
                                    echo print_menu_user($this->session->userdata('nombre'));
                                    }
                                    else{
                                    echo print_menu($this->session->userdata('nombre'));;
                                    }

                                }
                                else{
                                    echo print_login();
                                }
                            }
            ?>
        </div>
    </div>

    <div class="contenido large-8 columns">
	    <!-- <img src="<?php echo base_url();?>/assets/img/banner_home.jpg"> -->
      <h2>ETAPAS DEL PROCESO DE SELECCIÓN DE ANTECEDENTES</h2>
        <ol id="reemp-list">
         <li><strong>PUBLICACIÓN DE LLAMADOS A SELECCIÓN.</strong><br>Constantemente se actualizarán publicaciones de cargos que sean llamados a selección en página web. El postulante registrado podrá revisar la descripción del cargo y podrá participar del proceso de selección de antecedentes sólo haciendo click en el botón POSTULAR.

El llamado a selección, podrá requerir que el postulante adjunte nueva documentación (por ejemplo, certificados para acreditar experiencia laboral), lo cual debe ser realizado al momento de la postulación a dicho cargo. Esta información estará disponible en descripción del cargo y bases del proceso.</li>

         <li><strong>FILTRO O EVALUACIÓN CURRÍCULAR.</strong><br>Se realiza evaluación y filtro de currículos de acuerdo a requisitos exigidos por perfil del cargo. Esto puede ser realizado por el Subdepto de selección o bien por una comisión establecida, dependiendo de los requisitos solicitados.</li>
         <li>
           <strong>EVALUACIÓN PSICOLABORAL. </strong><br>Psicólogo realiza evaluación de los candidatos de acuerdo a perfil requerido para el cargo, esta evaluación puede incluir la aplicación de pruebas, y puede ser realizada de manera individual o grupal. En esta etapa, también podría ser aplicada una prueba técnica o de conocimientos, información que será entregada en bases del proceso.
         </li>
         <li><strong>ENTREVISTAS DE VALORACIÓN GLOBAL.</strong><br>Comisión a cargo del proceso realiza entrevista con candidatos, de acuerdo a pauta de evaluación, se define orden de prioridad para la propuesta y se envía a Director para su resolución.
         </li>
        </ol>
    </div>
</div>



</div>
