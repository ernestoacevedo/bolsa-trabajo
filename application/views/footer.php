




        </div>
      </div>
      <footer>
		  <div class="footer-top-part">
		  	<div class="row">
		  		<div class="large-12 columns widget-content">
				<p>1 Norte 13 Oriente s/n, Talca | <a href="mailto:informaciones@hospitaldetalca.cl" target="_blank">informaciones@hospitaldetalca.cl</a> | Mesa Central (71)2209100 - (71) 2412700 </p></div>
		  	</div>
		  </div>
		  <div class="footer-bottom-part">
			  <div class="row">
			  	<div class="large-12 large-centered columns">
				  	<div class="row">
					  	<div class="large-5 columns widget-content">
						  	<ul class="contact-list">
							  	<li></li>
							  	<li></li>
							  	<li></li>
						  	</ul>
				  		</div>
				  		<div class="large-7 columns widget-content">
				  		</div>
				  	</div>
				  	<p>
Hospital de Talca 2015. Todos los derechos reservados. | Desarrollado por <a href="http://www.vao.cl" target="_blank">VAO Comunicaciones</a></p>
					<a href="http://www.gob.cl/" target="_blank" class="gob"><img class="alignnone size-full" src="<?php echo base_url();?>assets/img/logo-gob.jpg"></a>
			  	</div>
			  </div>
		  </div>
       </footer>
       <div id="modalRegistro" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
           <h2 id="modalTitle">Registrar Usuario</h2>
           <form id="form_registro" action="<?php echo site_url('usuario/registrar');?>" data-abide="ajax">
               <div class="row">
                   <div class="large-12 columns">
                       <div>
                           <label>RUT
                               <input id="rut_reg" name="rut_reg" type="text" placeholder="Ej: 12345789-0" required/>
                               <small class="error">Ingrese su RUT.</small>
                           </label>
                       </div>
                       <div>
                           <label>Nombres
                               <input id="nombres_reg" name="nombres_reg" type="text" required pattern="[a-zA-Z]+">
                               <small class="error">Ingrese sus nombres.</small>
                           </label>
                       </div>
                       <div>
                           <label>Apellidos
                               <input id="apellidos_reg" name="apellidos_reg" type="text" required pattern="[a-zA-Z]+">
                               <small class="error">Ingrese sus apellidos.</small>
                           </label>
                       </div>
                       <label>Correo electrónico
                           <input id="email_reg" name="email_reg" type="email" required>
                           <small class="error">Ingrese su correo electrónico.</small>
                       </label>
                       <div>
                           <label>Contraseña
                               <input id="pass_reg" name="pass_reg" type="password" required/>
                               <small class="error">Ingrese su contraseña.</small>
                           </label>
                       </div>
       <!--                     <div class="g-recaptcha" data-sitekey="6LcXcwcTAAAAAP8zRm0JgSOqaaglE7L3FpyQx0jP"></div> -->
                   </div>
               </div>
               <div class="row">
                   <div class="large-9 large-offset-3 columns">
                       <a href="#" id="reg_btn" class="button" type="submit">Registrar</a>
                       <a id="cancel_btn" href="#" class="button alert">Cancelar</a>
                   </div>
               </div>
           </form>
           <a class="close-reveal-modal" aria-label="Close">&#215;</a>
       </div>

       <div id="modalRecuperar" class="reveal-modal small" data-reveal aria-labelledby="mTitle" aria-hidden="true" role="dialog">
         <h2 id="mTitle">Recuperar Contraseña</h2>
         <a class="close-reveal-modal" aria-label="Close">&#215;</a>
         <form id="form_recuperar" action="<?php echo site_url('recuperar/generar_ticket');?>" method="POST">
             <div class="row">
                 <div class="large-12 columns">
                     <div>
                           <label>RUT
                               <input id="rut_rec" name="rut_rec" type="text" placeholder="Ej: 12345789-0" required/>
                           </label>
                       </div>
                 </div>
             </div>
             <div class="row">
                   <div class="large-9 large-offset-3 columns">
                       <a href="#" id="rec_btn" class="button" type="submit">Enviar</a>
                       <a id="cancel_rec_btn" href="#" class="button alert">Cancelar</a>
                   </div>
               </div>
         </form>
       </div>

       <div id="modalDatos" class="reveal-modal small" data-reveal aria-labelledby="dTitle" aria-hidden="true" role="dialog">
         <h2 id="dTitle"></h2>
         <a class="close-reveal-modal" aria-label="Close">&#215;</a>
         <div class="row">
             <div class="small-6 columns">
                 <br>
                 <a id="cv_link" href="#" target="_blank">Ver Currículum</a>
             </div>
         </div>
         <br>
         <table id="tabla_datos">
           <thead>
             <th style="text-align: center;">
                 Tipo
             </th>
             <th style="text-align: center;">
                 Ver
             </th>
           </thead>
           <tbody>

           </tbody>
         </table>
       </div> <!-- modalDatos -->
<script src="<?php echo base_url();?>assets/js/jquery.js"></script>
<script src="<?php echo base_url();?>assets/js/foundation.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.noty.packaged.min.js"></script>
<script src="<?php echo base_url();?>assets/js/noty.theme.relax.js"></script>
<script src="<?php echo base_url();?>assets/js/foundation-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/js/moment.js"></script>
<script src="<?php echo base_url();?>assets/js/cool-share.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery-te-1.4.0.min.js"></script>
<script src="<?php echo base_url();?>assets/js/waypoints.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.qtip.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.filter_input.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63482626-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
