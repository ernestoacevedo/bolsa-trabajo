<div class="row" style="margin-top: 10px;">
    <div class="large-4 small-12 columns">
        <div class="panel sidepanel">
            <?php echo print_menu_user($this->session->userdata('nombre')); ?>
        </div>
    </div>
    <div class="contenido large-8 columns">
        <h3><i class="fa fa-archive"></i> Administración de Documentos</h3>
        <a id="btn_subir" href="#" class="button small"><i class="fa fa-plus"></i> Subir Archivo</a>
        <table id="tabla_documentos" class="datatable" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Tipo
                </th>
                <th style="text-align: center;">
                    Ver
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                                                    foreach($documentos as $documento){
                                                                                    echo '<tr>';
                                                                                                    echo '<td style="text-align: center;">'.$documento['tipo_documento'].'</td>';
                                                                                                    echo '<td style="text-align: center;"><a target="_blank" href="'.base_url().'documentos/'.$documento['ruta_documento'].'"><i class="fa fa-eye"></i></a> </td>';
                                                                                                    echo '<td style="text-align: center;"><a href="'.site_url('documento/eliminar').'/'.$documento['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                                                    echo '</tr>';
                                                                                    }
                ?>
            </tbody>
        </table>
    </div>
    <div id="modalSubir" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
        <h2 id="modalTitle">Subir Archivo</h2>
        <form id="form_documento" action="<?php echo site_url('documento/subir');?>"  method="post" accept-charset="utf-8" enctype="multipart/form-data" >
            <div class="row">
                <div class="small-2 columns">
                    <label for="tipo_documento" class="right inline">Tipo de Documento</label>
                </div>
                <div class="small-10 columns">
                    <select name="tipo_documento" id="tipo_documento">
                        <option value="Enseñanza Media">Certificado de Enseñanza Media</option>
                        <option value="Enseñanza Superior">Certificado de Enseñanza Superior</option>
                        <option value="Certificado de Título">Certificado de Título</option>
                        <option value="Otro">Otro Tipo de Certificado</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="file" class="right inline">Archivo</label>
                </div>
                <div class="small-10 columns">
                    <input type="file" name="file" id="file">
                    <small>Tamaño máximo 20MB.</small>
                </div>
            </div>
            <div class="row">
              <div class="small-3 centered columns">
                <!-- <a href="#" id="subir_archivo" class="button centered">Subir</a> -->
                <input type="submit" id="subir_archivo" class="button centered" value="Subir">
              </div>
            </div>

        </form>
        <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div> <!-- modalSubir -->
</div>
</div>
