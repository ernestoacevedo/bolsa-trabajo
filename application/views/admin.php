<div id="principal" class="row" style="margin-top: 10px;">
    <div class="large-4 small-12 columns">
        <div class="panel sidepanel">
            <?php echo print_menu($this->session->userdata('nombre')); ?>
        </div>
    </div>
    <div class="contenido large-8 columns">
        <div class="row">
            <div class="large-4 small-6 columns">
                <div class="panel">
                    <strong class="counter"><?php echo $total_usuarios;?></strong> Usuarios Registrados
                </div>
            </div>
            <div class="large-4 small-6 columns">
                <div class="panel">
                    <strong class="counter"><?php echo $total_ofertas;?></strong> Ofertas Publicadas
                </div>
            </div>
            <div class="large-4 small-6 columns">
                <div class="panel">
                    <strong class="counter"><?php echo $total_postulaciones;?></strong> Postulaciones
                </div>
            </div>
        </div>
        <div class="row">
            <div class="large-6 columns">
                <div class="panel">
                    <h4>Postulaciones Recientes</h4>
                    <hr>
                    <ul>
                        <?php
                                            foreach ($recientes as $reciente) {
                                                echo '<li><a href="'.site_url('cv/topdf').'/'.$reciente['rut'].'">'.$reciente['nombres'].' '.$reciente['apellidos'].'</a> en <a href="'.site_url('oferta/ver').'/'.$reciente['id_oferta'].'">'.$reciente['titulo'].'</a></li>';
                                            }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="large-6 columns">
                <div id="ofertas" class="panel">
                    <h4>Ofertas Populares</h4>
                    <hr>
                    <ul style="list-style: none;">
                        <?php
                                        foreach ($populares as $oferta) {
                                            echo '<li> '.$oferta['vistas'].' <a href="'.site_url('oferta/ver').'/'.$oferta['id_oferta'].'">'.$oferta['titulo'].'</a>'.'</li>';
                                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php
                    $mensaje = $this->session->flashdata('msg');
                    if($mensaje!=''){
                            echo    '<div data-alert class="alert-box '.$this->session->flashdata('type').'">
                                    '.$mensaje.'
                                    <a href="#" class="close">&times;</a>
                                </div>';
                    }
        ?>
    </div>
</div>
</div>
