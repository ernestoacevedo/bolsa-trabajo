<div class="row" style="margin-top: 10px;">



    <div class="large-4 small-12 columns">

        <div class="panel <?php
                        if($this->session->userdata('logged_in')){
                            echo 'sidepanel';
                        }
            ?>">
            <?php
                            $user = $this->session->userdata('user_data');
                            if (!isset($user)) {
                            echo print_login();
                            }
                            else {
                                if($this->session->userdata('logged_in')){
                                    if($this->session->userdata('rol')=='user'){
                                    echo print_menu_user($this->session->userdata('nombre'));
                                    }
                                    else{
                                    echo print_menu($this->session->userdata('nombre'));;
                                    }

                                }
                                else{
                                    echo print_login();
                                }
                            }
            ?>
        </div>
    </div>


    <div class="contenido large-8 columns">
	   <h2>PARA POSTULAR AL HOSPITAL REGIONAL DE TALCA DEBERÁ CUMPLIR CON LOS SIGUIENTES PASOS:</h2>
       <ol id="reemp-list">
        <li>Los interesados deberán <a id="link_registro" href="#">REGISTRAR</a> cuenta de usuario, completando todos los campos de información solicitados, posteriormente llevar sus datos en Currículum Vitae y adjuntando la siguiente documentación obligatoria:
          <ul id="lista-interna">
            <li>Para cargos <u>administrativos y auxiliares</u>, debe adjuntar licencia de enseñanza media.</li>
            <li>Para cargos <u>técnicos o profesionales del área clínica</u>, adjuntar certificado de título y registro de acreditación de prestadores individuales de salud.</li>
            <li>Para cargos <u>profesionales no clínicos</u>, adjuntar certificado de título.</li>
          </ul>
<br>
El registro facilita la postulación de los candidatos ya que podrán recibir notificaciones de los llamados a selección vigentes y reducir el tiempo de postulación, ya que esta será realizada con sólo un click.
        </li>
        <li>Además los antecedentes recepcionados podrán ser considerados en base de datos de reemplazo, siempre y cuando el postulante señale estar disponible para la realización de estos, al completar campos de información.
        Aquel postulante que no presente documentación obligatoria y/o sus datos se encuentren incompletos, no serán considerado para procesos de selección de cargos o reemplazos.</li>
       </ol>
    </div>

</div>

</div>
