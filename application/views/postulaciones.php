<div class="row" style="margin-top: 10px;">
    <div class="large-4 small-12 columns">
        <div class="panel sidepanel">
            <?php echo print_menu($this->session->userdata('nombre'));?>
        </div>
    </div>
    <div class="large-8 columns">
        <h2>Listado de Postulaciones</h2>
        <table id="tabla_postulaciones" class="datatable" border="1" style="table">
            <thead>
                <th style="text-align: center; width: 40px;">
                    Rut
                </th>
                <th style="text-align: center; width: 40px;">
                    Postulante
                </th>
                <th style="text-align: center; width: 30px;">
                    Oferta
                </th>
                <th style="text-align: center; width: 30px;">
                    Fecha de Postulación
                </th>
                <th style="text-align: center; width: 30px;">
                    Etapa
                </th>
                <th style="text-align: center; width: 30px;">
                    Estado
                </th>
                <th style="text-align: center; width: 30px;">
                  Disponible para Reemplazos
                </th>
                <th style="text-align: center; width: 30px;">
                  Editar
                </th>
                <th style="text-align: center; width: 30px;">
                  Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                    foreach($postulaciones as $postulacion){
                                                    $reemp = ($postulacion['reemplazo']=='1') ? 'Sí' : 'No';
                                                    $estado = ($postulacion['estado'] == 0) ? 'Terminada' : 'Activa';
                                                    echo '<tr>';
                                                            echo '<td class="rut" style="text-align: center;">'.$postulacion['rut_postulante'].'</td>';
                                                            echo '<td style="text-align: center;"><a class="user_link" data-rut="'.$postulacion['rut_postulante'].'" href="#" data-cv="'.site_url('cv/topdf').'/'.$postulacion['rut_postulante'].'" data-url="'.site_url('documento/documentos_usuario').'/'.$postulacion['rut_postulante'].'">'.$postulacion['nombres'].' '.$postulacion['apellidos'].'</a></td>';
                                                            echo '<td style="text-align: center;">'.$postulacion['titulo'].'</td>';
                                                            echo '<td style="text-align: center;">'.$postulacion['fecha'].'</td>';
                                                            echo '<td style="text-align: center;">'.obtenerEtapa($postulacion['etapa']-1).'</td>';
                                                            echo '<td style="text-align: center;">'.$estado.'</td>';
                                                            echo '<td style="text-align: center;">'.$reemp.'</td>';
                                                            echo '<td style="text-align: center;"><a href="#" data-url="'.site_url('postulacion/obtener').'/'.$postulacion['id'].'" class="editar editarpost"><i class="fa fa-pencil fa-lg"></i></a></td>';
                                                            echo '<td style="text-align: center;"><a href="'.site_url('postulacion/eliminar').'/'.$postulacion['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                    echo '</tr>';
                                                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
<div id="modalEditar" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <h2 id="modalTitle">Editar Postulación</h2>
    <form id="form_editar_postulacion" action="<?php echo site_url('postulacion/editar');?>">
        <input type="hidden" id="id_postulacion" name="id_postulacion">
        <input type="hidden" id="id_oferta" name="id_oferta">
        <input type="hidden" id="etapa_actual" name="etapa_actual">
        <input type="hidden" id="nombres" name="nombres">
        <input type="hidden" id="email" name="email">
        <div class="row">
            <div class="small-2 columns">
                <label for="etapa" class="right inline">Etapa</label>
            </div>
            <div class="small-10 columns">
                <select name="etapa" id="etapa">
                    <option value="1">Recepción de Currículum</option>
                    <option value="2">Entrevista Psicológica</option>
                    <option value="3">Entrevista Personal</option>
                    <option value="4">Seleccionado</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Activa</label>
            </div>
            <div class="small-10 columns">
                <div class="switch">
                    <input id="estado" name="estado" type="checkbox" value="0">
                    <label for="estado"></label>
                </div>
            </div>
        </div>
        <a id="btn_act_post" href="#" class="button expand warning">Actualizar</a>
    </form>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
</div>
