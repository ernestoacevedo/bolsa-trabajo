          <div class="row" style="margin-top: 10px;">
		  	<div class="large-4 small-12 columns">

        <div class="panel <?php
                        if($this->session->userdata('logged_in')){
                            echo 'sidepanel';
                        }
            ?>">
            <?php
                            $user = $this->session->userdata('user_data');
                            if (!isset($user)) {
                            echo print_login();
                            }
                            else {
                            if($this->session->userdata('logged_in')){
                                if($this->session->userdata('rol')=='user'){
                                echo print_menu_user($this->session->userdata('nombre'));
                                }
                                else{
                                echo print_menu($this->session->userdata('nombre'));;
                                }

                            }
                            else{
                                echo print_login();
                            }
                            }
            ?>
        </div>
    </div>
            <div id="panel_oferta" class="contenido large-8 columns" data-id-oferta="<?php echo $oferta['id'];?>" data-url="<?php echo site_url('postulacion/nueva');?>">
              <?php
              	echo '<h1>'.$oferta['titulo'].'</h1>';
              	echo '<div class="row">
	                  <div class="small-3 large-2 columns">
	                    <strong>Grado:</strong>
	                  </div>
	                  <div class="small-9 large-10 columns">'.$oferta['grado'].'</div>
	                </div><br>';
              	echo '<div class="row">
	                  <div class="small-3 large-2 columns">
	                    <strong>Descripción:</strong>
	                  </div>
	                  <div class="small-9 large-10 columns">'.$oferta['descripcion'];
	            if($oferta['ruta_descripcion']!=''){
	            	echo '<br><a href="'.base_url().'doc_ofertas/'.$oferta['ruta_descripcion'].'">Descargar Bases del Proceso</a></div></div><br>';
	            }
	            else{
	                echo '</div></div><br>';
	            }
	           	echo '<div class="row">
	                  <div class="small-3 large-2 columns">
	                    <strong>Remuneración:</strong>
	                  </div>
	                  <div class="small-3 large-4 columns">'.$oferta['remuneracion'].'</div>
	                  <div class="small-3 large-2 columns">
	                    <strong>Fecha de Cierre:</strong>
	                  </div>
	                  <div class="small-3 large-4 columns">'.$oferta['fecha_cierre'].'</div>
	                </div><br>';
	            echo '<div class="row">
	                  <div class="small-3 large-2 columns">
	                    <strong>Horas:</strong>
	                  </div>
	                  <div class="small-3 large-4 columns">'.$oferta['horas'].'</div>
	                  <div class="small-3 large-2 columns">
	                    <strong>Puestos:</strong>
	                  </div>
	                  <div class="small-3 large-4 columns">'.$oferta['puestos'].'</div>
	                </div><br>';
	            echo '<div class="row">
	                  <div class="small-3 large-2 columns">
	                    <strong>Jornada:</strong>
	                  </div>
	                  <div class="small-9 large-10 columns">'.$oferta['jornada'].'</div>
	                </div><br>';
	            echo '<div class="row">
	                  <div class="small-3 large-2 columns">
	                    <strong>Tipo de Contrato:</strong>
	                  </div>
	                  <div class="small-9 large-10 columns">'.$oferta['tipo_contrato'].'</div>
	                </div><br>';
	            echo '<div class="row">
	                  <div class="small-3 large-2 columns">
	                    <strong>Características:</strong>
	                  </div>
	                  <div class="small-9 large-10 columns">'.$oferta['caracteristicas'].'</div>
	                </div><br>';
	            echo '<div class="row">
	                  <div class="small-3 large-2 columns">
	                    <strong>Deseables:</strong>
	                  </div>
	                  <div class="small-9 large-10 columns">'.$oferta['deseables'].'</div>
	                </div><br>';
	            echo '<div class="row">
	                  <div class="small-3 large-2 columns">
	                    <strong>Antecedentes:</strong>
	                  </div>
	                  <div class="small-9 large-10 columns">'.$oferta['antecedentes'].'</div>
	                </div><br>';
	            echo '<div class="row">
	                  <div class="small-3 large-2 columns">
	                    <strong>Legal:</strong>
	                  </div>
	                  <div class="small-9 large-10 columns">'.$oferta['legal'].'</div>
	                </div><br>';
	            echo '<div class="row">
	                  <div class="small-3 large-2 columns">
	                    <strong>Entrevista Psicológica:</strong>
	                  </div>
	                  <div class="small-9 large-10 columns">'.$oferta['entrevista_psico'].'</div>
	                </div><br>';
	            echo '<div class="row">
	                  <div class="small-3 large-2 columns">
	                    <strong>Entrevista Personal:</strong>
	                  </div>
	                  <div class="small-9 large-10 columns">'.$oferta['entrevista_pers'].'</div>
	                </div><br>';
	            echo '<div class="row">
	                  <div class="small-3 large-2 columns">
	                    <strong>Selección Final:</strong>
	                  </div>
	                  <div class="small-9 large-10 columns">'.$oferta['seleccion_final'].'</div>
	                </div><br>';
	            echo '<div class="row">
	                  <div class="small-3 large-2 columns">
	                    <strong>Correo de Contacto:</strong>
	                  </div>
	                  <div class="small-9 large-10 columns">'.$oferta['email_depto'].'</div>
	                </div><br>';
	            echo '<div class="row">
	                  <div class="small-3 large-2 columns">
	                    <strong>Número de Contacto:</strong>
	                  </div>
	                  <div class="small-9 large-10 columns">'.$oferta['num_depto'].'</div>
	                </div><br>';
              ?>
              <div class="row">
              	<div class="large-12 small-12 columns">
              		<?php
              			if($oferta['activa']=='1'){
                      $hoy = date('d/m/Y');
                      $d1 = DateTime::createFromFormat('d/m/Y',$hoy);
                      $d2 = DateTime::createFromFormat('d/m/Y',$oferta['fecha_cierre']);
                      if($d1<=$d2){
                        echo '<a href="#" class="button large success" data-reveal-id="modalBases" style="margin-right: 30px;" data-fecha="" data-url="'.site_url('oferta/ver/'.$oferta['id']).'">Postular</a>';
                      }
              			}
              		?>
              		 <a id="avance_btn" href="<?php echo site_url('oferta/avance/'.$oferta['id']);?>" class="button large" style="margin-right: 30px;">Ver Avance</a>
					<span class="socialShare"></span>
              	</div>
              </div>
            </div>

            <div id="modalBases" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
              <br>
              <br>
              <p align="justify">Declaro haber leído bases del proceso y estar en conocimiento de requisitos del cargo al cual postulo y
                 etapas del proceso de selección de antecedentes</p>
              <div class="row">
                <div class="small-8 large-6 small-centered large-centered columns">
                  <a id="postular_btn" href="#" class="button success small">Aceptar</a>
                  <a id="cancelar_bases_btn" href="" class="button alert small">Cancelar</a>
                </div>
              </div>

              <a class="close-reveal-modal" aria-label="Close">&#215;</a>
            </div>
          </div>
