<div class="row" style="margin-top: 10px;">

    <div class="large-4 small-12 columns">

        <div class="panel sidepanel">
            <?php echo print_menu($this->session->userdata('nombre'));?>
        </div>
    </div>

    <div class="large-8 columns">
        <h3>Publicar Oferta</h3>
        <form id="form_publicar" action="<?php echo site_url('admin/publicar');?>" accept-charset="utf-8" enctype="multipart/form-data" method="POST">
            <fieldset>
            <legend>Datos de la Oferta</legend>
            <div class="row">
                <div class="small-2 columns">
                    <label for="titulo" class="right inline">Título</label>
                </div>
                <div class="small-10 columns">
                    <input type="text" name="titulo" id="titulo" required>
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="grado" class="right inline">Grado</label>
                </div>
                <div class="small-10 columns">
                    <input type="text" name="grado" id="grado" required>
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="descripcion" class="right inline">Descripción</label>
                </div>
                <div class="small-10 columns">
                    <textarea name="descripcion" id="descripcion" cols="30" rows="6" style="resize: none;"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="horas" class="right inline">Horas</label>
                </div>
                <div class="small-4 columns">
                    <input type="number" name="horas" id="horas" min="1" value="">
                </div>
                <div class="small-2 columns">
                    <label for="puestos" class="right inline">Puestos</label>
                </div>
                <div class="small-4 columns">
                    <input type="number" name="puestos" id="puestos" min="1" value="">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="remuneracion" class="right inline">Remuneración</label>
                </div>
                <div class="small-4 columns">
                    <input type="number" name="remuneracion" id="remuneracion" min="0">
                </div>
                <div class="small-2 columns">
                    <label for="fecha_cierre" class="right inline">Fecha de Cierre</label>
                </div>
                <div class="small-4 columns">
                    <input type="text" name="fecha_cierre" id="fecha_cierre" data-date-format="dd/mm/yyyy" required>
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="jornada" class="right inline">Jornada</label>
                </div>
                <div class="small-10 columns">
                    <input type="text" name="jornada" id="jornada">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="tipo_contrato" class="right inline">Tipo de Contrato</label>
                </div>
                <div class="small-10 columns">
                    <input type="text" name="tipo_contrato" id="tipo_contrato">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="caracteristicas" class="right inline">Características</label>
                </div>
                <div class="small-10 columns">
                    <textarea name="caracteristicas" id="caracteristicas" cols="30" rows="3" style="resize: none;"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="deseables" class="right inline">Deseables</label>
                </div>
                <div class="small-10 columns">
                    <textarea name="deseables" id="deseables" cols="30" rows="3" style="resize: none;"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="legal" class="right inline">Legal</label>
                </div>
                <div class="small-10 columns">
                    <textarea name="legal" id="legal" cols="30" rows="2" style="resize: none;"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="antecedentes" class="right inline">Antecedentes</label>
                </div>
                <div class="small-10 columns">
                    <textarea name="antecedentes" id="antecedentes" cols="30" rows="3" style="resize: none;"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="entrevista_psico" class="right inline">Entrevista Psicológica</label>
                </div>
                <div class="small-10 columns">
                    <textarea name="entrevista_psico" id="entrevista_psico" cols="30" rows="2" style="resize: none;"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="entrevista_pers" class="right inline">Entrevista Personal</label>
                </div>
                <div class="small-10 columns">
                    <textarea name="entrevista_pers" id="entrevista_pers" cols="30" rows="2" style="resize: none;"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="seleccion_final" class="right inline">Selección Final</label>
                </div>
                <div class="small-10 columns">
                    <input type="text" name="seleccion_final" id="seleccion_final">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="file" class="right inline">Documento de Descripción</label>
                </div>
                <div class="small-10 columns">
                    <input type="file" name="file" id="file">
                    <p class="small-text-left" class="text_file">Tamaño máximo 20MB.</p>
                </div>
            </div>
            </fieldset>
            <fieldset>
            <legend>Datos del Departamento</legend>
            <div class="row">
                <div class="small-2 columns">
                    <label for="email_depto" class="right inline">Correo de Contacto</label>
                </div>
                <div class="small-10 columns">
                    <input type="email" name="email_depto" id="email_depto">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="num_depto" class="right inline">Número de Contacto</label>
                </div>
                <div class="small-10 columns">
                    <input type="text" name="num_depto" id="num_depto">
                </div>
            </div>
            </fieldset>
            <fieldset>
            <legend>Ajustes de publicación</legend>
            <div class="row">
                <div class="small-2 columns">
                    <label for="activa" class="right inline">Estado</label>
                </div>
                <div class="small-10 columns">
                    <select name="activa" id="activa">
                        <option value="1">Activa</option>
                        <option value="0">Inactiva</option>
                        <option value="2">Terminada</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="titulo" class="right inline">Visible</label>
                </div>
                <div class="small-10 columns">
                    <div class="switch">
                        <input id="estado" name="estado" type="checkbox" checked>
                        <label for="estado"></label>
                    </div>
                </div>
            </div>
            </fieldset>
            <input id="fecha_publicacion" name="fecha_publicacion" type="hidden">

            <a id="publicar_btn" href="#" type="submit" class="button expand"> Publicar </a>
        </form>
    </div>
</div>
</div>
