          <div class="row" style="margin-top: 10px;">
     
            <div class="large-4 small-12 columns">
     
              <div class="panel sidepanel">
              	<?php if($this->session->userdata('rol')=='admin'){
              			echo print_menu($this->session->userdata('nombre'));
              		}
              		else{
              			echo print_menu_user($this->session->userdata('nombre'));
              		}
              	?>
              </div>
            </div>
            <div class="contenido large-8 columns">
            <h3>Cambiar Contraseña</h3>
            <form action="<?php echo site_url('login/cambio_password');?>" id="form_password">
            <input type="hidden" id="rut" name="rut" value="<?php echo $this->session->userdata('rut');?>">
            <div class="row">
                <div class="small-2 columns">
                    <label class="right inline">Contraseña Actual</label>
                </div>
                <div class="small-10 columns">
                    <input type="password" name="pass_actual" id="pass_actual">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label class="right inline">Contraseña Nueva</label>
                </div>
                <div class="small-10 columns">
                    <input type="password" name="pass_nueva" id="pass_nueva">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label class="right inline">Confirmar Contraseña</label>
                </div>
                <div class="small-10 columns">
                    <input type="password" name="pass_conf" id="pass_conf">
                </div>
            </div>
              <a href="#" id="pass_btn" class="button">Cambiar Contraseña</a>
            </form>
            </div>
          </div>