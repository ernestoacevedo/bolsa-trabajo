<div class="row" style="margin-top: 10px;">

    <div class="large-4 small-12 columns">

        <div class="panel sidepanel">
            <?php echo print_menu_user($this->session->userdata('nombre')); ?>
        </div>
    </div>

    <div class="contenido large-8 columns">
        <ul class="breadcrumbs">
          <li class="current"><a href="#">1. Información Personal</a></li>
          <li class="unavailable"><a href="#">2. Información Académica</a></li>
          <li class="unavailable"><a href="#">3. Antecedentes Laborales</a></li>
        </ul>
        <h3><i class="fa fa-user"></i> Información Personal</h3>
        <form id="form_info_personal" action="<?php echo site_url('usuario/editar');?>">
            <div class="row">
                <div class="small-2 columns">
                    <label for="nombres" class="right inline">Nombres</label>
                </div>
                <div class="small-10 columns">
                    <input type="text" name="nombres" id="nombres" value="<?php echo $usuario['nombres'];?>">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="apellidos" class="right inline">Apellidos</label>
                </div>
                <div class="small-10 columns">
                    <input type="text" name="apellidos" id="apellidos" value="<?php echo $usuario['apellidos'];?>">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="ciudad" class="right inline">Ciudad</label>
                </div>
                <div class="small-10 columns">
                    <input type="text" name="ciudad" id="ciudad" value="<?php echo $usuario['ciudad'];?>">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="telefono" class="right inline">Teléfono Fijo</label>
                </div>
                <div class="small-4 columns">
                    <input type="text" name="telefono" id="telefono" value="<?php echo $usuario['telefono'];?>">
                </div>
                <div class="small-2 columns">
                    <label for="celular" class="right inline">Teléfono Móvil</label>
                </div>
                <div class="small-4 columns">
                    <input type="text" name="celular" id="celular" value="<?php echo $usuario['celular'];?>">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="email" class="right inline">Correo electrónico</label>
                </div>
                <div class="small-10 columns">
                    <input type="email" name="email" id="email" value="<?php echo $usuario['email'];?>">
                </div>
            </div>
            <div class="row">
                <div class="large-8 large-offset-4 columns">
                    <a href="<?php echo site_url('dashboard');?>" id="cancelar" class="button alert"> Cancelar </a>
                    <a id="actualizar_info_btn" href="#" class="button"> Siguiente </a>
                </div>
            </div>

        </form>
    </div>
</div>
</div>