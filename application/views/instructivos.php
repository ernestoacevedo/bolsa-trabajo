<div class="row" style="margin-top: 10px;">
    <div class="large-4 small-12 columns">
        <div class="panel sidepanel">
            <?php echo print_menu($this->session->userdata('nombre')); ?>
        </div>
    </div>
    <div class="contenido large-8 columns">
        <h3><i class="fa fa-files-o"></i> Instructivos</h3>
        <a id="btn_subir" href="#" class="button small"><i class="fa fa-plus"></i> Subir Archivo</a>
        <table id="tabla_documentos" class="datatable" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Oferta
                </th>
                <th style="text-align: center;">
                    Ver
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                                                    foreach($instructivos as $instructivo){
                                                                                    echo '<tr>';
                                                                                                    echo '<td style="text-align: center;">'.$instructivo['titulo_oferta'].'</td>';
                                                                                                    echo '<td style="text-align: center;"><a href="'.base_url().'instructivos/'.$instructivo['ruta_documento'].'"><i class="fa fa-eye"></i></a> </td>';
                                                                                                    echo '<td style="text-align: center;"><a href="'.site_url('instructivo/eliminar').'/'.$instructivo['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                                                    echo '</tr>';
                                                                                    }
                ?>
            </tbody>
        </table>
    </div>
    <div id="modalSubir" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
        <h2 id="modalTitle">Subir Archivo</h2>
        <form id="form_documento" action="<?php echo site_url('instructivo/subir');?>"  method="post" accept-charset="utf-8" enctype="multipart/form-data" >
            <div class="row">
                <div class="small-2 columns">
                    <label for="id_oferta" class="right inline">Oferta</label>
                </div>
                <div class="small-10 columns">
                    <select name="id_oferta" id="id_oferta">
                        <option value="">Seleccione una oferta</option>
                        <?php
                            foreach($ofertas as $oferta){
                                echo '<option value="'.$oferta['id'].'">'.$oferta['titulo'].'</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="file" class="right inline">Archivo</label>
                </div>
                <div class="small-10 columns">
                    <input type="file" name="file" id="file">
                    <p class="small-text-left" class="text_file">Tamaño máximo 20MB.</p>
                </div>
            </div>
            <div class="row">
              <div class="small-3 centered columns">
                <!-- <a href="#" id="subir_archivo" class="button centered">Subir</a> -->
                <input type="submit" id="subir_archivo" class="button centered" value="Subir">
              </div>
            </div>

        </form>
        <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>
</div>
</div>
