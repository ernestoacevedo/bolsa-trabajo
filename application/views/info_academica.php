<div class="row" style="margin-top: 10px;">
    <div class="large-4 small-12 columns">
        <div class="panel sidepanel">
            <?php echo print_menu_user($this->session->userdata('nombre'));?>
        </div>
    </div>
    <div class="contenido large-8 columns">
        <ul class="breadcrumbs">
            <li><a href="<?php echo site_url('dashboard/personal');?>">1. Información Personal</a></li>
            <li class="current"><a href="#">2. Información Académica</a></li>
            <li class="unavailable"><a href="#">3. Antecedentes Laborales</a></li>
        </ul>
        <h3><i class="fa fa-graduation-cap"></i> Información Académica</h3>
        <a id="add_med" href="#" class="button small"><i class="fa fa-plus"></i> Enseñanza Media</a>
        <a id="add_sup" href="#" class="button small"><i class="fa fa-plus"></i> Educación Superior</a>
        <a id="add_esp" href="#" class="button small"><i class="fa fa-plus"></i> Especialización</a>
        <a id="add_cap" href="#" class="button small"><i class="fa fa-plus"></i> Capacitación</a>
        <h4> Enseñanza Media</h4>
        <table id="tabla_med" class="" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Nombre
                </th>
                <th style="text-align: center;">
                    Tipo
                </th>
                <th style="text-align: center;">
                    País
                </th>
                <th style="text-align: center;">
                    Año Inicio
                </th>
                <th style="text-align: center;">
                    Año Término
                </th>
                <th style="text-align: center;">
                    Editar
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                                                                foreach($info_media as $institucion){
                                                                                                    echo '<tr>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['nombre'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['tipo'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['pais'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['inicio'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['termino'].'</td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="#" data-url="'.site_url('antecedente/obtener').'/'.$institucion['id'].'" class="editarmedia editar"><i class="fa fa-pencil fa-lg"></i></a></td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="'.site_url('antecedente/eliminar').'/'.$institucion['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                                                                    echo '</tr>';
                                                                                                }
                ?>
            </tbody>
        </table>
        <h4> Educación Superior</h4>
        <table id="tabla_sup" class="" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Nombre
                </th>
                <th style="text-align: center;">
                    Tipo
                </th>
                <th style="text-align: center;">
                    Carrera
                </th>
                <th style="text-align: center;">
                    País
                </th>
                <th style="text-align: center;">
                    Año Inicio
                </th>
                <th style="text-align: center;">
                    Año Término
                </th>
                <th style="text-align: center;">
                    Situación
                </th>
                <th style="text-align: center;">
                    Grado Académico
                </th>
                <th style="text-align: center;">
                    Editar
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                                                                foreach($info_superior as $institucion){
                                                                                                    echo '<tr>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['nombre'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['tipo'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['carrera'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['pais'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['inicio'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['termino'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['situacion'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$institucion['grado'].'</td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="#" data-url="'.site_url('antecedente/obtener').'/'.$institucion['id'].'" class="editarsup editar"><i class="fa fa-pencil fa-lg"></i></a></td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="'.site_url('antecedente/eliminar').'/'.$institucion['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                                                                    echo '</tr>';
                                                                                                }
                ?>
            </tbody>
        </table>
        <h4> Estudios de Especialización</h4>
        <table id="tabla_cap" class="" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Nombre
                </th>
                <th style="text-align: center;">
                    Institución
                </th>
                <th style="text-align: center;">
                    Tipo
                </th>
                <th style="text-align: center;">
                    Inicio
                </th>
                <th style="text-align: center;">
                    Término
                </th>
                <th style="text-align: center;">
                    Editar
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                                                                foreach($info_especializaciones as $esp){
                                                                                                    echo '<tr>';
                                                                                                                        echo '<td style="text-align: center;">'.$esp['nombre'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$esp['institucion'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$esp['tipo_esp'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$esp['mes_inicio'].'/'.$esp['inicio'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$esp['mes_termino'].'/'.$esp['termino'].'</td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="#" data-url="'.site_url('antecedente/obtener').'/'.$esp['id'].'" class="editaresp editar"><i class="fa fa-pencil fa-lg"></i></a></td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="'.site_url('antecedente/eliminar').'/'.$esp['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                                                                    echo '</tr>';
                                                                                                }
                ?>
            </tbody>
        </table>
        <h4> Capacitaciones</h4>
        <table id="tabla_cap" class="" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Capacitación
                </th>
                <th style="text-align: center;">
                    Institución
                </th>
                <th style="text-align: center;">
                    Inicio
                </th>
                <th style="text-align: center;">
                    Término
                </th>
                <th style="text-align: center;">
                    Horas
                </th>
                <th style="text-align: center;">
                    Editar
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                                                                foreach($info_capacitaciones as $capacitacion){
                                                                                                    echo '<tr>';
                                                                                                                        echo '<td style="text-align: center;">'.$capacitacion['nombre'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$capacitacion['institucion'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$capacitacion['mes_inicio'].'/'.$capacitacion['inicio'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$capacitacion['mes_termino'].'/'.$capacitacion['termino'].'</td>';
                                                                                                                        echo '<td style="text-align: center;">'.$capacitacion['horas'].'</td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="#" data-url="'.site_url('antecedente/obtener').'/'.$capacitacion['id'].'" class="editarcap editar"><i class="fa fa-pencil fa-lg"></i></a></td>';
                                                                                                                        echo '<td style="text-align: center;"><a href="'.site_url('antecedente/eliminar').'/'.$capacitacion['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                                                                    echo '</tr>';
                                                                                                }
                ?>
            </tbody>
        </table>
        <div class="row">
            <div class="small-6 large-centered columns">
                <a href="<?php echo site_url('dashboard/personal');?>" class="button warning"> Atrás </a>
                <a href="<?php echo site_url('dashboard/laboral');?>" class="button"> Siguiente </a>
            </div>
        </div>
    </div>

</div>
<div id="modalMedia" class="reveal-modal small" data-reveal aria-labelledby="medTitle" aria-hidden="true" role="dialog">
    <h2 id="medTitle">Agregar Establecimiento</h2>
    <form id="form_agregar_media" action="<?php echo site_url('antecedente/agregar_media');?>">
        <input id="id_med" name="id_med" type="hidden">
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Nombre</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="institucion_med" name="institucion_med">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Tipo</label>
            </div>
            <div class="small-10 columns">
                <select name="tipo_med" id="tipo_med">
                    <option value="Científico-Humanista">Científico Humanista</option>
                    <option value="Técnico Profesional">Técnico Profesional</option>
                    <option value="Laboral">Laboral</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Título Profesional</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="titulo_med" name="titulo_med" placeholder="Opcional">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">País</label>
            </div>
            <div class="small-10 columns">
                <select name="pais_med" id="pais_med">
                    <?php echo print_paises(); ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Ciudad</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="ciudad_med" name="ciudad_med">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Año Inicio</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="inicio_med" id="inicio_med" min="1900">
            </div>
            <div class="small-2 columns">
                <label class="right inline">Año Término</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="termino_med" id="termino_med" min="1901">
            </div>
        </div>
        <a id="btn_add_med" href="#" class="button expand">Agregar</a>
        <a id="btn_act_med" href="#" class="button warning expand" style="display: none;" data-url="<?php echo site_url('antecedente/editar_media');?>">Actualizar</a>
    </form>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!--  -->
<div id="modalSuperior" class="reveal-modal small" data-reveal aria-labelledby="supTitle" aria-hidden="true" role="dialog">
    <h2 id="supTitle">Agregar Establecimiento</h2>
    <form id="form_agregar_superior" action="<?php echo site_url('antecedente/agregar_superior');?>">
        <input id="id_sup" name="id_sup" type="hidden">
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Nombre</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="institucion_sup" name="institucion_sup">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Tipo</label>
            </div>
            <div class="small-10 columns">
                <select name="tipo_sup" id="tipo_sup">
                    <option value="Universidad">Universidad</option>
                    <option value="Instituto Profesional">Instituto Profesional</option>
                    <option value="Centro de Formación Técnica">Centro de Formación Técnica</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">País</label>
            </div>
            <div class="small-10 columns">
                <select name="pais_sup" id="pais_sup">
                    <?php echo print_paises(); ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Ciudad</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="ciudad_sup" name="ciudad_sup">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Carrera</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="carrera_sup" name="carrera_sup">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Año Inicio</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="inicio_sup" id="inicio_sup" min="1900">
            </div>
            <div class="small-2 columns">
                <label class="right inline">Año Término</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="termino_sup" id="termino_sup" min="1901">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Situación</label>
            </div>
            <div class="small-4 columns">
                <select name="situacion_sup" id="situacion_sup">
                    <option value="No Tiene">No Tiene</option>
                    <option value="Incompleta">Incompleta</option>
                    <option value="Titulado">Titulado</option>
                    <option value="Egresado">Egresado</option>
                    <option value="En Trámite">En Trámite</option>
                </select>
            </div>
            <div class="small-2 columns">
                <label class="right inline">Grado Académico</label>
            </div>
            <div class="small-4 columns">
                <select name="grado_sup" id="grado_sup">
                    <option value="No Tiene">No Tiene</option>
                    <option value="Doctorado">Doctorado</option>
                    <option value="Licenciado">Licenciado</option>
                    <option value="Profesional">Profesional</option>
                    <option value="Técnico Profesional">Técnico Profesional</option>
                </select>
            </div>
        </div>
        <a id="btn_add_sup" href="#" class="button expand">Agregar</a>
        <a id="btn_act_sup" href="#" class="button warning expand"  style="display: none;" data-url="<?php echo site_url('antecedente/editar_superior');?>">Actualizar</a>
    </form>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!--  -->
<div id="modalCapacitacion" class="reveal-modal small" data-reveal aria-labelledby="capTitle" aria-hidden="true" role="dialog">
    <h2 id="capTitle">Agregar Capacitación</h2>
    <form id="form_agregar_capacitacion" action="<?php echo site_url('antecedente/agregar_capacitacion');?>">
        <input id="id_cap" name="id_cap" type="hidden">
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Institución</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="institucion_cap" name="institucion_cap" placeholder="Nombre de la Institución">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Lugar</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="ciudad_cap" name="ciudad_cap">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Capacitación</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="nombre_cap" name="nombre_cap" placeholder="Nombre del Curso / Seminario">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Período Inicio</label>
            </div>
            <div class="small-4 columns">
                <select name="mes_inicio_cap" id="mes_inicio_cap">
                    <?php echo print_meses(); ?>
                </select>
            </div>
            <div class="small-2 columns">
                <label class="right inline">Año</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="inicio_cap" id="inicio_cap" min="1901">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Período Término</label>
            </div>
            <div class="small-4 columns">
                <select name="mes_termino_cap" id="mes_termino_cap">
                    <?php echo print_meses(); ?>
                </select>
            </div>
            <div class="small-2 columns">
                <label class="right inline">Año</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="termino_cap" id="termino_cap" min="1901">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Horas</label>
            </div>
            <div class="small-4 columns end">
                <input type="number" name="horas_cap" id="horas_cap" min="0">
            </div>
        </div>
        <a id="btn_add_cap" href="#" class="button expand">Agregar</a>
        <a id="btn_act_cap" href="#" class="button warning expand"  style="display: none;" data-url="<?php echo site_url('antecedente/editar_capacitacion');?>">Actualizar</a>
    </form>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!--  -->
<div id="modalEspecializacion" class="reveal-modal small" data-reveal aria-labelledby="espTitle" aria-hidden="true" role="dialog">
    <h2 id="espTitle">Agregar Especialización</h2>
    <form id="form_agregar_especializacion" action="<?php echo site_url('antecedente/agregar_especializacion');?>">
        <input id="id_esp" name="id_esp" type="hidden">
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Institución</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="institucion_esp" name="institucion_esp" placeholder="Nombre de la Institución">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Nombre</label>
            </div>
            <div class="small-10 columns">
                <input type="text" id="nombre_esp" name="nombre_esp" placeholder="Nombre del Magíster / Diplomado / Post Título">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Tipo</label>
            </div>
            <div class="small-10 columns end">
                <select name="tipo_esp" id="tipo_esp">
                    <option value="Post Título">Post Título</option>
                    <option value="Magíster">Magíster</option>
                    <option value="Doctorado">Doctorado</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Período Inicio</label>
            </div>
            <div class="small-4 columns">
                <select name="mes_inicio_esp" id="mes_inicio_esp">
                    <?php echo print_meses(); ?>
                </select>
            </div>
            <div class="small-2 columns">
                <label class="right inline">Año</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="inicio_esp" id="inicio_esp" min="1901">
            </div>
        </div>
        <div class="row">
            <div class="small-2 columns">
                <label class="right inline">Período Término</label>
            </div>
            <div class="small-4 columns">
                <select name="mes_termino_esp" id="mes_termino_esp">
                    <?php echo print_meses(); ?>
                </select>
            </div>
            <div class="small-2 columns">
                <label class="right inline">Año</label>
            </div>
            <div class="small-4 columns">
                <input type="number" name="termino_esp" id="termino_esp" min="1901">
            </div>
        </div>
        <a id="btn_add_esp" href="#" class="button expand">Agregar</a>
        <a id="btn_act_esp" href="#" class="button warning expand"  style="display: none;" data-url="<?php echo site_url('antecedente/editar_especializacion');?>">Actualizar</a>
    </form>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!--  -->
</div>
