<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Currículum</title>
        <link rel="stylesheet" href="<?php
            echo base_url(); ?>assets/css/foundation.css" />
            <link rel="stylesheet" href="<?php
                echo base_url(); ?>assets/css/font-awesome.css" />
                <script src="<?php
                echo base_url(); ?>assets/js/modernizr.js"></script>
            </head>
            <body>
                <div id="curriculum">
                    <div class="row">
                        <div class="small-6 large-centered columns">
                            <h1 class="text-center">CURRÍCULUM VITAE</h1>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="small-12 columns">
                            <h2>Antecedentes Personales</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-3 large-2 columns">
                            <strong>Nombre Completo</strong>
                        </div>
                        <div id="cv_name" name="cv_name" class="small-9 large-10 columns" data-name="<?php
                            echo $usuario['apellidos']; ?>"><?php
                        echo $usuario['nombres'] . ' ' . $usuario['apellidos']; ?></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="small-3 large-2 columns">
                            <strong>RUT</strong>
                        </div>
                        <div id="cv_rut" name="cv_rut" class="small-3 large-4 columns" data-rut="<?php
                            echo $usuario['rut']; ?>"><?php
                        echo $usuario['rut']; ?></div>
                        <div class="small-3 large-2 columns">
                            <strong>Fecha de Nacimiento</strong>
                        </div>
                        <div class="small-3 large-4 columns"><?php
                        echo $usuario['fecha_nacimiento']; ?></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="small-3 large-2 columns">
                            <strong>Dirección</strong>
                        </div>
                        <div class="small-9 large-10 columns"><?php
                        echo $usuario['direccion']; ?></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="small-3 large-2 columns">
                            <strong>Teléfono</strong>
                        </div>
                        <div class="small-3 large-4 columns"><?php
                        echo $usuario['telefono']; ?></div>
                        <div class="small-3 large-2 columns">
                            <strong>Correo Electrónico</strong>
                        </div>
                        <div class="small-3 large-4 columns"><?php
                        echo $usuario['email']; ?></div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">
                            <hr>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="small-12 columns">
                            <h2>Antecedentes Académicos</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">
                            <h3>Enseñanza Media</h3>
                        </div>
                    </div>
                    <?php
                    foreach ($info_media as $institucion) {
                        echo '<div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Institución</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $institucion['nombre'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Tipo</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $institucion['tipo'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>País</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $institucion['pais'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Inicio</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $institucion['inicio'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Término</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $institucion['termino'] . '</div>
                                        </div>
                                        <br>';
                    } ?>
                    <div class="row">
                        <div class="small-12 columns">
                            <h3>Enseñanza Superior</h3>
                        </div>
                    </div>
                    <?php
                    foreach ($info_superior as $institucion) {
                        echo '<div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Institución</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $institucion['nombre'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Tipo</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $institucion['tipo'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>País</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $institucion['pais'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Carrera</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $institucion['carrera'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Inicio</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $institucion['inicio'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Término</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $institucion['termino'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Situación</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $institucion['situacion'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Grado Académico</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $institucion['grado'] . '</div>
                                        </div>
                                        <br>';
                    }
                    ?>
                    <div class="row">
                        <div class="small-12 columns">
                            <h3>Capacitaciones</h3>
                        </div>
                    </div>
                    <?php
                    foreach ($info_capacitaciones as $capacitacion) {
                        echo '<div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Nombre</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $capacitacion['nombre'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Institución</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $capacitacion['institucion'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Inicio</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $capacitacion['mes_inicio'] . '/' . $capacitacion['inicio'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Término</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $capacitacion['mes_termino'] . '/' . $capacitacion['termino'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Horas</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $capacitacion['horas'] . '</div>
                                        </div>
                                        <br>';
                    }
                    ?>
                    <div class="row">
                        <div class="small-12 columns">
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">
                            <h2>Antecedentes Laborales</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">
                            <h3>Experiencia Laboral</h3>
                        </div>
                    </div>
                    <?php
                    foreach ($info_antecedentes as $antecedente) {
                        echo '<div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Empresa</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $antecedente['nombre_empresa'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Cargo</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $antecedente['cargo'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>País</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $antecedente['pais'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Inicio</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $antecedente['mes_inicio'] . '/' . $antecedente['inicio'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Término</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $antecedente['mes_termino'] . '/' . $antecedente['termino'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Funciones</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $antecedente['funciones'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Logros</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $antecedente['logros'] . '</div>
                                        </div>
                                        <br>';
                    }
                    ?>
                    <div class="row">
                        <div class="small-12 columns">
                            <h3>Referencias</h3>
                        </div>
                    </div>
                    <?php
                    foreach ($info_referencias as $referencia) {
                        echo '<div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Nombre</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $referencia['nombre'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Cargo</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $referencia['cargo'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Teléfono</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $referencia['telefono'] . '</div>
                                        </div>
                                        <div class="row">
                                                    <div class="small-3 large-2 columns">
                                                                <strong>Correo Electrónico</strong>
                                                    </div>
                                                    <div class="small-9 large-10 columns">' . $referencia['email'] . '</div>
                                        </div>
                                        <br>';
                    }
                    ?>
                </div>
                <div class="row">
                    <div class="small-6 large-centered columns">
                        <a id="btn_descargar" href="<?php echo site_url('cv/topdf').'/'.$usuario['rut'];?>" class="button alert"><i class="fa fa-file-pdf-o"></i> Descargar</a>
                        <a href="#" id="btn_imprimir" class="button success"><i class="fa fa-print"></i> Imprimir</a>
                        <a href="javascript:history.back()" id="btn_volver" class="button"><i class="fa fa-reply"></i> Volver</a>
                    </div>
                </div>
                <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
                <script src="<?php echo base_url(); ?>assets/js/foundation.min.js"></script>
                <script src="<?php echo base_url();?>assets/js/jspdf.min.js"></script>
                <script>
                var doc = new jsPDF();
                // var specialElementHandlers = {
                //     '#editor': function (element, renderer) {
                //         return true;
                //     }
                // };
                //var $filename = "curriculum_"+$('#cv_name').data('name')+"_"+$("#cv_rut").data('rut')+".pdf";
                // $(document).on('click','#btn_descargar',function(e){
                // e.preventDefault();
                // e.stopPropagation();
                // });
                // $('#btn_descargar').click(function () {
                //     doc.fromHTML($('#curriculum').html(), 15, 15, {
                //         'width': 170
                //     });
                //     doc.save($filename);
                // });
                $(document).on('click','#btn_imprimir',function(e){
                	$('#btn_descargar').hide();
                	$('#btn_imprimir').hide();
                	$('#btn_volver').hide();
                	window.print();
                	$('#btn_descargar').show();
                	$('#btn_imprimir').show();
                	$('#btn_volver').show();
                });
                </script>
            </body>
        </html>