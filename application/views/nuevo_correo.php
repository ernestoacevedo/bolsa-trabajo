<div class="row" style="margin-top: 10px;">
    <div class="large-4 small-12 columns">
        <div class="panel sidepanel">
            <?php echo print_menu($this->session->userdata('nombre')); ?>
        </div>
    </div>
    <div class="contenido large-8 columns">
        <h2>Enviar Correo Masivo</h2>
        <form id="form_correo" action="<?php echo site_url('admin/enviar_correo');?>" method="POST" accept-charset="utf-8">
            <div class="row">
                <div class="small-2 columns">
                    <label for="asunto" class="right inline">Asunto</label>
                </div>
                <div class="small-10 columns">
                    <input type="text" name="asunto" id="asunto">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label for="asunto" class="right inline">Contenido</label>
                </div>
                <div class="small-10 columns">
                    <textarea name="contenido" id="contenido" class="msg" cols="30" rows="20"></textarea>
                </div>
            </div>
            <div class="large-8 large-offset-4 columns">
                    <a href="<?php echo site_url('admin');?>" id="cancelar" class="button alert"> Cancelar </a>
                    <input type="submit" class="button" value="Enviar Correo">
            </div>
        </form>
    </div>
</div>
</div>