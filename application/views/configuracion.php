<div class="row" style="margin-top: 10px;">

    <div class="large-4 small-12 columns">

        <div class="panel sidepanel">
            <?php echo print_menu_user($this->session->userdata('nombre')); ?>
        </div>
    </div>

    <div class="contenido large-8 columns">
        <h2>Configuración</h2>
        <form id="form_config" action="<?php echo site_url('usuario/editar_config');?>">
            <div class="row">
                <div class="small-2 columns">
                    <label for="titulo" class="right inline">Recibir correos masivos</label>
                </div>
                <div class="small-10 columns">
                    <div class="switch">
                        <input id="recibir_info" name="recibir_info" type="checkbox" value="<?php echo $usuario['recibir_info'];?>"
                        <?php $estado = ($usuario['recibir_info'] == 1) ? 'checked' : '';
                        echo $estado;
                        ?>>
                        <label for="recibir_info"></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="large-8 large-offset-4 columns">
                    <a href="<?php echo site_url('dashboard');?>" id="cancelar" class="button alert"> Cancelar </a>
                    <a id="actualizar_config_btn" href="#" class="button"> Guardar </a>
                </div>
            </div>
        </form>
    </div>
</div>