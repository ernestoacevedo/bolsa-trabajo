<div class="row" style="margin-top: 10px;">
    <div class="large-4 small-12 columns">
        <div class="panel sidepanel">
            <?php echo print_menu_user($this->session->userdata('nombre'));?>
        </div>
    </div>
    <div class="contenido large-8 columns">
        <h2>Listado de Postulaciones</h2>
        <table id="tabla_postulaciones" class="datatable" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    Rut Postulante
                </th>
                <th style="text-align: center;">
                    Nombre Postulante
                </th>
                <th style="text-align: center;">
                    Oferta
                </th>
                <th style="text-align: center;">
                    Fecha Postulación
                </th>
                <th style="text-align: center;">
                    Estado
                </th>
                <th style="text-align: center;">
                    Etapa
                </th>
                <th style="text-align: center;">
                    Ver Avance
                </th>
            </thead>
            <tbody>
                <?php

                                                    foreach($postulaciones as $postulacion){
                                                    $estado = ($postulacion['estado'] == 0) ? 'Terminada' : 'Activa';
                                                    echo '<tr>';
                                                            echo '<td style="text-align: center;">'.$postulacion['rut_postulante'].'</td>';
                                                            echo '<td style="text-align: center;"><a href="'.site_url('cv/ver_cv/'.$postulacion['rut_postulante']).'">'.$postulacion['nombres'].' '.$postulacion['apellidos'].'</a></td>';
                                                            echo '<td style="text-align: center;">'.$postulacion['titulo'].'</td>';
                                                            echo '<td style="text-align: center;">'.$postulacion['fecha'].'</td>';
                                                            echo '<td style="text-align: center;">'.$estado.'</td>';
                                                            echo '<td style="text-align: center;">'.obtenerEtapas($postulacion['etapa']-1).'</td>';
													echo '<td style="text-align: center;"><a class="btnavance" href="'.site_url('oferta/data_avance').'/'.$postulacion['id_oferta'].'"><i class="fa fa-eye"></i></a></td>';
                                                    echo '</tr>';
                                                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
<div id="modalAvance" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <h2 id="modalTitle">Avance.</h2>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
  <h3 style="font-size: 18px;">Etapa Actual: <strong></strong></h3>
  <table id="tabla_avance" cellspacing="0">
      <tbody>
              <tr><td class="fill" data-etapa="1" data-hasqtip="24" oldtitle="Recepción de Currículum" title="" aria-describedby="qtip-24"></td><td class="fill" data-etapa="2" data-hasqtip="25" oldtitle="Entrevista Psicológica" title=""></td><td class="fill" data-etapa="3" data-hasqtip="26" oldtitle="Entrevista Personal" title=""></td><td data-hasqtip="27" oldtitle="Selección" title=""></td></tr>
      </tbody>
  </table>
  <table id="info_avance" cellspacing="0">
      <tbody>
           <tr>
            <td class="fill">
              <ul id="etapa1">
              </ul>
            </td>
            <td class="fill">
              <ul id="etapa2">
              </ul>
            </td>
            <td class="fill">
              <ul id="etapa3">
              </ul>
            </td>
            <td class="fill">
              <ul id="etapa4">
              </ul>
            </td>
           </tr>
         </tbody>
     </table>
</div>
</div>
