<div class="row" style="margin-top: 10px;">
    <div class="large-4 small-12 columns">
        <div class="panel sidepanel">
            <?php echo print_menu($this->session->userdata('nombre'));?>
        </div>
    </div>
    <div class="large-8 columns">
        <h2>Usuarios Registrados</h2>
        <table id="tabla_usuarios" class="datatable" border="1" style="table">
            <thead>
                <th style="text-align: center; width: 40px;">
                    RUT
                </th>
                <th style="text-align: center; width: 40px;">
                    Nombre completo
                </th>
                <th style="text-align: center; width: 30px;">
                    Nivel de estudios
                </th>
                <th style="text-align: center; width: 30px;">
                    Ciudad
                </th>
                <th style="text-align: center; width: 30px;">
                    Email
                </th>
                <th style="text-align: center; width: 30px;">
                    Celular
                </th>
                <th style="text-align: center; width: 30px;">
                    Disponible para Reemplazos
                </th>
                <th style="text-align: center; width: 30px;" class="nosort">
                  Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                    foreach($usuarios as $usuario){
                                                    $reemp = ($usuario['reemplazo']=='1') ? 'Sí' : 'No';
                                                    echo '<tr>';
                                                            echo '<td class="rut" style="text-align: center;">'.$usuario['rut'].'</td>';
                                                            echo '<td style="text-align: center;"><a class="user_link" data-rut="'.$usuario['rut'].'" href="#" data-cv="'.site_url('cv/topdf').'/'.$usuario['rut'].'" data-url="'.site_url('documento/documentos_usuario').'/'.$usuario['rut'].'">'.$usuario['nombres'].' '.$usuario['apellidos'].'</a></td>';
                                                            echo '<td style="text-align: center;">'.$usuario['nivel'].'</td>';
                                                            echo '<td style="text-align: center;">'.$usuario['ciudad'].'</td>';
                                                            echo '<td style="text-align: center;">'.$usuario['email'].'</td>';
                                                            echo '<td style="text-align: center;">'.$usuario['celular'].'</td>';
                                                            echo '<td style="text-align: center;">'.$reemp.'</td>';
                                                            echo '<td style="text-align: center;"><a href="'.site_url('usuario/eliminar').'/'.$usuario['rut'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                    echo '</tr>';
                                                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
<div id="modalDatos" class="reveal-modal small" data-reveal aria-labelledby="dTitle" aria-hidden="true" role="dialog">
  <h2 id="dTitle"></h2>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
  <div class="row">
      <div class="small-6 columns">
          <br>
          <a id="cv_link" href="#" target="_blank">Ver Currículum</a>
      </div>
  </div>
  <br>
  <table id="tabla_datos">
    <thead>
      <th style="text-align: center;">
          Tipo
      </th>
      <th style="text-align: center;">
          Ver
      </th>
    </thead>
    <tbody>

    </tbody>
  </table>
</div> <!-- modalDatos -->
</div>
