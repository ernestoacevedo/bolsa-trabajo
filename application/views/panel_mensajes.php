<div class="row" style="margin-top: 10px;">
    <div class="large-4 small-12 columns">
        <div class="panel sidepanel">
            <?php echo print_menu($this->session->userdata('nombre')); ?>
        </div>
    </div>
    <div class="large-8 columns">
        <h2>Mensajes Predeterminados</h2>
        <br>
        <form id="form_mensajes" action="<?php echo site_url('admin/actualizar_config');?>" method="POST">
            <input type="hidden" name="id" id="id" value="<?php echo $config['id'];?>">
            <label>Registro</label>
            <textarea name="registro" id="registro" class="msg" cols="30" rows="10"><?php echo $config['msg_registro'];?></textarea>
            <label>Inicio de Postulación</label>
            <textarea name="inicio" id="inicio" class="msg" cols="30" rows="10"><?php echo $config['msg_inicio'];?></textarea>
            <label>Avance de Etapa</label>
            <textarea name="avance" id="avance" class="msg" cols="30" rows="10"><?php echo $config['msg_avance'];?></textarea>
            <label>Término de Postulación</label>
            <textarea name="termino" id="termino" class="msg" cols="30" rows="10"><?php echo $config['msg_termino'];?></textarea>
            <label>Selección</label>
            <textarea name="seleccion" id="seleccion" class="msg" cols="30" rows="10"><?php echo $config['msg_seleccion'];?></textarea>
            <input type="submit" id="act_config" class="button expand" value="Actualizar Configuración">
        </form>
    </div>
</div>
</div>