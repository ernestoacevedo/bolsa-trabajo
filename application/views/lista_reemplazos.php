<div class="row" style="margin-top: 10px;">
    <div class="large-4 small-12 columns">
        <div class="panel sidepanel">
            <?php echo print_menu($this->session->userdata('nombre'));?>
        </div>
    </div>
    <div class="large-8 columns">
        <h2>Lista de Reemplazos</h2>
        <table id="tabla_usuarios" class="datatable" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    RUT
                </th>
                <th style="text-align: center;">
                    Nombre Completo
                </th>
                <th style="text-align: center;">
                    Tipo Reemplazo
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                    foreach($usuarios as $usuario){
                                                    echo '<tr>';
                                                            echo '<td style="text-align: center;">'.$usuario['rut'].'</td>';
                                                            echo '<td style="text-align: center;"><a class="user_link" data-rut="'.$usuario['rut'].'" href="#" data-cv="'.site_url('cv/topdf').'/'.$usuario['rut'].'" data-url="'.site_url('documento/documentos_usuario').'/'.$usuario['rut'].'">'.$usuario['nombres'].' '.$usuario['apellidos'].'</a></td>';
                                                            echo '<td style="text-align: center;">'.obtenerTipoReemplazo($usuario['tipo_reemplazo']-1).'</td>';
                                                            echo '<td style="text-align: center;"><a href="'.site_url('usuario/eliminar').'/'.$usuario['rut'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                    echo '</tr>';
                                                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
</div>
