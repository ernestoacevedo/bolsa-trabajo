<div class="row" style="margin-top: 10px;">
    <div class="large-4 small-12 columns">
        <div class="panel sidepanel">
            <?php echo print_menu($this->session->userdata('nombre')); ?>
        </div>
    </div>
    <div class="contenido large-8 columns">
        <h3>Exportar datos de postulante</h3>
        <form action="<?php echo site_url('admin/exportar');?>" method="post">
            <label for="rut_postulante"></label>
            <select name="rut_postulante" id="rut_postulante">
                <option value="">Seleccione un postulante</option>
                <?php
                                            foreach($usuarios as $usuario){
                                                echo '<option value="'.$usuario['rut'].'">'.$usuario['apellidos'].' '.$usuario['nombres'].'</option>';
                                            }
                ?>
            </select>
            <button class="button success" type="submit"><i class="fa fa-file-excel-o"></i> Descargar</button>
        </form>
        <br>
        <h3>Exportar todos los datos</h3>
        <form action="<?php echo site_url('admin/exportar');?>" method="post">
            <input type="hidden" id="rut_postulante" name="rut_postulante">
            <button class="button success" type="submit"><i class="fa fa-file-excel-o"></i> Descargar todos</button>
        </form>
    </div>
</div>
</div>