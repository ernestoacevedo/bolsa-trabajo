<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$titulo.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<table border="1">
  <thead>
  <tr>
    <th>RUT</th>
    <th>Postulante</th>
    <th>Ciudad de residencia</th>
    <th>Nivel de estudios</th>
    <th>Título</th>
    <th>Cargo al que postula</th>
    <th>Correo Electrónico</th>
    <th>Nº Celular</th>
    <th>Disponible para reemplazos</th>
    <th>Fecha de última actualización</th>
  </tr>
  </thead>
  <?php
  echo '<tbody>';
  echo utf8_decode($contenido);
  echo '</tbody>';
  ?>
</table>
