<div class="row" style="margin-top: 10px;">
    <div class="large-4 small-12 columns">
        <div class="panel sidepanel">
            <?php echo print_menu($this->session->userdata('nombre')); ?>
        </div>
    </div>
    <div class="large-8 columns">
        <h3><i class="fa fa-archive"></i> Visualización de Documentos</h3>
        <table id="tabla_documentos" class="datatable" border="1" style="table">
            <thead>
                <th style="text-align: center;">
                    RUT
                </th>
                <th style="text-align: center;">
                    Postulante
                </th>
                <th style="text-align: center;">
                    Tipo
                </th>
                <th style="text-align: center;">
                    Ver
                </th>
                <th style="text-align: center;">
                    Eliminar
                </th>
            </thead>
            <tbody>
                <?php
                                                                                    foreach($documentos as $documento){
                                                                                    echo '<tr>';
                                                                                                    echo '<td style="text-align: center;">'.$documento['rut_postulante'].'</td>';
                                                                                                    echo '<td style="text-align: center;">'.$documento['nombres'].' '.$documento['apellidos'].'</td>';
                                                                                                    echo '<td style="text-align: center;">'.$documento['tipo_documento'].'</td>';
                                                                                                    echo '<td style="text-align: center;"><a href="'.base_url().'documentos/'.$documento['ruta_documento'].'"><i class="fa fa-eye"></i></a> </td>';
                                                                                                    echo '<td style="text-align: center;"><a href="'.site_url('documento/eliminar').'/'.$documento['id'].'" class="eliminar"><i class="fa fa-trash fa-lg"></i></a></td>';
                                                                                    echo '</tr>';
                                                                                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
</div>